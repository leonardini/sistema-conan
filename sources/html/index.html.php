<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="stylesheet" type="text/css" href="default/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="default/css/bootstrap-theme.min.css">
	<script src="default/js/jquery-1.11.0.min.js"></script>
	<script src="default/js/bootstrap.min.js"></script>
	<!--<script src="default/js/jquery-ui-1.10.3.min.js"></script>-->
	<link rel="SHORTCUT ICON" href="default/icon/32blitz.ico">
	<title><?= $title; ?></title>
<style type="text/css">
body{padding-top: 40px;padding-bottom: 40px;background-color: #eee;}
</style>
<script>
$(function(){
	$('[title]').tooltip();
});
</script>
</head>
<div class="modal fade" id="contacto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">Contactar al Desarrollador</h4>
		</div>
		<div class="modal-body">
			<p><img src="default/images/32.png" class="img-thumbnail" width="150px;" height="150px;"></p>
			<p><span class="glyphicon glyphicon-phone"></span> <strong>591-76108443</strong> </p>
			<p><span class="glyphicon glyphicon-envelope"></span> <strong>ohkmalganis@gmail.com</strong> </p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger" class="close" data-dismiss="modal">
				<span class="glyphicon glyphicon-ban-circle"></span>
				<span class="hidden-xs"> Cerrar</span>
			</button>
		</div>
	</div>
</div>
<body>
	<center>
		<a href="in.php?m=login&f=do"><img src="default/images/back.png" class="img-thumbnail"></a>
	</center><br>
	<p class="text-center">
		<a data-toggle="modal" data-target="#contacto" class="btn btn-warning">
			<span class="glyphicon glyphicon-phone"></span> Contactar
		</a>
	</p>
</body>
</html>