<?php

require_once('CodigoControlV7.php');

function formatear_en($fecha){
    $dates = new DateTime($fecha);
    $dat = $dates->format('Y-m-d');
    return $dat;
}

function formato_factura($fecha)
{
    $fecha = formatear_en($fecha);
    // Separando Año, Mes, Dia FINAL
    $ano = substr($fecha, 0, -6);
    $mes = substr($fecha, 5, -3);
    $dia = substr($fecha, -2);
    return $ano.$mes.$dia;
}

// 1

$numero_autorizacion = '3904001727777';
$numero_factura = '996767';
$nit_cliente = '0';
$fecha_compra = formato_factura(str_replace("/", "-", '18/06/2007'));
//echo $fecha_compra."<br>";
$monto_compra = round('42126');
$clave = 'wf-mjQ\_9p}rE76GjTf5wbIa3n]=fH_$5dFEqCAs6$Kg)tshE3Iwup9xD*YAf-(D';
$clave = trim($clave);

echo CodigoControlV7::generar($numero_autorizacion, $numero_factura, $nit_cliente, $fecha_compra, $monto_compra, $clave).'<br><br>';

// 2

$numero_autorizacion = '5004004753679';
$numero_factura = '648685';
$nit_cliente = '2441946';
$fecha_compra = formato_factura(str_replace("/", "-", '15/11/2008'));
//echo $fecha_compra."<br>";
$monto_compra = round('96927');
$clave = 'sxmyzn3zEvd7rsdBZUS-Gg])v3+B#i))a7xz*CvziP+qbKBzX%S(b_SWyX_kG-pm';
$clave = trim($clave);

echo CodigoControlV7::generar($numero_autorizacion, $numero_factura, $nit_cliente, $fecha_compra, $monto_compra, $clave).'<br><br>';

// 3

$numero_autorizacion = '6004004328489';
$numero_factura = '643545';
$nit_cliente = '2699012';
$fecha_compra = formato_factura(str_replace("/", "-", '22/07/2008'));
//echo $fecha_compra."<br>";
$monto_compra = round('63540.49');
$clave = 'Iuj#}W6mUXqDfkW77#TjL(7jLFt3@eKZx@=Nd5jgz}C6-Ddi}83{nuSd_L(}]P6B';
$clave = trim($clave);

echo CodigoControlV7::generar($numero_autorizacion, $numero_factura, $nit_cliente, $fecha_compra, $monto_compra, $clave).'<br><br>';

// 4

$numero_autorizacion = '5004008001517';
$numero_factura = '750483';
$nit_cliente = '1748787';
$fecha_compra = formato_factura(str_replace("/", "-", '05/07/2007'));
//echo $fecha_compra."<br>";
$monto_compra = round('48962');
$clave = 'iiv}#]d5bqf6*#JVmGRbePwz-yp*Q(h_Prn}}qD5vC+9x*W6W{LMhIYZRTwDz7{z';
$clave = trim($clave);

echo CodigoControlV7::generar($numero_autorizacion, $numero_factura, $nit_cliente, $fecha_compra, $monto_compra, $clave).'<br><br>';

// 5

$numero_autorizacion = '2904002922240';
$numero_factura = '535050';
$nit_cliente = '1522420';
$fecha_compra = formato_factura(str_replace("/", "-", '14/02/2008'));
//echo $fecha_compra."<br>";
$monto_compra = round('67178');
$clave = 'mPwspveZ#Pm=t*Fig+$j6=]N5LC7yWMwkutnZDcSXv]KAUt5-I-kVjIqj-xXBzEY';
$clave = trim($clave);

echo CodigoControlV7::generar($numero_autorizacion, $numero_factura, $nit_cliente, $fecha_compra, $monto_compra, $clave).'<br><br>';

// 6

$numero_autorizacion = '700400544942';
$numero_factura = '639489';
$nit_cliente = '111634017';
$fecha_compra = formato_factura(str_replace("/", "-", '08/01/2007'));
//echo $fecha_compra."<br>";
$monto_compra = round('15738');
$clave = 'rdX(IGN5-sDVNt-JRf=#gNUgpP5Fz7jd8+wjSxX[qzvDbLVB8n-FNfYQ+Z42M-g$';
$clave = trim($clave);

echo CodigoControlV7::generar($numero_autorizacion, $numero_factura, $nit_cliente, $fecha_compra, $monto_compra, $clave).'<br><br>';

// 7

$numero_autorizacion = '7004009214633';
$numero_factura = '629448';
$nit_cliente = '4676279';
$fecha_compra = formato_factura(str_replace("/", "-", '16/08/2008'));
//echo $fecha_compra."<br>";
$monto_compra = round('66955');
$clave = '@7iIfHYVPseS89{57WVSKL{ZK$\(3GJ4Ns}CWws]+mK+-jGMyHM_8k4J5i6gWrXj';
$clave = trim($clave);

echo CodigoControlV7::generar($numero_autorizacion, $numero_factura, $nit_cliente, $fecha_compra, $monto_compra, $clave).'<br><br>';

// 8

$numero_autorizacion = '1904001788654';
$numero_factura = '568878';
$nit_cliente = '1002720010';
$fecha_compra = formato_factura(str_replace("/", "-", '13/02/2008'));
//echo $fecha_compra."<br>";
$monto_compra = round('87176.10');
$clave = 'xR{=#I$#E\EDa[W9cVpJ(v*fppd4sXQkIZXbP#c@4%zwKe*XHxXXLr@V%Y(epMYV';
$clave = trim($clave);

echo CodigoControlV7::generar($numero_autorizacion, $numero_factura, $nit_cliente, $fecha_compra, $monto_compra, $clave).'<br><br>';

// 9

$numero_autorizacion = '3904003335469';
$numero_factura = '825730';
$nit_cliente = '3114048';
$fecha_compra = formato_factura(str_replace("/", "-", '18/05/2007'));
//echo $fecha_compra."<br>";
$monto_compra = round('19433.51');
$clave = 'u6HUq-r_6dnb#PK@cdDZLvj$\+FiM+UHsT]4mEd74NSx#iSQ9pz9]zxu)U}[t69v';
$clave = trim($clave);

echo CodigoControlV7::generar($numero_autorizacion, $numero_factura, $nit_cliente, $fecha_compra, $monto_compra, $clave).'<br><br>';

// 10

$numero_autorizacion = '3004007693620';
$numero_factura = '776540';
$nit_cliente = '3166239';
$fecha_compra = formato_factura(str_replace("/", "-", '10/03/2008'));
//echo $fecha_compra."<br>";
$monto_compra = round('34795');
$clave = '[dI)FtFsEBnxp4fzxr2w+6Nt_nbmth$P-LW]vkY#$CXpG#j@FUdjqp6*Btb-KC@5';
$clave = trim($clave);

echo CodigoControlV7::generar($numero_autorizacion, $numero_factura, $nit_cliente, $fecha_compra, $monto_compra, $clave).'<br><br>';

// $numero_autorizacion = '79040011859';
// $numero_factura = '152';
// $nit_cliente = '1026469026';
// $fecha_compra = '20070728';
// $monto_compra = '135';
// $clave = 'A3Fs4s$)2cvD(eY667A5C4A2rsdf53kw9654E2B23s24df35F5';

// echo CodigoControlV7::generar($numero_autorizacion, $numero_factura, $nit_cliente, $fecha_compra, $monto_compra, $clave).'<br/>';
?>
