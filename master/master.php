<?php

require(MASTERRUTE."db.master".EXT);
$tb = new master();
$tables = $tb->_show_tables();
$tb->Close();
// Definiendo el resultado de la Tabla
$object = "Tables_in_".DB_NAME;
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="stylesheet" type="text/css" href="default/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="default/css/bootstrap-theme.min.css">
	<script src="default/js/jquery-1.11.0.min.js"></script>
	<script src="default/js/jquery-ui-1.10.3.min.js"></script>
	<script src="default/js/bootstrap.min.js"></script>
	<link rel="SHORTCUT ICON" href="default/icon/favicon.ico">
	<title>Ohk Framework 3.0</title>
	<style type="text/css">
	.data{
		color: white;
		background: green;
	}
	</style>
</head>
<body class="container">
	<br>
	<div class="panel panel-primary">
		<div class="panel-heading"><strong> Master en Acción </strong></div>
		<div class="panel-body">
			<p>A continuación el master le hará unas preguntas para proseguir con el desarrollo automático de módulos.</p>
			<?php
			if(isset($_GET['m'])){
				$m = htmlspecialchars(addslashes(trim($_GET['m'])),ENT_QUOTES);
			}
			?>
			<p>1. El Master Desarrollara los módulos competentes dentro de la carpeta seleccionada <strong><?php echo $m; ?></strong>.</p>
			<p>Para ello requiere la pre-configuración de los siguientes aspectos.</p>
			<form name="form1" method="post" action="master/save.php">
				<p>
					<label class="form-control" for="tablas"><strong>Tablas Existentes:</strong>
					<select name="tablas__" id="tablas__">
						<option value="0">-- Seleccione --</option>
						<?php while($row = $tables->fetch_object()){ ?>
						<option value="<?= $row->$object; ?>"><?= $row->$object; ?></option>
						<?php } ?>
					</select>
					</label>
				</p>
				<div id="recibe"></div>
				<script type="text/javascript">
				$(function(){
					$("#tablas__").on("change",function(){
						var id=$(this).val();
						if(id!=0){
							$("#recibe").load('master/estructura.php?t='+id);
						}else{
							$("#recibe").empty();
						}
					});
				})
				</script>
				<p>
					<div class="btn-group">
						<label class="btn btn-primary" for="listado">
							<input type="checkbox" name="listado" id="listado" checked="checked"> Listado
						</label>
						<label class="btn btn-success">
							<input type="checkbox" name="altas" id="altas" checked="checked"> Altas
						</label>
						<label class="btn btn-warning">
							<input type="checkbox" name="edicion" id="edicion" checked="checked"> Edicion
						</label>
						<label class="btn btn-primary" for="vista">
							<input type="checkbox" name="vista" id="vista" checked="checked"> Vista
						</label>
						<label class="btn btn-danger">
							<input type="checkbox" name="bajas" id="bajas" checked="checked"> Bajas
						</label>
						<input type="hidden" name="ruta" id="ruta" value="<?= MODULES; ?>" />
					</div>
				</p>
				<p><input type="submit" name="continuar" value="Continuar con Master" class="btn btn-success" /></p>
			</form>
		</div>
		<div class="panel-footer"><small> Powered by OHK &copy 2013 </small></div>
	</div>
</body>