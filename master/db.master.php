<?php

require(DBMYSQLI);

class master extends DB{
    
    public function __construct(){
        DB::__construct();
    }
    
    public function _show_tables(){
        $query_ = "SHOW TABLES";
        return DB::execute($query_);
    }
    
    public function _show_tables_diferent($table){
        $query_ = "SHOW TABLES WHERE Tables_in_".DB_NAME." != '$table'";
        return DB::execute($query_);
    }
    
    public function _show_columns($table){
        $query_ = "SHOW COLUMNS FROM ".$table;
        return DB::execute($query_);
    }
    
    public function _call_multiple_left_join($fields, $table, $join_table, $on, $where){
        $query_ = DB::_arma_multiple_join($fields, $table, $join_table, $on, $where);
        return DB::execute($query_);
    }
    
    public function _evaluar_columna($f, $t, $n, $k, $d, $e){
        if($k === "PRI"){
            return "0";
        }else{
            if(stristr($t, '(') === FALSE){
                switch($t){
                    case 'date':
                    $return = '5';
                    return $return;
                    break;
                
                    case 'datetime':
                    $return = '5';
                    return $return;
                    break;
                
                    case 'timestamp':
                    $return = '1';
                    return $return;
                    break;
                
                    case 'time':
                    $return = '1';
                    return $return;
                    break;
                
                    case 'year':
                    $return = '1';
                    return $return;
                    break;
                
                    case 'text':
                    $return = '2';
                    return $return;
                    break;
                
                    case 'tinytext':
                    $return = '2';
                    return $return;
                    break;
                
                    case 'mediumtext':
                    $return = '2';
                    return $return;
                    break;
                
                    case 'longtext':
                    $return = '2';
                    return $return;
                    break;
                
                    default:
                    $return = '1';
                    return $return;
                }
            }else{
                $type = strstr($t, '(', true);
                switch($type){
                    
                    case 'int':
                    $return = '1';
                    return $return;
                
                    case 'varchar':
                    $return = '1';
                    return $return;
                
                    case 'char':
                    $return = '1';
                    return $return;
                
                    case 'bigint':
                    $return = '1';
                    return $return;
                
                    case 'decimal':
                    $return = '1';
                    return $return;
                
                    case 'enum':
                    $return = '3';
                    return $return;
                    
                    default:
                    $return = '1';
                    return $return;
                }
            }
        }
    }
    
    public function _select_($v, $f){
        switch($v){
            case '0':
            $return = "¡Importante!";
            return $return;
            break;
            case '1':
            $return = "
            <select name=\"$f\" id=\"$f\">
                <option value=\"text\" selected>Text</option>
                <option value=\"textarea\">TextArea</option>
                <option value=\"select\">Select</option>
                <option value=\"hidden\">Hidden</option>
                <option value=\"datepicker\">DatePicker</option>
                <option value=\"ot\">Otra Tabla</option>
            </select>
            ";
            return $return;
            break;
            case '2':
            $return = "
            <select name=\"$f\" id=\"$f\">
                <option value=\"text\">Text</option>
                <option value=\"textarea\" selected>TextArea</option>
                <option value=\"select\">Select</option>
                <option value=\"hidden\">Hidden</option>
                <option value=\"datepicker\">DatePicker</option>
                <option value=\"ot\">Otra Tabla</option>
            </select>
            ";
            return $return;
            break;
            case '3':
            $return = "
            <select name=\"$f\" id=\"$f\">
                <option value=\"text\">Text</option>
                <option value=\"textarea\">TextArea</option>
                <option value=\"select\" selected>Select</option>
                <option value=\"hidden\">Hidden</option>
                <option value=\"datepicker\">DatePicker</option>
                <option value=\"ot\">Otra Tabla</option>
            </select>
            ";
            return $return;
            break;
            case '4':
            $return = "
            <select name=\"$f\" id=\"$f\">
                <option value=\"text\">Text</option>
                <option value=\"textarea\">TextArea</option>
                <option value=\"select\">Select</option>
                <option value=\"hidden\" selected>Hidden</option>
                <option value=\"datepicker\">DatePicker</option>
                <option value=\"ot\">Otra Tabla</option>
            </select>
            ";
            return $return;
            break;
            case '5':
            $return = "
            <select name=\"$f\" id=\"$f\">
                <option value=\"text\">Text</option>
                <option value=\"textarea\">TextArea</option>
                <option value=\"select\">Select</option>
                <option value=\"hidden\">Hidden</option>
                <option value=\"datepicker\" selected>DatePicker</option>
                <option value=\"ot\">Otra Tabla</option>
            </select>
            ";
            return $return;
            break;
        }
    }
}
?>