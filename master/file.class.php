<?php

// Clases que generan los archivos vitales para el master
// De acuerdo a los datos enviados generamos formularios
// en HTML y las clases en php para su funcionamiento.

class file_master{
    
    public function __construct(){
        
    }
    
    // Funcion que recorre todos los valores recibidos por el metodo POST
    // sin tener una referencia del nombre de cada uno de los valores.

    public function _show_me_the_post($post){
        
        $array = array();
    
        foreach($post AS $id => $values){
            if(is_array($values)){
                foreach($values AS $idd => $valuess){
                    if(($valuess!=0) or (!empty($valuess))){
                        $array[$id][$idd] = array($idd => $valuess);
                    }
                }
            }else{
                $array[$id] = $values;
            }
        }
        
        return $array;
    }
    
    // En dos niveles, busca dentro de un array, los valores
    // dependiendo del Key enviado y llega hasta el segundo
    // nivel de arrays en busca de su valor
    
    public function _dame_tabla($array, $campo, $columna){
        $val2 = $array;
        foreach($array AS $id => $value){
            if($id==$columna){
                foreach($val2 AS $idd => $valuess){
                    if($idd==$campo){
                        if(is_array($valuess)){
                            foreach($valuess AS $vid => $vv){
                                if((is_array($vv))and $vid==$columna){
                                    foreach($vv AS $vd => $aa){
                                        if(!is_array($aa)){
                                            return $aa;
                                        }else{
                                            return "NO ENCONTRADO!";
                                        }
                                    }
                                }
                            }
                        }else{
                            return $valuess;
                        }
                    }
                }
                return "NULL";
            }
        }
        return "NO ENCONTRADO!";
    }
    
    // de acuerdo a los campos enviados por el metodo post
    // y los datos enviados de una consulta, conocemos los campos
    // y llenamos con las caracteristicas globales y personales
    
    public function _make_the_post($fields, $values, $primary, $post){
        $array_return = array();
        if(is_array($primary)){
            foreach($primary AS $pr => $value_pri){
                $array_return[$value_pri] = array(
                    "type" => "primary",
                    "table" => "NULL",
                    "label" => "NULL",
                    "value" => "NULL",
                    "fm" => "NULL",
                    "wysiwyg" => "NULL",
                    "dtp" => "NULL",
                    "null" => "NO",
                    "invalue" => "PRI",
                    "lbl" => $post["lbl"][$value_pri]
                );
            }
        }
        if(is_array($fields)){
            foreach($fields AS $id => $value){
                if($this->_dame_tabla($values, $value, $value)=="ot"){
                    $array_return[$value] = array(
                        "type" => "ot",
                        "table" => $this->_dame_tabla($values, "tables", $value),
                        "label" => $this->_dame_tabla($values, "labels", $value),
                        "value" => $this->_dame_tabla($values, "values", $value),
                        "fm" => "NULL",
                        "wysiwyg" => "NULL",
                        "dtp" => "NULL",
                        "null" => $this->_dame_tabla($values, "null", $value),
                        "invalue" => $this->_dame_tabla($values, "invalue", $value),
                        "lbl" => $this->_dame_tabla($values, "lbl", $value)
                    );
                }
                if($this->_dame_tabla($values, $value, $value)=="text"){
                    $array_return[$value] = array(
                        "type" => "text",
                        "table" => "NULL",
                        "label" => "NULL",
                        "value" => "NULL",
                        "fm" => $this->_dame_tabla($values, "fm", $value),
                        "wysiwyg" => "NULL",
                        "dtp" => "NULL",
                        "null" => $this->_dame_tabla($values, "null", $value),
                        "invalue" => $this->_dame_tabla($values, "invalue", $value),
                        "lbl" => $this->_dame_tabla($values, "lbl", $value)
                    );
                }

                if($this->_dame_tabla($values, $value, $value)=="hidden"){
                    $array_return[$value] = array(
                        "type" => "text",
                        "table" => "NULL",
                        "label" => "NULL",
                        "value" => "NULL",
                        "fm" => "NULL",
                        "wysiwyg" => "NULL",
                        "dtp" => "NULL",
                        "null" => $this->_dame_tabla($values, "null", $value),
                        "invalue" => $this->_dame_tabla($values, "invalue", $value),
                        "lbl" => $this->_dame_tabla($values, "lbl", $value)
                    );
                }
                if($this->_dame_tabla($values, $value, $value)=="textarea"){
                    $array_return[$value] = array(
                        "type" => "textarea",
                        "table" => "NULL",
                        "label" => "NULL",
                        "value" => "NULL",
                        "fm" => "NULL",
                        "wysiwyg" => $this->_dame_tabla($values, "wysiwyg", $value),
                        "dtp" => "NULL",
                        "null" => $this->_dame_tabla($values, "null", $value),
                        "invalue" => $this->_dame_tabla($values, "invalue", $value),
                        "lbl" => $this->_dame_tabla($values, "lbl", $value)
                    );
                }
                if($this->_dame_tabla($values, $value, $value)=="datepicker"){
                    $array_return[$value] = array(
                        "type" => "datepicker",
                        "table" => "NULL",
                        "label" => "NULL",
                        "value" => "NULL",
                        "fm" => "NULL",
                        "wysiwyg" => "NULL",
                        "dtp" => $this->_dame_tabla($values, "dtp", $value),
                        "null" => $this->_dame_tabla($values, "null", $value),
                        "invalue" => $this->_dame_tabla($values, "invalue", $value),
                        "lbl" => $this->_dame_tabla($values, "lbl", $value)
                    );
                }
                if($this->_dame_tabla($values, $value, $value)=="select"){
                    $array_return[$value] = array(
                        "type" => "select",
                        "table" => "NULL",
                        "label" => "NULL",
                        "value" => "NULL",
                        "fm" => "NULL",
                        "wysiwyg" => "NULL",
                        "dtp" => "NULL",
                        "null" => $this->_dame_tabla($values, "null", $value),
                        "invalue" => $this->_dame_tabla($values, "invalue", $value),
                        "lbl" => $this->_dame_tabla($values, "lbl", $value)
                    );
                }
            }
            return $array_return;
        }else{
            echo "Debe enviar un array.";
        }
    }

    // de acuerdo a los campos enviados por el metodo post
    // y los datos enviados de una consulta, conocemos los campos
    // y llenamos con las caracteristicas globales y personales
    // esta funcion evita mostrar el campo primario
    
    public function _make_the_post_whitout_pri($fields, $values, $primary){
        $array_return = array();
        if(is_array($fields)){
            foreach($fields AS $id => $value){
                if($this->_dame_tabla($values, $value, $value)=="ot"){
                    $array_return[$value] = array(
                        "type" => "ot",
                        "table" => $this->_dame_tabla($values, "tables", $value),
                        "label" => $this->_dame_tabla($values, "labels", $value),
                        "value" => $this->_dame_tabla($values, "values", $value),
                        "fm" => "NULL",
                        "wysiwyg" => "NULL",
                        "dtp" => "NULL",
                        "null" => "NO",
                        "invalue" => $this->_dame_tabla($values, "invalue", $value),
                        "lbl" => $this->_dame_tabla($values, "lbl", $value)
                    );
                }
                if($this->_dame_tabla($values, $value, $value)=="text"){
                    $array_return[$value] = array(
                        "type" => "text",
                        "table" => "NULL",
                        "label" => "NULL",
                        "value" => "NULL",
                        "fm" => $this->_dame_tabla($values, "fm", $value),
                        "wysiwyg" => "NULL",
                        "dtp" => "NULL",
                        "null" => $this->_dame_tabla($values, "null", $value),
                        "invalue" => $this->_dame_tabla($values, "invalue", $value),
                        "lbl" => $this->_dame_tabla($values, "lbl", $value)
                    );
                }

                if($this->_dame_tabla($values, $value, $value)=="hidden"){
                    $array_return[$value] = array(
                        "type" => "text",
                        "table" => "NULL",
                        "label" => "NULL",
                        "value" => "NULL",
                        "fm" => "NULL",
                        "wysiwyg" => "NULL",
                        "dtp" => "NULL",
                        "null" => $this->_dame_tabla($values, "null", $value),
                        "invalue" => $this->_dame_tabla($values, "invalue", $value),
                        "lbl" => $this->_dame_tabla($values, "lbl", $value)
                    );
                }
                if($this->_dame_tabla($values, $value, $value)=="textarea"){
                    $array_return[$value] = array(
                        "type" => "textarea",
                        "table" => "NULL",
                        "label" => "NULL",
                        "value" => "NULL",
                        "fm" => "NULL",
                        "wysiwyg" => $this->_dame_tabla($values, "wysiwyg", $value),
                        "dtp" => "NULL",
                        "null" => $this->_dame_tabla($values, "null", $value),
                        "invalue" => $this->_dame_tabla($values, "invalue", $value),
                        "lbl" => $this->_dame_tabla($values, "lbl", $value)
                    );
                }
                if($this->_dame_tabla($values, $value, $value)=="datepicker"){
                    $array_return[$value] = array(
                        "type" => "datepicker",
                        "table" => "NULL",
                        "label" => "NULL",
                        "value" => "NULL",
                        "fm" => "NULL",
                        "wysiwyg" => "NULL",
                        "dtp" => $this->_dame_tabla($values, "dtp", $value),
                        "null" => $this->_dame_tabla($values, "null", $value),
                        "invalue" => $this->_dame_tabla($values, "invalue", $value),
                        "lbl" => $this->_dame_tabla($values, "lbl", $value)
                    );
                }
                if($this->_dame_tabla($values, $value, $value)=="select"){
                    $array_return[$value] = array(
                        "type" => "select",
                        "table" => "NULL",
                        "label" => "NULL",
                        "value" => "NULL",
                        "fm" => "NULL",
                        "wysiwyg" => "NULL",
                        "dtp" => "NULL",
                        "null" => $this->_dame_tabla($values, "null", $value),
                        "invalue" => $this->_dame_tabla($values, "invalue", $value),
                        "lbl" => $this->_dame_tabla($values, "lbl", $value)
                    );
                }
            }
            return $array_return;
        }else{
            echo "Debe enviar un array.";
        }
    }
    
    // Se envia el nombre de la tabla, y la funcion se encarga de crear las
    // carpetas pertinentes, conociendo la ruta que tambien es enviada, una
    // vez creada la carpeta (por la fuerza), se empieza a hacer el recorrido
    // de las acciones permitidas, las altas, bajas, cambios, etc...
    // y de acuerdo a los campos que hay en el array recibido, conocemos la
    // forma en la que deben generarse los formularios en cada archivo.
    // ninguno de los campos debe enviarse de manera NULA.
    
    public function _execute_everything($tabla, $array, $array_no_pri, $acciones, $ruta){
        if((!is_array($ruta))and(!is_array($tabla))){
            if((is_array($array))and(is_array($acciones))){
                $nruta = $ruta.$tabla."/";
                $this->_force_to_create_a_new_folder($nruta, 0777);
                // Acciones de creación/apertura del archivo
                $filename = $nruta."db.".$tabla.EXT;
                $FD = fopen($filename,"w+");
                chmod($filename, 0666);
                if ($FD === FALSE){
                    echo "Error al abrir/crear el archivo: $filename";
                    return FALSE;
                }
                $this->_make_header_on_class($FD, $tabla);
                $this->_make_the_class($FD, $tabla);
                fwrite($FD,"\n?>");
                fclose($FD);
                foreach($acciones AS $ida => $valuea){
                    if($valuea=="altas"){
                        // El archivo "nuevo"
                        $filename = $nruta."nuevo.".$tabla.EXT;
                        $FD = fopen($filename,"w+");
                        chmod($filename, 0666);
                        if ($FD === FALSE){
                            echo "Error al abrir/crear el archivo: $filename";
                            return FALSE;
                        }
                        $this->_make_the_new($FD,$tabla,$array,$array_no_pri);
                        fclose($FD);
                        // Creando el archivo que recibe las variables
                        // en PHP y las guarda en la base de Datos
                        $filename = $nruta."save.".$tabla.EXT;
                        $FD = fopen($filename,"w+");
                        chmod($filename, 0666);
                        if ($FD === FALSE){
                            echo "Error al abrir/crear el archivo: $filename";
                            return FALSE;
                        }
                        $this->_make_the_save($FD,$tabla,$array);
                        fclose($FD);
                    }elseif($valuea=="listado"){
                        // Creando el archivo de lista
                        $filename = $nruta."lista.".$tabla.EXT;
                        $FD = fopen($filename,"w+");
                        chmod($filename, 0666);
                        if ($FD === FALSE){
                            echo "Error al abrir/crear el archivo: $filename";
                            return FALSE;
                        }
                        $this->_make_the_list_($FD,$tabla,$array);
                        fclose($FD);
                    }elseif($valuea=="edicion"){
                        // armando la edicion
                        // el archivo de la vista
                        $filename = $nruta."editar.".$tabla.EXT;
                        $FD = fopen($filename,"w+");
                        chmod($filename, 0666);
                        if ($FD === FALSE){
                            echo "Error al abrir/crear el archivo: $filename";
                            return FALSE;
                        }
                        $this->_make_the_edit($FD,$tabla,$array,$array_no_pri);
                        fclose($FD);
                        //
                        $filename = $nruta."savechanges.".$tabla.EXT;
                        $FD = fopen($filename,"w+");
                        chmod($filename, 0666);
                        if ($FD === FALSE){
                            echo "Error al abrir/crear el archivo: $filename";
                            return FALSE;
                        }
                        $this->_make_the_save_changes($FD,$tabla,$array);
                        fclose($FD);
                    }elseif($valuea=="vista"){
                        // Creando el archivo PHP que hará la vista
                        // De los campos del array
                        $filename = $nruta."see.".$tabla.EXT;
                        $FD = fopen($filename,"w+");
                        chmod($filename, 0666);
                        if ($FD === FALSE){
                            echo "Error al abrir/crear el archivo: $filename";
                            return FALSE;
                        }
                        $this->_make_the_see($FD,$tabla,$array);
                        fclose($FD);
                    }elseif($valuea=="bajas"){
                        // Creando el archivo que recibe las variables
                        // en PHP y las guarda en la base de Datos
                        $filename = $nruta."eliminar.".$tabla.EXT;
                        $FD = fopen($filename,"w+");
                        chmod($filename, 0666);
                        if ($FD === FALSE){
                            echo "Error al abrir/crear el archivo: $filename";
                            return FALSE;
                        }
                        $this->_make_the_delete($FD,$tabla,$array);
                        fclose($FD);
                    }
                }
                // Haciendo los archivos de "seguridad"
                // Haciendo el index.php vacio.
                $filename = $nruta."index".EXT;
                $FD = fopen($filename,"w+");
                chmod($filename, 0666);
                if ($FD === FALSE){
                    echo "Error al abrir/crear el archivo: $filename";
                    return FALSE;
                }
                fclose($FD);
                // Haciendo el index.html vacio.
                // Haciendo los archivos de "seguridad"
                $filename = $nruta."index.html";
                $FD = fopen($filename,"w+");
                chmod($filename, 0666);
                if ($FD === FALSE){
                    echo "Error al abrir/crear el archivo: $filename";
                    return FALSE;
                }
                fclose($FD);
                // ...
                return TRUE;
            }else{
                echo "Los campos y las acciones deben ser enviadas en un array.";
                return FALSE;
            }
        }else{
            echo "La ruta y/o tabla deben ser enviado(s) en una cadena, no en un array.";
            return FALSE;
        }
        return TRUE;
    }
    
    // Encabezado del archivo que contiene las clases
    
    private function _make_header_on_class($FD, $table){
        fwrite($FD,"<?php\n");
        fwrite($FD,"/*\n");
        fwrite($FD,"*   Las clases a seguir con las básicas requeridas para\n");
        fwrite($FD,"*   el funcionamiento de los A,B,M,S\n");
        fwrite($FD,"*   Powered by OHK\n");
        fwrite($FD,"*/\n");
        fwrite($FD,"\n");
        fwrite($FD,"// Inclusion del arhivo de MYSQLI \n");
        fwrite($FD,"// Que incluye las clases necesarias \n");
        fwrite($FD,"require_once(DBMYSQLI); \n");
        fwrite($FD,"\n");
        fwrite($FD,"// Clase ".$table."\n");
        fwrite($FD,"\n");
        fwrite($FD,"\n");
    }
    
    // Creando la clase completa de acuerdo a los parámetros enviados
    
    private function _make_the_class($FD, $table){
        fwrite($FD,"class $table extends DB{ \n");
        fwrite($FD,"\n");
        fwrite($FD,"\tprivate \$table = \"$table\";\n");
        fwrite($FD,"\n");
        fwrite($FD,"\tpublic function __construct(){\n");
        fwrite($FD,"\t\tDB::__construct(); \n");
        fwrite($FD,"\t}\n");
        fwrite($FD,"\tpublic function _insert_$table(\$fields){\n");
        fwrite($FD,"\t\t\$insert = DB::getInsertSql(\$this->table, \$fields); \n");
        fwrite($FD,"\t\t\$insert = DB::getInsertSql(\$this->table, \$fields); \n");
        fwrite($FD,"\t\t\$last_insert = DB::getInsertId(DB::execute(\$insert)); \n");
        fwrite($FD,"\t\treturn \$last_insert; \n");
        fwrite($FD,"\t}\n");
        fwrite($FD,"\n");
        fwrite($FD,"\tpublic function _select_all_$table(){ \n");
        fwrite($FD,"\t\t\$query_ = DB::_arma_select('*', \$this->table); \n");
        fwrite($FD,"\t\treturn DB::execute(\$query_); \n");
        fwrite($FD,"\t}\n");
        fwrite($FD,"\n");
        fwrite($FD,"\tpublic function _prepare_insert(\$table, \$fields){ \n");
        fwrite($FD,"\t\treturn DB::getInsertSql(\$table, \$fields); \n");
        fwrite($FD,"\t}\n");
        fwrite($FD,"\n");
        fwrite($FD,"\t// Realiza la insercion de la tabla \n");
        fwrite($FD,"\tpublic function _make_insert_$table(\$fields){ \n");
        fwrite($FD,"\t\t\$query_ = DB::_arma_insert(\$fields, \$this->table); \n");
        fwrite($FD,"\t\t\$last_insert = DB::getInsertId(DB::execute(\$query_)); \n");
        fwrite($FD,"\t\treturn \$last_insert; \n");
        fwrite($FD,"\t}\n");
        fwrite($FD,"\n");
        fwrite($FD,"\t// Realiza la actualizacion de campos a la tabla \n");
        fwrite($FD,"\tpublic function _make_update_$table(\$fields, \$where){ \n");
        fwrite($FD,"\t\t\$udp_ = DB::_arma_update(\$this->table); \n");
        fwrite($FD,"\t\t\$set_ = DB::_arma_set(\$fields); \n");
        fwrite($FD,"\t\t\$where_ = DB::_arma_where(\$where); \n");
        fwrite($FD,"\t\t\$query_ = \$udp_.\$set_.\$where_; \n");
        fwrite($FD,"\t\tDB::execute(\$query_); \n");
        fwrite($FD,"\t\treturn TRUE; \n");
        fwrite($FD,"\t}\n");
        fwrite($FD,"\n");
        fwrite($FD,"\t// Realiza la eliminacion fisica de los datos \n");
        fwrite($FD,"\tpublic function _make_delete_$table(\$where){ \n");
        fwrite($FD,"\t\t\$delete_ = DB::_arma_delete(\$this->table); \n");
        fwrite($FD,"\t\t\$where_ = DB::_arma_where(\$where); \n");
        fwrite($FD,"\t\t\$query_ = \$delete_.\$where_; \n");
        fwrite($FD,"\t\tDB::execute(\$query_); \n");
        fwrite($FD,"\t\treturn TRUE; \n");
        fwrite($FD,"\t}\n");
        fwrite($FD,"\n");
        fwrite($FD,"\t// Selecciona los campos de una $table \n");
        fwrite($FD,"\t// De a cuerdo a un ID \n");
        fwrite($FD,"\tpublic function _select_all_".$table."_id(\$id){ \n");
        fwrite($FD,"\t\t\$fields = DB::_arma_select('*', \$this->table); \n");
        fwrite($FD,"\t\t\$where = DB::_arma_where(\$id); \n");
        fwrite($FD,"\t\t\$query_ = \$fields.\$where; \n");
        fwrite($FD,"\t\t\$result = DB::execute(\$query_); \n");
        fwrite($FD,"\t\treturn \$result; \n");
        fwrite($FD,"\t}\n");
        fwrite($FD,"\n");
        fwrite($FD,"\t// Selecciona los campos de una $table \n");
        fwrite($FD,"\t// De acuerdo a varios parametros. \n");
        fwrite($FD,"\tpublic function _select_$table(\$fields='*', \$where=FALSE, \$order=FALSE, \$limit=FALSE, \$table=FALSE){ \n");
        fwrite($FD,"\t\tif(!\$table) \$table = \$this->table; \n");
        fwrite($FD,"\t\t\$fields = DB::_arma_select(\$fields, \$table); \n");
        fwrite($FD,"\t\tif(\$where) \$where = DB::_arma_where(\$where); else \$where = \"\"; \n");
        fwrite($FD,"\t\tif(\$order) \$order = DB::_arma_order(\$order); else \$order = \"\"; \n");
        fwrite($FD,"\t\tif(\$limit) \$limit = DB::_arma_limit(\$limit); else \$limit = \"\"; \n");
        fwrite($FD,"\t\t\$query_ = \$fields.\$where.\$order.\$limit; \n");
        fwrite($FD,"\t\treturn DB::execute(\$query_); \n");
        fwrite($FD,"\t}\n");
        fwrite($FD,"\n");
        fwrite($FD,"\t// Funcion que crea multiples joins\n");
        fwrite($FD,"\n");
        fwrite($FD,"\tpublic function _call_multiple_left_join(\$fields, \$join_table, \$on, \$where){\n");
        fwrite($FD,"\t\t\$query_ = DB::_arma_multiple_join(\$fields, \$this->table, \$join_table, \$on, \$where); \n");
        fwrite($FD,"\t\treturn DB::execute(\$query_); \n");
        fwrite($FD,"\t}\n");
        fwrite($FD,"\n");
        fwrite($FD,"\t// Funcion que ejecuta cualquier consulta\n");
        fwrite($FD,"\n");
        fwrite($FD,"\tpublic function ejecutar(\$query){\n");
        fwrite($FD,"\t\treturn DB::execute(\$query); \n");
        fwrite($FD,"\t}\n");
        fwrite($FD,"\n");
        fwrite($FD,"\t// Muestra errores \n");
        fwrite($FD,"\tpublic function _error_deleting(){ \n");
        fwrite($FD,"\t\techo \"Error al eliminar los datos.\"; \n");
        fwrite($FD,"\t}\n");
        fwrite($FD,"\n");
        fwrite($FD,"}\n");
    }
    
    // Creando el arhivo de guardado de los datos
    
    public function _make_the_save($FD, $table, $array){
        fwrite($FD,"<?php\n");
        fwrite($FD,"// Incluyendo archivo de la base de Datos\n");
        fwrite($FD,"require_once(MODULES.'$table/db.$table'.EXT);\n");
        fwrite($FD,"require(SYSTEM.'helpers/date.code_helper'.EXT);\n");
        fwrite($FD,"// Inicializando la clase de la base de Datos\n");
        fwrite($FD,"\$new = new $table();\n");
        fwrite($FD,"// Recibiendo y limpiando Datos\n");
        // Desglozando el array
        if(is_array($array)){
            $this->_recibe_dependiendo($FD, $array);
            reset($array);
            $s = 1;
            $max = count($array);
            $c = "\$insert = array(";
            foreach($array AS $id => $value){
                if(!in_array("primary", $value)){
                    $c.= "\"$id\" => \"\$$id\"";
                    if($s<$max){
                        $c.= ", ";
                    }else{
                        $c.=");";
                    }
                }
                $s++;
            }
            fwrite($FD,"$c\n");
        }
        $pri = $this->_give_me_the_pri($array);
        fwrite($FD,"\$last_insert = \$new->_make_insert_$table(\$insert);\n");
        fwrite($FD,"header(\"Location: in.php?m=$table&f=lista\");\n");
        //fwrite($FD,"header(\"Location: in.php?m=$table&f=see&$pri=\$last_insert\");\n");
        fwrite($FD,"exit;\n");
        fwrite($FD,"\n");
        fwrite($FD,"?>\n");
        
    }
    
    // Creando el arhivo de guardado de los datos modificados
    
    public function _make_the_save_changes($FD, $table, $array){
        fwrite($FD,"<?php\n");
        fwrite($FD,"// Incluyendo archivo de la base de Datos\n");
        fwrite($FD,"require_once(MODULES.'$table/db.$table'.EXT);\n");
        fwrite($FD,"require(SYSTEM.'helpers/date.code_helper'.EXT);\n");
        fwrite($FD,"// Inicializando la clase de la base de Datos\n");
        fwrite($FD,"\$new = new $table();\n");
        fwrite($FD,"// Recibiendo y limpiando Datos\n");
        // Desglozando el array
        if(is_array($array)){
            $this->_recibe_dependiendo($FD, $array);
            reset($array);
            $s = 1;
            $max = count($array);
            $c = "\$update = array(";
            foreach($array AS $id => $value){
                if(!in_array("primary", $value)){
                    $c.= "\"$id\" => \"\$$id\"";
                    if($s<$max){
                        $c.= ", ";
                    }else{
                        $c.=");";
                    }
                }
                $s++;
            }
            fwrite($FD,"$c\n");
            reset($array);
            $pri = $this->_give_me_the_pri($array);
            fwrite($FD,"\$where = array(\"$pri\" => \"\$$pri\");\n");
        }
        fwrite($FD,"\$new->_make_update_$table(\$update, \$where);\n");
        fwrite($FD,"header(\"Location: in.php?m=$table&f=lista\");\n");
        //fwrite($FD,"header(\"Location: in.php?m=$table&f=see&$pri=\$$pri\");\n");
        fwrite($FD,"exit;\n");
        fwrite($FD,"\n");
        fwrite($FD,"?>\n");
        
    }
    
    // Creando el archivo que elimina los datos
    // de acuerdo a un parametro enviado por _GET
    // que debe ser si o si el dato primario.
    
    public function _make_the_delete($FD, $table, $array){
        fwrite($FD,"<?php\n");
        fwrite($FD,"\n");
        fwrite($FD,"// Incluyendo archivo de la base de Datos\n");
        fwrite($FD,"require_once(MODULES.'$table/db.$table'.EXT);\n");
        fwrite($FD,"// Inicializando la clase de la base de Datos\n");
        fwrite($FD,"\$new = new $table();\n");
        fwrite($FD,"// Recibiendo y limpiando Datos\n");
        $pri = $this->_give_me_the_pri($array);
        fwrite($FD,"\$$pri = addslashes(trim(\$_GET['$pri']));\n");
        fwrite($FD,"\n");
        fwrite($FD,"\$delete = array(\"$pri\" => \"\$$pri\");\n");
        fwrite($FD,"\$new->_make_delete_$table(\$delete);\n");
        fwrite($FD,"\n");
        fwrite($FD,"header(\"Location: in.php?m=$table&f=lista\");\n");
        fwrite($FD,"exit;\n");
        fwrite($FD,"\n");
        fwrite($FD,"?>\n");
    }
    
    // Funcion que expondra el codigo fuente dentro del
    // archivo de la vista para generar una vista "LOL"
    
    public function _make_the_see($FD, $table, $array){
        $tables_ = array();
        $columns_ = array();
        $pri = $this->_give_me_the_pri($array);
        fwrite($FD,"<?php\n");
        fwrite($FD,"\n");
        fwrite($FD,"/*\n");
        fwrite($FD,"*   Vista regular de los campos de una base de datos\n");
        fwrite($FD,"*   Powered by OHK\n");
        fwrite($FD,"*/\n");
        fwrite($FD,"\n");
        fwrite($FD,"require_once(MODULES.'$table/db.$table'.EXT);\n");
        fwrite($FD,"require(SYSTEM.'helpers/date.code_helper'.EXT);\n");
        fwrite($FD,"\$new = new $table();\n");
        fwrite($FD,"\n");
        fwrite($FD,"\$$pri = addslashes(trim(\$_GET['$pri']));\n");
        // Comprobando si es que hay alguna tabla relacionada
        // con otra tabla para hacer los LEFT JOIN correspondiente(s).
        if($this->_is_there_a_ot($array)){
            $tables_ = $this->_give_me_the_tables_ot($array);
            $columns_ = $this->_give_me_the_column_whit_ot($array);
            $labelsandvalues = $this->_give_me_the_labels_and_values($array, $columns_);
            $rest = $this->_give_me_the_rest_of_the_columns($array, $columns_);
            $select_ff = $this->_make_the_select_for_the_joins($array, $rest, $table, $labelsandvalues);
            $tables_jt = $this->_make_the_jt_tables($tables_);
            $on_ = $this->_make_the_on($labelsandvalues, $table);
            fwrite($FD,"\$ff = $select_ff\n");
            fwrite($FD,"\$tt = \"$table\";\n");
            fwrite($FD,"\$jt = $tables_jt\n");
            fwrite($FD,"\$on = $on_\n");
            fwrite($FD,"\n");
            fwrite($FD,"\$where_u = array(\"$table.$pri\" => \"\$$pri\");\n");
            fwrite($FD,"\n");
            fwrite($FD,"\$values = \$new->_call_multiple_left_join(\$ff, \$jt, \$on, \$where_u);\n");
        }else{
            fwrite($FD,"\n");
            fwrite($FD,"\$where_u = array(\"$pri\" => \"\$$pri\");\n");
            fwrite($FD,"\n");
            fwrite($FD,"\$values = \$new->_select_$table('*', \$where_u);\n");
        }
        fwrite($FD,"if(!\$values)echo \$new->error;\n");
        fwrite($FD,"\$new->close();\n");
        fwrite($FD,"?>\n");
        fwrite($FD,"\n");
        fwrite($FD,"\t<script> \n");
        fwrite($FD,"\tfunction eliminar(id){\n");
        fwrite($FD,"\t\tbootbox.confirm(\"Está seguro que desea eliminar la informacion?\",function(result){\n");
        fwrite($FD,"\t\t\tif(result){\n");
        fwrite($FD,"\t\t\t\twindow.location=\"?m=$table&f=eliminar&$pri=\"+id;\n");
        fwrite($FD,"\t\t\t}\n");
        fwrite($FD,"\t\t});\n");
        fwrite($FD,"\t}\n");
        fwrite($FD,"\t</script> \n");
        //fwrite($FD,"\t<br>\n");
        fwrite($FD,"\t<div class=\"panel panel-primary\"> \n");
        fwrite($FD,"\t<div class=\"panel-heading\"><strong> Vista de $table </strong></div> \n");
        fwrite($FD,"\t<div class=\"panel-body\"> \n");
        fwrite($FD,"\t<p>Opciones en $table: </p> \n");
        fwrite($FD,"\t<p> \n");
        fwrite($FD,"\t\t<a href=\"?m=$table&f=lista\" class='btn btn-success'><span class=\"glyphicon glyphicon-list\"></span><span class=\"hidden-xs\"> Listado</span></a> \n");
        fwrite($FD,"\t\t<a href=\"?m=$table&f=nuevo\" class='btn btn-primary'><span class=\"glyphicon glyphicon-plus\"></span><span class=\"hidden-xs\"> Nuevo(a)</span></a> \n");
        fwrite($FD,"\t\t<a href=\"?m=$table&f=editar&$pri=<?= \$$pri; ?>\" class='btn btn-warning'><span class=\"glyphicon glyphicon-pencil\"></span><span class=\"hidden-xs\"> Modificar</span></a> \n");
        fwrite($FD,"\t\t<a href=\"javascript:eliminar(<?= \$$pri; ?>)\" class='btn btn-danger'><span class=\"glyphicon glyphicon-remove\"></span><span class=\"hidden-xs\"> Eliminar</span></a> \n");
        fwrite($FD,"\t</p> \n");
        fwrite($FD,"\t\t<div class=\"panel panel-default\"> \n");
        fwrite($FD,"\t\t\t<div class=\"panel-heading\"><strong>$table</strong></div> \n");
        fwrite($FD,"\t\t\t<div class=\"panel-body\"> \n");
        fwrite($FD,"\t\t\t<p>Vista única de $table</p> \n");
        fwrite($FD,"\t\t\t</div> \n");
        fwrite($FD,"\t\t\t<div class=\"table-responsive\"> \n");
        fwrite($FD,"\t\t\t<?php while(\$row = \$values->fetch_object()){ ?> \n");
        fwrite($FD,"\t\t\t<table class=\"table table-condensed table-bordered table-hover\"> \n");
        fwrite($FD,"\t\t\t\t<tbody> \n");
        // Varias Lineas
        $this->_make_the_list_on_the_see($FD, $array);
        // Dependiendo de las columnas
        fwrite($FD,"\t\t\t\t</tbody> \n");
        fwrite($FD,"\t\t\t</table> \n");
        fwrite($FD,"\t\t\t<?php } ?> \n");
        fwrite($FD,"\t\t</div>\n");
        fwrite($FD,"\t</div>\n");
        fwrite($FD,"</div>\n");
    }
    
    public function _make_the_list_($FD, $table, $array){
        $tables_ = array();
        $columns_ = array();
        $pri = $this->_give_me_the_pri($array);
        fwrite($FD,"<?php\n");
        fwrite($FD,"\n");
        fwrite($FD,"/*\n");
        fwrite($FD,"*   Lista regular de los campos de una base de datos\n");
        fwrite($FD,"*   Powered by OHK\n");
        fwrite($FD,"*/\n");
        fwrite($FD,"\n");
        fwrite($FD,"require_once(MODULES.'$table/db.$table'.EXT);\n");
        fwrite($FD,"require(SYSTEM.'helpers/date.code_helper'.EXT);\n");
        fwrite($FD,"\$new = new $table();\n");
        fwrite($FD,"\n");
        // Comprobando si es que hay alguna tabla relacionada
        // con otra tabla para hacer los LEFT JOIN correspondiente(s).
        if($this->_is_there_a_ot($array)){
            $tables_ = $this->_give_me_the_tables_ot($array);
            $columns_ = $this->_give_me_the_column_whit_ot($array);
            $labelsandvalues = $this->_give_me_the_labels_and_values($array, $columns_);
            $rest = $this->_give_me_the_rest_of_the_columns($array, $columns_);
            $select_ff = $this->_make_the_select_for_the_joins($array, $rest, $table, $labelsandvalues);
            $tables_jt = $this->_make_the_jt_tables($tables_);
            $on_ = $this->_make_the_on($labelsandvalues, $table);
            fwrite($FD,"\$ff = $select_ff\n");
            fwrite($FD,"\$tt = \"$table\";\n");
            fwrite($FD,"\$jt = $tables_jt\n");
            fwrite($FD,"\$on = $on_\n");
            fwrite($FD,"\n");
            fwrite($FD,"\$where_u = \"\";\n");
            fwrite($FD,"\n");
            fwrite($FD,"\$values = \$new->_call_multiple_left_join(\$ff, \$jt, \$on, \$where_u);\n");
        }else{
            fwrite($FD,"\$campos = array('*');\n");
            fwrite($FD,"\$limit = \"0,150\";\n");
            fwrite($FD,"\$where_u = \"\";\n");
            fwrite($FD,"\n");
            fwrite($FD,"\$values = \$new->_select_$table('*', \$where_u);\n");
            fwrite($FD,"\n");
        }

        fwrite($FD,"?>\n");
        fwrite($FD,"\t<script> \n");
        fwrite($FD,"\t$(function(){ \n");
        fwrite($FD,"\t\t$('#sort').dataTable(); \n");
        fwrite($FD,"\t\t$('[title]').tooltip(); \n");
        fwrite($FD,"\t}); \n");
        fwrite($FD,"\tfunction eliminar(id){\n");
        fwrite($FD,"\t\tbootbox.confirm(\"Está seguro que desea eliminar la informacion?\",function(result){\n");
        fwrite($FD,"\t\t\tif(result){\n");
        fwrite($FD,"\t\t\t\twindow.location=\"?m=$table&f=eliminar&$pri=\"+id;\n");
        fwrite($FD,"\t\t\t}\n");
        fwrite($FD,"\t\t});\n");
        fwrite($FD,"\t}\n");
        fwrite($FD,"\t</script> \n");
        fwrite($FD,"\t<br> \n");
        fwrite($FD,"\t<div class=\"panel panel-default\"> \n");
        fwrite($FD,"\t<div class=\"panel-heading\"><strong> Listado General </strong></div> \n");
        fwrite($FD,"\t<div class=\"panel-body\"> \n");
        //fwrite($FD,"\t<p>Para agregar un(a) nuevo(a) haga clic en el botón a seguir: </p> \n");
        fwrite($FD,"\t<p><a href=\"?m=$table&f=nuevo\" class='btn btn-success'><span class=\"glyphicon glyphicon-share\"></span><span class=\"hidden-xs\"> Crear nuevo</span></a></p> \n");
        // fwrite($FD,"\t<div class=\"panel panel-default\"> \n");
        // fwrite($FD,"\t\t<div class=\"panel-heading\"><strong>Listado General</strong></div> \n");
        // fwrite($FD,"\t\t<div class=\"panel-body\"> \n");
        // fwrite($FD,"\t\t<p>Listado de la Tabla.</p> \n");
        fwrite($FD,"\t\t<div class=\"table-responsive\">\n");
        
        fwrite($FD,"\t\t<table class=\"table table-bordered table-condensed table-hover\" id=\"sort\"> \n");
        fwrite($FD,"\t\t\t<thead> \n");
        fwrite($FD,"\t\t\t\t<tr class=\"text-center\"> \n");
        // Empieza lo dinámico
        $this->_make_the_th_on_the_list($FD, $array);
        fwrite($FD,"\t\t\t\t\t<th class=\"text-center\">Opciones</th> \n");
        // Finaliza lo dinámico en los headers
        fwrite($FD,"\t\t\t\t</tr> \n");
        fwrite($FD,"\t\t\t</thead> \n");
        fwrite($FD,"\t\t\t<tbody> \n");
        fwrite($FD,"\t\t\t\t<?php\n");
        fwrite($FD,"\t\t\t\t\$count=0;\n");
        fwrite($FD,"\t\t\t\twhile(\$row = \$values->fetch_object()){ \n");
        fwrite($FD,"\t\t\t\t\t\$count++;\n");
        fwrite($FD,"\t\t\t\t?>\n");
        fwrite($FD,"\t\t\t\t<tr> \n");
        // Empieza lo dinámico
        $this->_make_the_td_on_the_list($FD, $array);

        fwrite($FD,"\t\t\t\t\t<td class=\"text-center\">\n"); /*
        fwrite($FD,"\t\t\t\t\t\t<a href=\"in.php?m=$table&f=see&$pri=<?= \$row->$pri; ?>\" title=\"Ver detalles\"><span class=\"glyphicon glyphicon-eyelistaa>\n");
        //fwrite($FD,"\t\t\t\t\t\t<a href=\"in.php?m=$table&f=see&$pri=<?= \$row->$pri; ?>\" title=\"Ver detalles\"><span 
        */
        fwrite($FD,"\t\t\t\t\t\t<a href=\"?m=$table&f=editar&$pri=<?= \$row->$pri; ?>\" title=\"Modificar\" class=\"btn btn-warning btn-xs\"><span class=\"glyphicon glyphicon-pencil\"></span> Modificar</a>\n");
        fwrite($FD,"\t\t\t\t\t\t<a href=\"javascript:eliminar(<?= \$row->$pri; ?>)\" title=\"Eliminar\" class=\"btn btn-danger btn-xs\"><span class=\"glyphicon glyphicon-remove\"></span> Eliminar</a>\n");
        fwrite($FD,"\t\t\t\t\t</td>\n");
        // Finaliza lo dinámico en los datos
        fwrite($FD,"\t\t\t\t</tr> \n");
        fwrite($FD,"\t\t\t\t<?php } ?> \n");
        fwrite($FD,"\t\t\t</tbody> \n");
        fwrite($FD,"\t\t</table> \n");
        fwrite($FD,"\t\t</div> \n");
        fwrite($FD,"\t</div> \n");
        //fwrite($FD,"</div> \n");
        //fwrite($FD,"</div> \n");
    }

    // Funcion que crea el archivo "nuevo"

    public function _make_the_new($FD, $table, $array, $array_no_pri){
        $pri = $this->_give_me_the_pri($array);
        $comp = $this->_give_me_the_pri_($array);
        $vals = array_keys($array);
        // Los campos menos la llave primaria
        $compare = array_diff($vals,$comp);
        $first = $this->_give_me_the_first_($compare);
        fwrite($FD,"<?php\n");
        fwrite($FD,"require(SYSTEM.'helpers/date.code_helper'.EXT);\n");
        if($this->_is_there_a_ot($array)){
            fwrite($FD,"require_once(MODULES.'$table/db.$table'.EXT);\n");
            fwrite($FD,"\n");
            fwrite($FD,"\$pro = new $table();\n");
            $columns_ = $this->_give_me_the_column_whit_ot($array);
            $labelsandvalues = $this->_give_me_the_labels_and_values($array, $columns_);
            $this->_make_the_selects_($FD, $labelsandvalues, $table);
        }
        fwrite($FD,"?>\n");
        fwrite($FD,"\t<script> \n");
        fwrite($FD,"\t$(function(){ \n");
        fwrite($FD,"\t\t$('#$first').select(); \n");
        if($this->_is_there_a_datepicker($array)){
            $dtp = $this->_give_me_the_datepicker_($array);
            fwrite($FD,"\t\t\$('#$dtp').datepicker({ \n");
            fwrite($FD,"\t\tshowOtherMonths: true,\n");
            fwrite($FD,"\t\tselectOtherMonths: true,\n");
            fwrite($FD,"\t\tdateFormat: 'dd-mm-yy'\n");
            fwrite($FD,"\t\t});\n");
        }
        fwrite($FD,"\t\t\$.validate(); \n");
        fwrite($FD,"\t}); \n");
        fwrite($FD,"\t</script> \n");
        fwrite($FD,"\t<br>\n");
        fwrite($FD,"\t<div class=\"panel panel-default\"> \n");
        fwrite($FD,"\t<div class=\"panel-heading\"><strong> Ingresando nueva informacion </strong></div> \n");
        fwrite($FD,"\t<div class=\"panel-body\"> \n");
        /*fwrite($FD,"\t<p> \n");
        fwrite($FD,"\t<br> \n");
        fwrite($FD,"\t<div class=\"panel panel-default\"> \n");
        fwrite($FD,"\t\t<div class=\"panel-heading\"><strong>$table</strong></div> \n");
        fwrite($FD,"\t\t<div class=\"panel-body\"> \n");
        fwrite($FD,"\t\t<p>A continuación llene la $table.</p> \n");
        fwrite($FD,"\t\t</div> \n");*/
        fwrite($FD,"\t\t<form class=\"container\" name=\"form1\" method=\"post\" id=\"formid\" action=\"?m=$table&f=save\"> \n");
        fwrite($FD,"\t\t\t<p> \n");
        // Codigo que genera los inputs
        $this->_make_the_inputs($FD, $array, $array_no_pri);
        // Codigo que genera los inputs
        fwrite($FD,"\t\t\t</p> \n");
        fwrite($FD, "\t\t\t<p>\n");
        fwrite($FD, "\t\t\t\t<button type=\"submit\" name=\"Enviar\" class=\"btn btn-success\" /><span class=\"glyphicon glyphicon-floppy-disk\"></span> <span class=\"hidden-xs\">Guardar Informacion</span></button> <a href=\"?m=$table&f=lista\" class=\"btn btn-danger\"><span class=\"glyphicon glyphicon-ban-circle\"></span><span class=\"hidden-xs\"> Cancelar</span></a>\n");
        fwrite($FD, "\t\t\t</p>\n");
        fwrite($FD,"\t\t</form> \n");
        fwrite($FD,"\t</div> \n");
        /*fwrite($FD,"\t</p> \n");
        fwrite($FD,"\t</div> \n");*/
    }

    // Funcion que crea el archivo "editar"

    public function _make_the_edit($FD, $table, $array, $array_no_pri){
        $pri = $this->_give_me_the_pri($array);
        $comp = $this->_give_me_the_pri_($array);
        $vals = array_keys($array);
        // Los campos menos la llave primaria
        $compare = array_diff($vals,$comp);
        $first = $this->_give_me_the_first_($compare);
        fwrite($FD,"<?php\n");
        fwrite($FD,"require(SYSTEM.'helpers/date.code_helper'.EXT);\n");
        fwrite($FD,"require_once(MODULES.'$table/db.$table'.EXT);\n");
        if($this->_is_there_a_ot($array)){
            fwrite($FD,"\n");
            fwrite($FD,"\$pro = new $table();\n");
            $columns_ = $this->_give_me_the_column_whit_ot($array);
            $labelsandvalues = $this->_give_me_the_labels_and_values($array, $columns_);
            $this->_make_the_selects_($FD, $labelsandvalues, $table);
        }
        fwrite($FD,"// Recibiendo variable\n");
        fwrite($FD,"\$$pri = addslashes(trim(\$_GET['$pri']));\n");
        fwrite($FD, "\n");
        fwrite($FD, "\$news = new $table();\n");
        fwrite($FD, "\n");
        fwrite($FD,"// Estableciendo parametro recibido\n");
        fwrite($FD,"\$where = array(\"$pri\" => \"\$$pri\");\n");
        fwrite($FD,"\$values = \$news->_select_$table('*', \$where);\n");
        fwrite($FD,"if(!\$values)echo \$news->error;\n");
        fwrite($FD,"\$news->close();\n");
        fwrite($FD,"?>\n");
        fwrite($FD,"\t<script> \n");
        fwrite($FD,"\t$(function(){ \n");
        fwrite($FD,"\t\t$('#$first').select(); \n");
        if($this->_is_there_a_datepicker($array)){
            $dtp = $this->_give_me_the_datepicker_($array);
            fwrite($FD,"\t\t\$('#$dtp').datepicker({ \n");
            fwrite($FD,"\t\tshowOtherMonths: true,\n");
            fwrite($FD,"\t\tselectOtherMonths: true,\n");
            fwrite($FD,"\t\tdateFormat: 'dd-mm-yy'\n");
            fwrite($FD,"\t\t});\n");
        }
        fwrite($FD,"\t\t\$.validate(); \n");
        fwrite($FD,"\t}); \n");
        fwrite($FD,"\t</script> \n");
        fwrite($FD,"\t<br>\n");
        fwrite($FD,"\t<div class=\"panel panel-default\"> \n");
        fwrite($FD,"\t<div class=\"panel-heading\"><strong> Modificando la informacion </strong></div> \n");
        fwrite($FD,"\t<div class=\"panel-body\"> \n");
        /*fwrite($FD,"\t<p> \n");
        fwrite($FD,"\t<br> \n");
        fwrite($FD,"\t<div class=\"panel panel-default\"> \n");
        fwrite($FD,"\t\t<div class=\"panel-heading\"><strong>$table</strong></div> \n");
        fwrite($FD,"\t\t<div class=\"panel-body\"> \n");
        fwrite($FD,"\t\t<p>A continuación llene la $table.</p> \n");
        fwrite($FD,"\t\t</div> \n");*/
        fwrite($FD,"\t\t<?php while(\$row = \$values->fetch_object()){ ?>\n");
        fwrite($FD,"\t\t<form class=\"container\" name=\"form1\" method=\"post\" id=\"formid\" action=\"?m=$table&f=savechanges\"> \n");
        fwrite($FD,"\t\t\t<p> \n");
        // Codigo que genera los inputs
        $this->_make_the_inputs_edit($FD, $array, $array_no_pri);
        // Codigo que genera los inputs
        fwrite($FD,"\t\t\t</p> \n");
        fwrite($FD, "\t\t\t<p>\n");
        fwrite($FD, "\t\t\t\t<input type=\"hidden\" name=\"$pri\" id=\"$pri\" value=\"<?= \$$pri; ?>\"/>\n");
        fwrite($FD, "\t\t\t\t<button type=\"submit\" name=\"Enviar\" class=\"btn btn-success\" /><span class=\"glyphicon glyphicon-floppy-disk\"></span> <span class=\"hidden-xs\">Guardar Cambios</span></button> <a href=\"?m=$table&f=lista\" class=\"btn btn-danger\"><span class=\"glyphicon glyphicon-ban-circle\"></span> <span class=\"hidden-xs\">Cancelar</span></a>\n");
        fwrite($FD, "\t\t\t</p>\n");
        fwrite($FD,"\t\t</form> \n"); 
        fwrite($FD,"\t\t<?php } ?>\n"); 
        fwrite($FD,"\t</div> \n");
        /*fwrite($FD,"\t</p> \n");
        fwrite($FD,"\t</div> \n");*/
    }

    // Realiza la recepcion de variables.
    
    public function _recibe_dependiendo($FD, $array){
        foreach($array AS $id => $value){
            if(is_array($value)){
                foreach($value AS $idd => $valuee){
                    if($idd=="type"){
                        switch($valuee){
                            case "text":
                            fwrite($FD,"\$$id = htmlspecialchars(\$_POST['$id'], ENT_QUOTES);\n");
                            break;
                            case "textarea":
                            fwrite($FD,"\$$id = htmlspecialchars(\$_POST['$id'], ENT_QUOTES);\n");
                            break;
                            case "ot":
                            fwrite($FD,"\$$id = \$_POST['$id'];\n");
                            break;
                            case "hidden":
                            fwrite($FD,"\$$id = \$_POST['$id'];\n");
                            break;
                            case "datepicker":
                            fwrite($FD,"\$$id = formatear_en(\$_POST['$id']);\n");
                            break;
                            case "select":
                            fwrite($FD,"\$$id = \$_POST['$id'];\n");
                            break;
                            case "primary":
                            fwrite($FD,"\$$id = \$_POST['$id'];\n");
                            break;
                        }
                    }
                }
                //fwrite($FD,"\n");
            }else{
                exit("Error recogiendo los datos.");
            }
        }
    }
    
    // Funcion que retorna el campo primario del array en otro array
    // para usarlo y compararlo con otro array :P

    public function _give_me_the_pri_($array){
        $narray = array();
        if(is_array($array)){
            foreach($array AS $key => $value){
                if(is_array($value)){
                    foreach($value AS $ket => $valuet){
                        if($valuet=="primary"){
                            $narray[] = $key;
                            return $narray;
                        }
                    }
                    echo "No se ha encontrado una llave primaria en la tabla.";
                    return FALSE;
                }else{
                    echo "Debe enviar un array multiple procesado para concer la llave primaria.";
                    return FALSE;
                }
            }
        }else{
            echo "Debe enviar un array multiple procesado para concer la llave primaria.";
            return FALSE;
        }
    }

    // Funcion que retorna el campo primario del array de columnas
    
    public function _give_me_the_pri($array){
        if(is_array($array)){
            foreach($array AS $key => $value){
                if(is_array($value)){
                    foreach($value AS $ket => $valuet){
                        if($valuet=="primary"){
                            return $key;
                        }
                    }
                    echo "No se ha encontrado una llave primaria en la tabla.";
                    return FALSE;
                }else{
                    echo "Debe enviar un array multiple procesado para concer la llave primaria.";
                    return FALSE;
                }
            }
        }else{
            echo "Debe enviar un array multiple procesado para concer la llave primaria.";
            return FALSE;
        }
    }
    
    // Funcion que retorna un valor positivo en caso de que exista
    // seleccionado un datepicker para el textfield(s)
    
    public function _is_there_a_datepicker($array){
        if(is_array($array)){
            foreach($array AS $id => $value){
                if(is_array($value)){
                    foreach($value AS $idv => $vv){
                        if(($idv=="dtp")and($vv=="on")){
                            return TRUE;
                        }
                    }
                }else{
                    return FALSE;
                }
            }
        }else{
            return FALSE;
        }
    }

    // Funcion que retorna el campo que lleva el
    // datepicker

    public function _give_me_the_datepicker_($array){
        if(is_array($array)){
            foreach($array AS $id => $value){
                if(is_array($value)){
                    foreach($value AS $idv => $vv){
                        if(($idv=="dtp")and($vv=="on")){
                            return $id;
                        }
                    }
                }else{
                    return FALSE;
                }
            }
        }else{
            return FALSE;
        }
    }
    
    // Funcion que retorna un valor positivo en caso de que exista
    // seleccionado un formato número para el textfield(s)
    
    public function _is_there_a_fm($array){
        if(is_array($array)){
            foreach($array AS $id => $value){
                if(is_array($value)){
                    foreach($value AS $idv => $vv){
                        if(($idv=="fm")and($vv=="on")){
                            return TRUE;
                        }
                    }
                }else{
                    return FALSE;
                }
            }
        }else{
            return FALSE;
        }
    }
    
    // Funcion que retorna un valor positivo en caso de que exista
    // seleccionado un wysiwyg para el textfield(s)
    
    public function _is_there_a_wysiwyg($array){
        if(is_array($array)){
            foreach($array AS $id => $value){
                if(is_array($value)){
                    foreach($value AS $idv => $vv){
                        if(($idv=="wysiwyg")and($vv=="on")){
                            return TRUE;
                        }
                    }
                }else{
                    return FALSE;
                }
            }
        }else{
            return FALSE;
        }
    }

    // Funcion que retorna en positivo si el campo enviado
    // pertenece o no a una seleccion positiva del wysiwyg

    public function _is_this_a_wysiwyg_on($array, $dato){
        if(is_array($array)){
            foreach($array AS $id => $value){
                if($id == $dato){                    
                    foreach($value AS $idv => $vv){
                        if(($idv=="wysiwyg")and($vv=="on")){
                            return TRUE;
                        }
                    }
                }
            }
        }else{
            return FALSE;
        }
    }

    // Funcion que retorna en positivo si el campo enviado
    // pertenece o no a una seleccion positiva del ot

    public function _is_this_a_ot_on($array, $dato){
        if(is_array($array)){
            foreach($array AS $id => $value){
                if($id == $dato){
                    foreach($value AS $idv => $vv){
                        if($idv=="ot"){
                            return TRUE;
                        }
                    }
                }
            }
        }else{
            return FALSE;
        }
    }
    
    // Funcion que retorna un valor positivo en caso de que exista
    // seleccionado una tabla que se relacione con otra tabla
    // Esta funcion solo busca saber si existe al menos un campo relacionado
    
    public function _is_there_a_ot($array){
        if(is_array($array)){
            foreach($array AS $id => $value){
                if(is_array($value)){
                    foreach($value AS $idv => $vv){
                        if($vv=="ot"){
                            return TRUE;
                        }
                    }
                }else{
                    return FALSE;
                }
            }
        }else{
            return FALSE;
        }
    }

    public function _is_there_a_select($array){
        if(is_array($array)){
            foreach($array AS $id => $value){
                if(is_array($value)){
                    foreach($value AS $idv => $vv){
                        if($vv=="select"){
                            return TRUE;
                        }
                    }
                }else{
                    return FALSE;
                }
            }
        }else{
            return FALSE;
        }
    }
    
    // Funcion que retorna un valor positivo en caso de que exista
    // seleccionado una tabla que se relacione con otra tabla
    // Esta funcion cuenta la cantidad de relaciones existentes
    
    public function _how_many_ot_exists($array){
        $contador = 0;
        if(is_array($array)){
            foreach($array AS $id => $value){
                foreach($value AS $idv => $vv){
                    if($vv=="ot"){
                        $contador++;
                    }
                }
            }
            return $contador;
        }else{
            return FALSE;
        }
    }

    // Funcion que retorna en positivo si el campo enviado
    // pertenece o no a una seleccion positiva del null

    public function _is_this_a_null_on($array, $dato){
        if(is_array($array)){
            foreach($array AS $id => $value){
                if($id == $dato){
                    foreach($value AS $idv => $vv){
                        if(($idv=="null")and($vv=="YES")){
                            return TRUE;
                        }
                    }
                }
            }
        }else{
            return FALSE;
        }
    }

    // Funcion que retorna en positivo si el campo enviado
    // pertenece o no a una seleccion positiva del datepicker

    public function _is_this_a_datepicker_on($array, $dato){
        if(is_array($array)){
            foreach($array AS $id => $value){
                if($id == $dato){
                    foreach($value AS $idv => $vv){
                        if(($idv=="dtp")and($vv=="on")){
                            return TRUE;
                        }
                    }
                }
            }
        }else{
            return FALSE;
        }
    }

    // Function que retorna la columna de la tabla que lleva
    // dentro de ella el ot (osea la tabla que tiene relacion)
    // con otra tabla

    public function _give_me_the_column_whit_ot($array){
        $narray = array();
        if(is_array($array)){
            foreach ($array as $key => $value) {
                if(in_array("ot", $value)){
                    $narray[] = "$key";
                }
            }
            return $narray;
        }else{
            echo "El valor enviado debe ser un array preformateado.";
            return FALSE;
        }
    }
    
    // Funcion que identifica todos los labels y values
    // de los campos que tienen las relaciones

    public function _give_me_the_labels_and_values($array,$column){
        $cols = array();
        if(is_array($array)){
            foreach ($array as $key => $value) {
                if(in_array($key,$column)){
                    foreach ($value as $keys => $values) {
                        if($keys=="table"){
                            $cols[$key]["table"] = $values;
                        }
                        if($keys=="label"){
                            $cols[$key]["label"] = $values;
                        }
                        if($keys=="value"){
                            $cols[$key]["value"] = $values;
                        }
                    }
                }
            }
            return $cols;
        }else{
            echo "El valor enviado debe ser un array preformateado.";
            return FALSE;
        }
    }

    // Funcion que retorna todos los campos que son naturales
    // y no tienen ninguna relación y/o dependencia con otra tabla

    public function _give_me_the_rest_of_the_columns($array, $columns){
        $rest = array();
        if(is_array($array)){
            foreach ($array as $key => $value) {
                if(!in_array($key, $columns)){
                    $rest[] = $key;
                }
            }
            return $rest;
        }else{
            echo "El valor enviado debe ser un array preformateado.";
            return FALSE;
        }
    }

    // Funcion que retorna un array de tablas
    // que estan relacionadas en el array "ot"
    
    public function _give_me_the_tables_ot($array){
        $narray = array();
        if(is_array($array)){
            foreach($array AS $key => $value){
                if(is_array($value)){
                    if($value["table"]!="NULL"){
                        $narray[] = $value["table"];
                    }
                }
            }
            return $narray;
        }else{
            return FALSE;
        }
    }

    // Funcion que arma el array de los campos a seleccionar

    public function _make_the_select_for_the_joins($array, $rest, $table, $labelsandvalues){
        $return = "array(";
        foreach ($rest as $key => $value) {
            $return .= "\"$table.$value\", ";
        }
        $ini = 0;
        $max = count($labelsandvalues);
        foreach ($labelsandvalues as $keys => $values) {
            foreach ($values as $key => $value) {
                if($key=="table"){
                    $return .= "\"$value.";
                }
                if($key=="label"){
                    $return .= "$value\"";
                }
            }
            $ini++;
            if($ini<$max){
                $return .= ", ";
            }
        }
        $return .= ");";
        return $return;
    }

    // Funcion que arma las tablas relacionadas para el LEFT JOIN $jt;

    public function _make_the_jt_tables($tables){
        $jt = "array(";
        $ini = 0;
        $max = count($tables);
        if(is_array($tables)){
            foreach ($tables as $key => $value) {
                $jt .= "\"$value\"";
                $ini++;
                if($ini<$max){
                    $jt .= ", ";
                }
            }
            $jt .= ");";
        }else{
            echo "El valor enviado debe ser un array preformateado.";
            return FALSE;
        }
        return $jt;
    }

    // Funcion que arma el ON para los campos de la tabla

    public function _make_the_on($labelsandvalues, $table){
        $on = "array(\n";
        if(is_array($labelsandvalues)){
            $ini = 0;
            $max = count($labelsandvalues);
            foreach ($labelsandvalues as $key => $value) {
                foreach ($value as $keys => $values) {
                    if($keys=="table"){
                        $on .= "\"$values";
                    }
                    if($keys=="value"){
                        $on .= ".$values\" ";
                        $on .= "=> \"$table.$values\" ";
                    }
                }
                $ini++;
                if($ini<$max){
                    $on .= ", \n";
                }
            }
            $on .= "\n );";
        }else{
            echo "El valor enviado debe ser un array preformateado.";
            return FALSE;
        }
        return $on;
    }

    public function _make_the_show($keyy, $array){
        if(is_array($array)){
            foreach ($array as $key => $value) {
                if($key == $keyy){
                    foreach ($value as $keys => $values) {
                        switch($values){
                            case "text":
                            return "<?= htmlspecialchars_decode(\$row->$key, ENT_QUOTES); ?>";
                            break;
                            case "textarea":
                            return "<?= htmlspecialchars_decode(\$row->$key, ENT_QUOTES); ?>";
                            break;
                            case "ot":
                            $ya = $this->_give_me_the_column($array, $key);
                            return "<?= \$row->$ya; ?>";
                            break;
                            case "hidden":
                            return "<?= \$row->$key; ?>";
                            break;
                            case "datepicker":
                            return "<?= formato_letra_es(\$row->$key); ?>";
                            break;
                            case "select":
                            return "<?= \$row->$key; ?>";
                            break;
                            case "primary":
                            return "<?= \$count; ?>";
                            break;
                        }
                    }
                }
            }
        }else{
            return FALSE;
        }
    }

    public function _make_the_show_see($keyy, $array){
        if(is_array($array)){
            foreach ($array as $key => $value) {
                if($key == $keyy){
                    foreach ($value as $keys => $values) {
                        switch($values){
                            case "text":
                            return "<?= htmlspecialchars_decode(\$row->$key, ENT_QUOTES); ?>";
                            break;
                            case "textarea":
                            return "<?= htmlspecialchars_decode(\$row->$key, ENT_QUOTES); ?>";
                            break;
                            case "ot":
                            $ya = $this->_give_me_the_column($array, $key);
                            return "<?= \$row->$ya; ?>";
                            break;
                            case "hidden":
                            return "<?= \$row->$key; ?>";
                            break;
                            case "datepicker":
                            return "<?= formato_letra_es(\$row->$key); ?>";
                            break;
                            case "select":
                            return "<?= \$row->$key; ?>";
                            break;
                            case "primary":
                            return "<?= \$row->$key; ?>";
                            break;
                        }
                    }
                }
            }
        }else{
            return FALSE;
        }
    }

    public function _make_the_list($keyy, $array){
        if(is_array($array)){
            foreach ($array as $key => $value) {
                if($key == $keyy){
                    foreach ($value as $keys => $values) {
                        switch($values){
                            case "text":
                            return "<?= htmlspecialchars_decode(\$row->$key, ENT_QUOTES); ?>";
                            break;
                            case "textarea":
                            return "<?= htmlspecialchars_decode(\$row->$key, ENT_QUOTES); ?>";
                            break;
                            case "ot":
                            $ya = $this->_give_me_the_column($array, $key);
                            return "<?= \$row->$ya; ?>";
                            break;
                            case "hidden":
                            return "<?= \$row->$key; ?>";
                            break;
                            case "datepicker":
                            return "<?= formato_letra_es(\$row->$key); ?>";
                            break;
                            case "select":
                            return "<?= \$row->$key; ?>";
                            break;
                            case "primary":
                            return "<?= \$row->$key; ?>";
                            break;
                        }
                    }
                }
            }
        }else{
            return FALSE;
        }
    }

    // Funcion que se encarga de imprimir los
    // inputs de un insert nuevo

    public function _make_the_inputs($FD, $array, $array_no_pri){
        foreach ($array_no_pri as $keys => $values) {
            $kyy = ucfirst($values['lbl']);
            fwrite($FD, "\t\t\t\t<p> \n");
            fwrite($FD, "\t\t\t\t<label for=\"$keys\"> \n");
            fwrite($FD, "\t\t\t\t\t<strong>$kyy:</strong> \n");
            $this->_make_the_input_($FD, $keys, $array_no_pri);
            fwrite($FD, "\t\t\t\t</label> \n");
            fwrite($FD, "\t\t\t\t</p> \n");
        }
        if($this->_is_there_a_wysiwyg($array)){
            fwrite($FD, "\t\t\t\t<script>\n");
            fwrite($FD, "\t\t\t\t\$('.jqte-test').jqte();\n");
            fwrite($FD, "\t\t\t\t// settings of status\n");
            fwrite($FD, "\t\t\t\tvar jqteStatus = true;\n");
            fwrite($FD, "\t\t\t\t\$(\".status\").click(function()\n");
            fwrite($FD, "\t\t\t\t{\n");
            fwrite($FD, "\t\t\t\t\tjqteStatus = jqteStatus ? false : true;\n");
            fwrite($FD, "\t\t\t\t\t\$('.jqte-test').jqte({\"status\" : jqteStatus})\n");
            fwrite($FD, "\t\t\t\t});\n");
            fwrite($FD, "\t\t\t\t</script>\n");
        }
    }

    public function _make_the_inputs_edit($FD, $array, $array_no_pri){
        foreach ($array_no_pri as $keys => $values) {
            $kyy = ucfirst($values['lbl']);
            fwrite($FD, "\t\t\t\t<p> \n");
            fwrite($FD, "\t\t\t\t<label for=\"$keys\"> \n");
            fwrite($FD, "\t\t\t\t\t<strong>$kyy:</strong> \n");
            $this->_make_the_input_edit($FD, $keys, $array_no_pri);
            fwrite($FD, "\t\t\t\t</label> \n");
            fwrite($FD, "\t\t\t\t</p> \n");
        }
        if($this->_is_there_a_wysiwyg($array)){
            fwrite($FD, "\t\t\t\t<script>\n");
            fwrite($FD, "\t\t\t\t\$('.jqte-test').jqte();\n");
            fwrite($FD, "\t\t\t\t// settings of status\n");
            fwrite($FD, "\t\t\t\tvar jqteStatus = true;\n");
            fwrite($FD, "\t\t\t\t\$(\".status\").click(function()\n");
            fwrite($FD, "\t\t\t\t{\n");
            fwrite($FD, "\t\t\t\t\tjqteStatus = jqteStatus ? false : true;\n");
            fwrite($FD, "\t\t\t\t\t\$('.jqte-test').jqte({\"status\" : jqteStatus})\n");
            fwrite($FD, "\t\t\t\t});\n");
            fwrite($FD, "\t\t\t\t</script>\n");
        }
    }

    // Funcion que se encarga en retornar el input
    // dependiendo de lo que requiera
    // text, textarea, select, datepicker, hidden, etc

    public function _make_the_input_($FD, $keyy, $array){
        $akey[] = $keyy;
        if(is_array($array)){
            foreach ($array as $key => $value) {
                if($key == $keyy){
                    foreach ($value as $keys => $values) {
                        if($keys=="type"){
                            switch($values){
                                case "text":
                                if(!$this->_is_this_a_null_on($array, $keyy)){
                                    fwrite($FD, "\t\t\t\t\t<input type=\"text\" name=\"$keyy\" id=\"$keyy\" data-validation=\"required\" class=\"form-control\" size=\"40\" />\n");
                                }else{
                                    fwrite($FD, "\t\t\t\t\t<input type=\"text\" name=\"$keyy\" id=\"$keyy\" class=\"form-control\" size=\"40\" />\n");
                                }
                                break;
                                case "textarea":
                                if($this->_is_this_a_wysiwyg_on($array, $keyy)){
                                    fwrite($FD, "\t\t\t\t\t<textarea name=\"$keyy\" id=\"$keyy\" class=\"jqte-test\" rows=\"5\" cols=\"55\"></textarea> \n");
                                }elseif(!$this->_is_this_a_null_on($array, $keyy)) {
                                    fwrite($FD, "\t\t\t\t\t<textarea name=\"$keyy\" id=\"$keyy\" class=\"form-control\" data-validation=\"required\" rows=\"5\" cols=\"55\"></textarea> \n");
                                }else{
                                    fwrite($FD, "\t\t\t\t\t<textarea name=\"$keyy\" id=\"$keyy\" class=\"form-control\" rows=\"5\" cols=\"55\"></textarea> \n");
                                }
                                break;
                                case "ot":
                                $labelsandvalues = $this->_give_me_the_labels_and_values($array, $akey);
                                $tableot = $labelsandvalues[$keyy]['table'];
                                $labelot = $labelsandvalues[$keyy]['label'];
                                $valueot = $labelsandvalues[$keyy]['value'];
                                fwrite($FD, "\t\t\t\t\t<select name=\"$keyy\" id=\"$keyy\" class=\"form-control\"> \n");
                                fwrite($FD, "\t\t\t\t\t<?php while(\$row_$tableot = \$value_$tableot -> fetch_object()){ ?>\n");
                                fwrite($FD, "\t\t\t\t\t\t<option value=\"<?= \$row_$tableot->$valueot; ?>\"><?= \$row_$tableot->$labelot; ?></option> \n");
                                fwrite($FD, "\t\t\t\t\t<?php } ?>\n");
                                fwrite($FD, "\t\t\t\t\t</select> \n");
                                break;
                                case "hidden":
                                fwrite($FD, "\t\t\t\t\t<input type=\"hidden\" name=\"$keyy\" id=\"$keyy\"/> \n");
                                break;
                                case "datepicker":
                                if((!$this->_is_this_a_null_on($array, $keyy))and($this->_is_this_a_datepicker_on($array, $keyy))){
                                    fwrite($FD, "\t\t\t\t\t<input type=\"text\" name=\"$keyy\" id=\"$keyy\" data-validation=\"date\" data-validation-format=\"dd-mm-yyyy\" class=\"form-control\" value=\"<?= date(\"d-m-Y\"); ?>\" /> \n");
                                }elseif($this->_is_this_a_datepicker_on($array, $keyy)){
                                    fwrite($FD, "\t\t\t\t\t<input type=\"text\" name=\"$keyy\" id=\"$keyy\" class=\"form-control\" data-validation-format=\"dd-mm-yyyy\" value=\"<?= date(\"d-m-Y\"); ?>\" /> \n");
                                }
                                break;
                                case "select":
                                $data_process = $this->_process_the_enum($this->_give_me_the_values_($array, $keyy));
                                fwrite($FD, "\t\t\t\t\t<select name=\"$keyy\" id=\"$keyy\" class=\"form-control\"> \n");
                                    foreach ($data_process as $key_p => $value_p) {
                                        fwrite($FD, "\t\t\t\t\t\t<option value=\"<?= $value_p; ?>\"><?= $value_p; ?></option> \n");
                                    }
                                fwrite($FD, "\t\t\t\t\t</select> \n");
                                break;
                            }
                        }
                    }
                }
            }
        }else{
            return FALSE;
        }
    }

    // Funcion que se encarga en retornar el input
    // dependiendo de lo que requiera
    // text, textarea, select, datepicker, hidden, etc

    public function _make_the_input_edit($FD, $keyy, $array){
        $akey[] = $keyy;
        if(is_array($array)){
            foreach ($array as $key => $value) {
                if($key == $keyy){
                    foreach ($value as $keys => $values) {
                        if($keys=="type"){
                            switch($values){
                                case "text":
                                if(!$this->_is_this_a_null_on($array, $keyy)){
                                    fwrite($FD, "\t\t\t\t\t<input type=\"text\" name=\"$keyy\" id=\"$keyy\" data-validation=\"required\" class=\"form-control\" value=\"<?= htmlspecialchars_decode(\$row->$keyy, ENT_QUOTES); ?>\"/> \n");
                                }else{
                                    fwrite($FD, "\t\t\t\t\t<input type=\"text\" name=\"$keyy\" id=\"$keyy\" class=\"form-control\" value=\"<?= htmlspecialchars_decode(\$row->$keyy, ENT_QUOTES); ?>\"/>\n");
                                }
                                break;
                                case "textarea":
                                if($this->_is_this_a_wysiwyg_on($array, $keyy)){
                                    fwrite($FD, "\t\t\t\t\t<textarea name=\"$keyy\" id=\"$keyy\" class=\"jqte-test\" rows=\"5\" cols=\"55\"><?= htmlspecialchars_decode(\$row->$keyy, ENT_QUOTES); ?></textarea> \n");
                                }elseif(!$this->_is_this_a_null_on($array, $keyy)) {
                                    fwrite($FD, "\t\t\t\t\t<textarea name=\"$keyy\" id=\"$keyy\" data-validation=\"required\" class=\"form-control\" rows=\"5\" cols=\"55\"><?= htmlspecialchars_decode(\$row->$keyy, ENT_QUOTES); ?></textarea> \n");
                                }else{
                                    fwrite($FD, "\t\t\t\t\t<textarea name=\"$keyy\" id=\"$keyy\" class=\"form-control\" rows=\"5\" cols=\"55\"><?= htmlspecialchars_decode(\$row->$keyy, ENT_QUOTES); ?></textarea> \n");
                                }
                                break;
                                case "ot":
                                $labelsandvalues = $this->_give_me_the_labels_and_values($array, $akey);
                                $tableot = $labelsandvalues[$keyy]['table'];
                                $labelot = $labelsandvalues[$keyy]['label'];
                                $valueot = $labelsandvalues[$keyy]['value'];
                                fwrite($FD, "\t\t\t\t\t<select name=\"$keyy\" id=\"$keyy\" class=\"form-control\"> \n");
                                fwrite($FD, "\t\t\t\t\t<?php while(\$row_$tableot = \$value_$tableot -> fetch_object()){ ?>\n");
                                fwrite($FD, "\t\t\t\t\t\t<option value=\"<?= \$row_$tableot->$valueot; ?>\" <?php if(\$row->$keyy==\$row_$tableot->$valueot){ echo \"selected\"; } ?>><?= \$row_$tableot->$labelot; ?></option> \n");
                                fwrite($FD, "\t\t\t\t\t<?php } ?>\n");
                                fwrite($FD, "\t\t\t\t\t</select> \n");
                                break;
                                case "hidden":
                                fwrite($FD, "\t\t\t\t\t<input type=\"hidden\" name=\"$keyy\" id=\"$keyy\" value=\"<?= htmlspecialchars_decode(\$row->$keyy, ENT_QUOTES); ?>\"/> \n");
                                break;
                                case "datepicker":
                                if((!$this->_is_this_a_null_on($array, $keyy))and($this->_is_this_a_datepicker_on($array, $keyy))){
                                    fwrite($FD, "\t\t\t\t\t<input type=\"text\" name=\"$keyy\" id=\"$keyy\" data-validation=\"date\" data-validation-format=\"dd-mm-yyyy\" class=\"form-control\" value=\"<?= formatear_es(\$row->$keyy); ?>\" /> \n");
                                }elseif($this->_is_this_a_datepicker_on($array, $keyy)){
                                    fwrite($FD, "\t\t\t\t\t<input type=\"text\" name=\"$keyy\" id=\"$keyy\" class=\"form-control\" data-validation-format=\"dd-mm-yyyy\" value=\"<?= formatear_es(\$row->$keyy); ?>\" /> \n");
                                }
                                break;
                                case "select":
                                $data_process = $this->_process_the_enum($this->_give_me_the_values_($array, $keyy));
                                fwrite($FD, "\t\t\t\t\t<select name=\"$keyy\" id=\"$keyy\" class=\"form-control\"> \n");
                                    foreach ($data_process as $key_p => $value_p) {
                                        fwrite($FD, "\t\t\t\t\t\t<option value=\"<?= $value_p; ?>\" <?php if(\$row->$keyy==$value_p){ echo \"selected\"; } ?>><?= $value_p; ?></option> \n");
                                    }
                                fwrite($FD, "\t\t\t\t\t</select> \n");
                                break;
                            }
                        }
                    }
                }
            }
        }else{
            return FALSE;
        }
    }

    // Funcion que procesa el valor enum()
    // y retorna los valores internos en un array

    public function _process_the_enum($enum){
        $enum = str_replace('enum(', '', $enum);
        //$enum = str_replace('\'', '', $enum);
        $enum = str_replace(')', '', $enum);
        $enum = explode(',', $enum);
        return $enum;
    }

    // Funcion que retorna los valores del invalue

    public function _give_me_the_values_($array, $key){
        if(is_array($array)){
            foreach ($array as $key_ => $value) {
                if($key_==$key){
                    foreach ($value as $keys => $values) {
                        if($keys=="invalue"){
                            return $values;
                        }
                    }
                }
            }
        }else{
            return FALSE;
        }
    }

    // Funcion que retorna la columna del "ot"

    public function _give_me_the_column($array, $key){
        if(is_array($array)){
            foreach ($array as $key_ => $value) {
                if($key_==$key){
                    foreach ($value as $keys => $values) {
                        if($keys=="label"){
                            return $values;
                        }
                    }
                }
            }
        }else{
            return FALSE;
        }
    }

    // Funcion que hace el listado en una vista

    public function _make_the_list_on_the_see($FD, $array){
        foreach ($array as $key => $value) {
            $ya = $this->_make_the_show_see($key, $array);
            fwrite($FD, "\t\t\t\t\t<tr>\n");
            $value['lbl'] = ucfirst($value['lbl']);
            fwrite($FD, "\t\t\t\t\t\t<th><strong>$value[lbl]</strong></th>\n");
            fwrite($FD, "\t\t\t\t\t\t<td>$ya</td>\n");
            fwrite($FD, "\t\t\t\t\t</tr>\n");
        }
    }

    // Funcion que imprime las cabeceras de la tabla de un listado

    public function _make_the_th_on_the_list($FD, $array){
        foreach ($array as $key => $value) {
            $ya = $this->_make_the_show($key, $array);
            $value['lbl'] = ucfirst($value['lbl']);
            fwrite($FD, "\t\t\t\t\t<th><strong>$value[lbl]</strong></th>\n");
        }
    }

    // Funcion que imprime los datos de la tabla de un listado

    public function _make_the_td_on_the_list($FD, $array){
        foreach ($array as $key => $value) {
            $ya = $this->_make_the_show($key, $array);
            fwrite($FD, "\t\t\t\t\t<td>$ya</td>\n");
        }
    }

    // Funcino que retorna el primer valor de un array

    public function _give_me_the_first_($array){
        if(is_array($array)){
            return $array[1];
        }else{
            return FALSE;
        }
    }

    // Funcion que hace los selects para los ot's

    public function _make_the_selects_($FD, $labelsandvalues, $ttable){
        $table = "";
        if(is_array($labelsandvalues)){
            foreach ($labelsandvalues as $keys => $values) {
                unset($campos);
                $campos = "array(";
                foreach ($values as $key => $value) {
                    if($key=="table"){
                        $table = $value;
                    }elseif ($key=="label") {
                        $campos .= "'$value', ";
                    }else{
                        $campos .= "'$value');";
                    }
                }
                fwrite($FD, "\n");
                fwrite($FD, "\$campos_$table = $campos\n");
                fwrite($FD, "\$value_$table = \$pro->_select_$ttable(\$campos_$table, NULL, NULL, NULL, \"$table\");\n");
                fwrite($FD, "if(!\$value_$table)echo \$pro->error;\n");
                //fwrite($FD, "\$pro->close();\n");
                fwrite($FD, "\n");
            }
            echo "<br>";
        }else{
            return FALSE;
        }
    }
    
    // Crea una carpeta a la fuerza
    // Si la carpeta ya existe la elimina incluyendo el contenido de la misma.
    
    public function _force_to_create_a_new_folder($ruta, $perm="0777"){
        if(file_exists($ruta)){
            // Aplicando eliminacion total
            // del directorio y archivos dentro
            $this->rrmdir($ruta);
            // Creando archivo
            mkdir($ruta, $perm);
            // Re-definiendo los permisos
            chmod($ruta, $perm);
            return TRUE;
        }else{
            // Creando archivo
            mkdir($ruta, $perm);
            // Re-definiendo los permisos
            chmod($ruta, $perm);
            return FALSE;
        }
    }
    
    // Funcion que se encarga de eliminar los archivos
    // dentro de una carpeta, completamente
    
    public function rrmdir($dir) {
        foreach(glob($dir . '/*') as $file) {
            if(is_dir($file)){
                rrmdir($file); // Retorna a la recursividad.
            }else{
                unlink($file); // Elimina el archivo.
            }
        }
        rmdir($dir);
    }
        
}

?>