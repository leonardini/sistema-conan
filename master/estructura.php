<?php

require_once("config.master.php");
require("db.master.php");

$t = addslashes(trim($_GET['t']));

$es = new master();
$est = $es->_show_columns($t);
$c = 1;

$tb = new master();
$tables = $tb->_show_tables_diferent($t);
$tb->Close();
// Definiendo el resultado de la Tabla
$object = "Tables_in_".DB_NAME;

while($row = $tables->fetch_object()){
    $rows[] = $row->$object;
}

?>
<script src='default/js/jquery-1.11.0.min.js'></script>
<script src='default/js/jquery-ui-1.10.3.min.js'></script>
<p>Detalles de la tabla seleccionada <strong><?= $t; ?></strong>.</p>
<p>Todos los campos que de NULO esten en estado NO, serán validados con Jquery automáticamente.</p>
<p>
    <table class="table" border="1">
        <thead>
            <tr>
                <th class="success">Campo</th>
                <th class="success">Tipo</th>
                <!--<th class="success">Nulo?</th>
                <th class="success">Llave</th>
                <th class="success">Default</th>
                <th class="success">Extra</th>-->
                <th class="danger">Formulario</th>
                <th class="danger">Tabla</th>
                <th class="danger">Label</th>
                <th class="danger">Value</th>
                <th class="danger">Otros</th>
            </tr>
        </thead>
        <tbody>
            <?php while($rowe = $est->fetch_object()){ ?>
            <tr>
                <td>
                    <?php 
                        if($rowe->Key=="PRI"){ 
                            if(strpos($rowe->Field, "_")){
                                $ff = strstr($rowe->Field, "_");
                                $ff = str_replace("_", "", $ff);
                            }else{
                                $ff = $rowe->Field;
                            }
                    ?>
                    <input type="text" size="8" name="lbl[<?= $rowe->Field; ?>]" id="lbl[<?= $rowe->Field; ?>]" value="<?= $ff; ?>" >
                    <?php
                        }else{
                            if(strpos($rowe->Field, "_")){
                                $ff = strstr($rowe->Field, "_");
                                $ff = str_replace("_", "", $ff);
                            }else{
                                $ff = $rowe->Field;
                            }
                    ?>
                    <input type="text" size="8" name="lbl[<?= $rowe->Field; ?>]" id="lbl[<?= $rowe->Field; ?>]" value="<?= $ff; ?>" >
                    <?php
                        }
                    ?>
                </td>
                <td>
                    <?= $rowe->Type; ?>
                    <input type="hidden" id="invalue[<?= $rowe->Field; ?>]" name="invalue[<?= $rowe->Field; ?>]" value="<?= $rowe->Type; ?>">
                </td>
                <!--<td>
                    <?= $rowe->Null; ?>
                    <input type="hidden" id="null[<?= $rowe->Field; ?>]" name="null[<?= $rowe->Field; ?>]" value="<?= $rowe->Null; ?>">
                </td>
                <td><?= $rowe->Key; ?></td>
                <td><?= $rowe->Default; ?></td>
                <td><?= $rowe->Extra; ?></td>-->
                <td>
                    <?php
                        $r = $es->_evaluar_columna($rowe->Field, $rowe->Type, $rowe->Null, $rowe->Key, $rowe->Default, $rowe->Extra);
                        echo $es->_select_($r, $rowe->Field);
                    ?>
                </td>
                <td>
                    <?php if($r>0){ ?>
                    <select name="tables[<?= $rowe->Field; ?>]" id="tables[<?= $rowe->Field; ?>]">
                        <option value="0">-- Elija --</option>
                        <?php
                        foreach($rows AS $id => $value){
                        ?>
                        <option value="<?= $value; ?>"><?= $value; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <?php } ?>
                    <script type="text/javascript">
                        $(function(){
                           $('#tables\\[<?= $rowe->Field; ?>\\]').change(function(){
                                var check = $('#<?= $rowe->Field; ?>').val();
                                if (check=="ot") {
                                    var val = $(this).val();
                                    var c = "<?= $rowe->Field; ?>";
                                    $("#labels_div<?= $c; ?>").load('master/label.php?t='+val+'&c='+c);
                                    $("#values_div<?= $c; ?>").load('master/value.php?t='+val+'&c='+c);
                                }else{
                                    alert('No se ha seleccionado: \"Otra tabla\" en el Formulario.');
                                }
                           });
                        })
                    </script>
                </td>
                <td>
                    <?php if($r>0){ ?>
                    <div id="labels_div<?= $c; ?>">
                        
                    </div>
                    <?php } ?>
                </td>
                <td>
                    <?php if($r>0){ ?>
                    <div id="values_div<?= $c; ?>">
                        
                    </div>
                    <?php } ?>
                </td>
                <td>
                    <?php if($r=='2'){ echo "<input type=\"checkbox\" name=\"wysiwyg[$rowe->Field]\" id=\"wysiwyg[$rowe->Field]\" checked=\"checked\" /> Usar Editor" ; } ?>
                    <?php if($r=='5'){ echo "<input type=\"checkbox\" name=\"dtp[$rowe->Field]\" id=\"dtp[$rowe->Field]\" checked=\"checked\" /> Usar Selector" ; } ?>
                    <?php if($r=='1'){ echo "<input type=\"checkbox\" name=\"fm[$rowe->Field]\" id=\"fm[$rowe->Field]\" /> Numerico" ; } ?>
                </td>
            </tr>
            <?php $c++; ?>
            <?php } ?>
        </tbody>
    </table>
</p>