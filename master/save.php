<?php

error_reporting(E_ALL);
require_once("config.master.php");
require_once("file.class.php");

$reservado = "tablas__";
$otros = array("listado", "altas", "edicion", "vista", "bajas", "continuar");
$todo_res = array("tablas__", "tables", "labels", "values", "fm", "wysiwyg", "dtp", "null", "invalue", "listado", "altas", "edicion", "vista", "bajas", "continuar");

$file = new file_master();

$valores = $file->_show_me_the_post($_POST);
$tabla = $valores[$reservado];

// Accion con la base de Datos
require_once("db.master.php");
$tb = new master();
$cols = $tb->_show_columns($tabla);

$fields = array();
$primary = array();

while($f = $cols->fetch_object()){
    if($f->Key!="PRI"){
        $fields[] = $f->Field;
    }else{
        $primary[] = $f->Field;
    }
}

$compare = array_values($fields);
$post = $_POST;
$arrays = $file->_make_the_post($fields, $valores, $primary, $post);

$arrays_no_pri = $file->_make_the_post_whitout_pri($fields, $valores, $primary);
$ruta = $file->_dame_tabla($valores, "ruta", "ruta");

$valores_reverse = array_keys($valores);
$opciones = array_diff(array_intersect($valores_reverse, $otros), $compare);
//$ruta = array_diff(array_diff($valores_reverse, $todo_res), $compare);

if($file->_is_there_a_select($arrays)){
    echo "SI. <br>";
}

$file->_execute_everything($tabla, $arrays, $arrays_no_pri, $opciones, $ruta);

function pre(){
    echo "<pre>";
    return TRUE;
}

function _pre(){
    echo "</pre>";
    return TRUE;
}

header("Location:../in.php?m=$tabla&f=lista");
exit("Fin del algoritmo.");

?>