<?php

// Archivo de Configuracion similar al archivo
// in.php pero con una variación menor

// La version del Framework
$framework_version = 'OHK 3.0';

// Definiendo la estructura de trabajo
define('ENVIRONMENT', 'desarrollo');

// De acuerdo a la estructura de trabajo determinamos si el sistema esta en modo
// desarrollo, prueba o entorno. De acuerdo a cada modo se activa o no la emision de errores.
if (defined('ENVIRONMENT'))
{
	switch (ENVIRONMENT)
	{
		case 'desarrollo':
			error_reporting(E_ALL);
			define('MASTER', TRUE);
		break;
	
		case 'testeo':
			error_reporting(0);
			define('MASTER', TRUE);
		break;

		case 'produccion':
			error_reporting(0);
			define('MASTER', FALSE);
		break;

		default:
			exit('El entorno de trabajo especificado no es el correcto.');
	}
}

// Configurando carpetas globales
// Sistema
$system_path = 'system';
// Configuracion de variables
$config_path = 'config';
// Bases de Datos
$database_path = 'database';
// Cargado/Ejecucion
$load_path = 'load';
// Modulos vitales
$modules = 'modules';
// Modulos vitales para el Master
$master = 'master';

// Incrementando el /
$system_path = rtrim($system_path, '/').'/';
$config_path = rtrim($config_path, '/').'/';
$database_path = rtrim($database_path, '/').'/';
$load_path = rtrim($load_path, '/').'/';
$modules = rtrim($modules, '/').'/';
$master = rtrim($master, '/').'/';

// El archivo en cuestion
define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

// La extension a utilizar
define('EXT', '.php');

// La Base Real
define('REALBASE', str_replace($master, "", str_replace("\\", "/", (str_replace(SELF, '', __FILE__)))));

// La ruta del Sistema
define('SYSTEM', REALBASE.$system_path);

// La ruta master
// La ruta master no debe ser modificada!
// !important
define('MASTERINI', REALBASE.$master.'master'.EXT);
// La ruta master sin el archivo
define('MASTERRUTE', REALBASE.$master);

// La ruta de la Configuracion
define('CONFIG', SYSTEM.$config_path);

// La ruta de la Configuracion
define('ENTORNO', CONFIG.'entorno'.EXT);

// La ruta de la Base de Datos
define('DATABASE', SYSTEM.$database_path);

// La ruta de la Base de Datos
// Exclusivamente en archivo MYSQLI
define('MYSQLI', DATABASE.'mysqli'.EXT);

// La ruta de la Base de Datos Exclusivamente en archivo MYSQLI
// Posible archivo definitivo para la conexion a la base de datos.
define('DBMYSQLI', DATABASE.'DBMySQLi'.EXT);

// La ruta del Cargado
define('LOAD', SYSTEM.$load_path);

// La ruta de los modulos
define('MODULES', REALBASE.$modules);

require_once(ENTORNO);
require_once(CONFIG.'database'.EXT);

?>
