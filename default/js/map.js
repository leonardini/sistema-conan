var map = null;

$(document).ready(function(){

	initComponents()
	
	/**
	 * Creacion mapa utilizando leaflet y openstreetmaps
	 * @type {[type]}
	 */
	map = L.map('map').setView([-16.485964617337928,-64.23506279227385], 6);
	L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
		attribution: '&copy; OpenStreetMap'
	}).addTo(map);

})

/**
 * Inicializa los componentes visuales de la pagina
 */
function initComponents() {
	$('#myModal').modal()
	$("#btnPanel").on("click", function(){$('#myModal').modal('show')});
	$("#loadMapa").click(getGeoInformacion);

	$('#selectDepartamentos').multiselect({
		buttonClass: 'btn btn-sm btn-default col-md-12',
		buttonContainer: '<div class="col-md-12" />',
		includeSelectAllOption: true,
		nonSelectedText: 'Departamentos',
		nSelectedText: 'Seleccionados',
		checkboxName: 'departamentos[]',
		maxHeight: 450,
		onChange: getMunicipioList,
		allSelectedText: "Todos Seleccionados",
        onSelectAll: function() {
            alert('onSelectAll triggered!');
        }
	});
	$('#selectMunicipios').multiselect({
		buttonClass: 'btn btn-sm btn-default col-md-12',
		buttonContainer: '<div class="col-md-12" />',
		includeSelectAllOption: true,
		nonSelectedText: 'Municipios',
		checkboxName: 'municipios[]',
		allSelectedText: "Todos Seleccionados",
		maxHeight: 450,
		onSelectAll: function() {
            alert('onSelectAll triggered!');
        }
	});

	$('#selectIndicadores').multiselect({
		buttonClass: 'btn btn-sm btn-default col-md-12',
		buttonContainer: '<div class="col-md-12" />',
		includeSelectAllOption: true,
		nonSelectedText: 'Indicadores',
		checkboxName: 'indicadores[]',
		allSelectedText: "Todos Seleccionados",
		maxHeight: 450
	});

	$(".cFecha").datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'yy',
        onClose: function(dateText, inst) { 
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year,1));
        }
    });

	getDepartamentoList();
	getIndicadoresList();
}

function loadSelect (idSelect, data, nivel, add) {
	var select = $(idSelect);
	select.empty();
	objeto = JSON.parse(data);
	var string = "";
	$.each(objeto, function(i, val) {
		var p = val;
		var text = p.nombre;
		if(string !== p.padre && nivel != 0)
		{
			string = p.padre;
			select.append("<optgroup label="+JSON.stringify(p.padre)+"></optgroup>")
		}
		/*if(add == 1)
			text =  text +" - "+ p.desc ;*/
		select.append("<option  latitud="+JSON.stringify(p.latitud)+" longitud="+JSON.stringify(p.longitud)+" value = "+p.id+">"+text+"</option>")
		
	});
	select.multiselect('rebuild')
	$("#divLoading").hide();

}

function getDepartamentoList (){
	$("#divLoading").show();
	$.post("modules/start/getDepartamentoList.start.php")
	//$.post("in.php?m=start&f=getDepartamentoList")
	.done(function(data){ loadSelect("#selectDepartamentos",data, 0,0) })
	.fail(failLoad)
	.always(alwaysLoad)
}

function getIndicadoresList (){
	$("#divLoading").show();
	$.post("modules/start/getIndicadoresList.start.php")
	//$.post("in.php?m=start&f=getDepartamentoList")
	.done(function(data){ loadSelect("#selectIndicadores",data, 0,0) })
	.fail(failLoad)
	.always(alwaysLoad)
}

function getMunicipioList (option, checked, select) {
	
	var formulario = $('#formMap').serializeJSON();
	ids = JSON.stringify(formulario.departamentos)
	$("#divLoading").show();
	$.post("modules/start/getMunicipioList.start.php",{ ids: ids})
	//$.post("in.php?m=start&f=getDepartamentoList")
	.done(function(data){ loadSelect("#selectMunicipios",data, 1,0) })
	.fail(failLoad)
	.always(alwaysLoad)
}
/**
 * Printing map to pdf
 */
function printMap(){
	window.print()
}
/*
data = [{iconclass: 'A', lat: 59.915, lon: 10.735}
        ,{iconclass: 'exclamation', lat: 59.9, lon: 10.7}
        ,{iconclass: 'BBL', lat: 59.9, lon: 10.75}
       ];

var iconclasses = {
  exclamation: 'font-size: 22px;',
  A: 'font-size: 22px;'
};

   var icon = L.divIcon({
     className: 'map-marker '+iconclass,
     iconSize:null,
     html:'<div class="icon" style="'+iconstyle+'">'+icontext+'</div><div class="arrow" />'
   });

  L.marker(pos).addTo(map); //reference marker
  L.marker(pos,{icon: icon}).addTo(map);

});
*/
function drawLayers (data) {

	var obj = $.parseJSON( data);
	var markers = new L.MarkerClusterGroup({
    		zoomToBoundsOnClick: true,
    		disableClusteringAtZoom: 15
		});
	$.each(obj, function(index, el) {
		console.log(el)
		latitud = el.latitud;
		longitud = el.longitud

		

		var icon = L.divIcon({
	     	className: 'map-marker',
	     	iconSize:null,
	    	html:'<i style="color:'+el.id_iconos+';" class="fa fa-map-marker fa-3x"></i>'
		});
		var marker = L.marker([longitud, latitud], {icon:icon});
		marker.bindPopup("<b><strong>"+el.nombre_indicador+"</strong></b><br>Valor del Indicador:<strong> "+el.valor+"</strong>").openPopup();
		/*marker.on('click', function(event) {
			console.log(1)
		});*/
		markers.addLayer(marker);
		
	
	})
	$('#myModal').modal('hide')
	markers.on('clusterclick', function(ev){
		console.log(123123123)
		//map.zoomIn()
	})
	map.addLayer(markers);
}

function failLoad () {
	console.log("fail")
}

function alwaysLoad () {
}

function getGeoInformacion()
{
	
	var formulario = JSON.stringify($('#formMap').serializeJSON());
	var formularioJSON = $('#formMap').serializeJSON();
	
	if(typeof formularioJSON.indicadores === "undefined")
	{
			alert("Seleccione al menos un Indicador")
			return false;
	}
	if(typeof formularioJSON.departamentos === "undefined")
	{
			alert("Seleccione al menos un Departamento")
			return false;
	}
	else
	{
		if(typeof formularioJSON.municipios === "undefined")
		{
			alert("Seleccione al menos un Municipio")
			return false;
		}
		else
		{
			var formulario =JSON.stringify( $('#formMap').serializeJSON());
			
			//$("#divLoading").show();
			$.post("modules/start/getInfoGeo.start.php",{ data: formulario})
			//$.post("in.php?m=start&f=getDepartamentoList")
			.done(function(data){drawLayers(data)})
			.fail(failLoad)
			.always(alwaysLoad)
			}
	}
		console.log(formulario)
}
	
