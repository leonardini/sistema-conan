<?php

global $title;

_make_header_($title);
_menu_top_();

function _make_header_($title){
	$tit = "";
	$tit .= "
	<!DOCTYPE html>
	<html lang=\"es\">
	<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <title>Sistema de Información Geográfica (SGIS)</title>
    <link rel=\"SHORTCUT ICON\" href=\"".ICON."32blitz.ico\"/>
    <!--<link href=\"".CSS."bootstrap.min.css\" rel=\"stylesheet\">-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"".CSS."bootstrap.css\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"".CSS."bootstrap-theme.min.css\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"".CSS."bootstrap-datatables.css\" />
    <link rel=\"stylesheet\" href=\"".CSS."jquery-te-1.4.0.css\" />
    <link rel=\"stylesheet\" href=\"".CSS."jquery-ui.css\" />
    <link href=\"".CSS."sb-admin-2.css\" rel=\"stylesheet\">
    <link href=\"".CSS."metisMenu.min.css\" rel=\"stylesheet\">
    <link href=\"".CSS."morris.css\" rel=\"stylesheet\">
    <link href=\"".CSS."font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\">
    <script src=\"".JS."jquery-1.11.0.min.js\"></script>
    <script src=\"".JS."jquery-ui-1.10.3.min.js\"></script>
    <script src=\"".JS."bootstrap.min.js\"></script>
    <script src=\"".JS."bootbox.min.js\"></script>
    <script src=\"".JS."jquery-te-1.4.0.min.js\"></script>
    <script src=\"".JS."jquery.form-validator.js\"></script>
    <script src=\"".JS."jquery.dataTables.min.js\"></script>
    <script src=\"".JS."bootstrap-datatables.js\"></script>
    <script src=\"".JS."jquery.serializejson.min.js\"></script>
    <script type=\"text/javascript\" src=\"".JS."metisMenu.min.js\"></script>
    <script type=\"text/javascript\" src=\"".JS."sb-admin-2.js\"></script>
	</head>
	<body>";
	echo $tit;
}


// Funciones que imprimen el menu
function _menu_top_(){

	$menu = "";
	$menu .= "
	<div id=\"wrapper\">

        <nav class=\"navbar navbar-default navbar-static-top\" role=\"navigation\" style=\"margin-bottom: 0\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"index.html\">
                    Consejo Nacional de Alimentación y Nitrución
                </a>
            </div>
            <ul class=\"nav navbar-top-links navbar-right\">
                
                <li class=\"dropdown\">
                    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\"> Acceso
                        <i class=\"fa fa-user fa-fw\"></i> <i class=\"fa fa-caret-down\"></i>
                    </a>
                    <ul class=\"dropdown-menu dropdown-user\">
                        <!--<li><a href=\"#\"><i class=\"fa fa-gear fa-fw\"></i> Iniciar Sesión</a>
                        </li>
                        <li class=\"divider\"></li>-->
                        <li><a href=\"in.php?m=login&f=logout\"><i class=\"fa fa-sign-out fa-fw\"></i> Cerrar Sesión</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div class=\"navbar-default sidebar\" role=\"navigation\">
                <div class=\"sidebar-nav navbar-collapse\">
                    <ul class=\"nav\" id=\"side-menu\">
                        <!-- <li class=\"sidebar-search\">
                            <div class=\"input-group custom-search-form\">
                                <input type=\"text\" class=\"form-control\" placeholder=\"Search...\">
                                <span class=\"input-group-btn\">
                                    <button class=\"btn btn-default\" type=\"button\">
                                        <i class=\"fa fa-search\"></i>
                                    </button>
                                </span>
                            </div>
                        </li> -->
                        <li class=\"active\">
                            <a href=\"in.php?m=start&f=index\"><i class=\"fa fa-home fa-fw\"></i> Inicio</a>
                        </li>
                        <!--<li>
                            <a href=\"#\"><i class=\"fa fa-map-o fa-fw\"></i> Geo-Referenciar<span class=\"fa arrow\"></span></a>
                            <ul class=\"nav nav-second-level\">
                                <li>
                                    <a href=\"in.php?m=geo_indicador&f=lista\">Referenciar Indicadores</a>
                                </li>
                                <li class=\"divider\"></li>
                                <li>
                                    <a href=\"in.php?m=geo_proyecto&f=lista\">Referenciar proyectos</a>
                                </li>
                            </ul>
                        </li>-->

                        <li>
                            <a href=\"#\"><i class=\"fa fa-bookmark fa-fw\"></i> Indicadores<span class=\"fa arrow\"></span></a>
                            <ul class=\"nav nav-second-level\">
                                <li>
                                    <a href=\"in.php?m=indicador&f=geo\">Mapa de Indicadores</a>
                                </li>
                                <li class=\"divider\"></li>
                                <li>
                                    <a href=\"in.php?m=indicador&f=lista\">Crear Indicadores</a>
                                </li>
                                <li>
                                    <a href=\"in.php?m=geo_indicador&f=lista\">Referenciar Indicadores</a>
                                </li>
                                <li>
                                    <a href=\"in.php?m=indicador&f=import\">Importar Indicadores (Excel)</a>
                                </li>
                                <li>
                                    <a href=\"in.php?m=indicador&f=imprimir\">Imprimir Indicadores</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href=\"#\"><i class=\"fa fa-book fa-fw\"></i> Proyectos<span class=\"fa arrow\"></span></a>
                            <ul class=\"nav nav-second-level\">
                                <li>
                                    <a href=\"in.php?m=proyecto&f=lista\">Crear Proyecto</a>
                                </li>
                                <li class=\"divider\"></li>
                                <li>
                                    <a href=\"in.php?m=geo_proyecto&f=lista\">Referenciar proyecto</a>
                                </li>
                                <li>
                                    <a href=\"in.php?m=proyecto&f=imprimir\">Imprimir Proyectos</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href=\"#\">
                                <i class=\"fa fa-gear fa-fw\"></i> Información Geográfica
                                <span class=\"fa arrow\"></span>
                            </a>
                            <ul class=\"nav nav-second-level\">
                                <li>
                                    <a href=\"in.php?m=dpto&f=lista\">Departamentos</a>
                                </li>
                                <li>
                                    <a href=\"in.php?m=municipio&f=lista\">Municipios</a>
                                </li>
                                <li>
                                    <a href=\"in.php?m=comunidad&f=lista\">Comunidades</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href=\"#\">
                                <i class=\"fa fa-gears fa-fw\"></i> Datos de Configuracion
                                <span class=\"fa arrow\"></span>
                            </a>
                            <ul class=\"nav nav-second-level\">
                                <li>
                                    <a href=\"in.php?m=sector_proyecto&f=lista\">Sectores para Proyectos </a>
                                </li>
                                <li>
                                    <a href=\"in.php?m=tipo_proyecto&f=lista\">Tipos de Proyectos </a>
                                </li>
                                <li>
                                    <a href=\"in.php?m=dependencia&f=lista\">Ministerios</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href=\"#\">
                                <i class=\"fa fa-user fa-fw\"></i> Usuarios
                                <span class=\"fa arrow\"></span>
                            </a>
                            <ul class=\"nav nav-second-level\">
                                <li>
                                    <a href=\"in.php?m=cargos&f=lista\">Cargos </a>
                                </li>
                                <li>
                                    <a href=\"in.php?m=personal&f=lista\">Personas</a>
                                </li>
                                <li>
                                    <a href=\"in.php?m=usuario&f=lista\">Usuarios </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id=\"page-wrapper\">
            <div class=\"container-fluid\">
                <div class=\"row\">
                    <div class=\"col-lg-12\">
	";

	echo $menu;

}
?>