<?php

require_once(DBMYSQLI); 

$DB = new DB();
$usuario = $_SESSION['usuario'];

$_consulta = "SELECT * FROM msg WHERE msgto = '$usuario' AND alert = 'A' 
AND kind = 'N' AND leido = 'N' AND tipo = 'R' AND estado = 'A'";
$_respond = $DB->execute($_consulta);

if($_respond->num_rows>0)
{

?>
<script>
	$(function(){
		$('#alerta').modal("show");
	});
</script>
<div class="modal fade" id="alerta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<form id="falert" class="modal-content" action="#" method="POST">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">Alerta de Notificación del Sistema </h4>
		</div>
		<div class="modal-body">
			<audio src="<?= SOUND; ?>/1475718280.mp3" autoplay></audio>
			<h1 class="label label-danger" style="font-size: 14px;">¡TIENE UNA NUEVA NOTIFICACION DEL SISTEMA, REVISELA CON URGENCIA!</h1>
			<br><br>
			<a href="in.php?m=msg&f=lista" class="btn btn-success"><span class="glyphicon glyphicon-envelope"></span> Ver Mensajes</a>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" class="close" data-dismiss="modal">
				<span class="glyphicon glyphicon-ban-circle"></span>
				<span class="hidden-xs"> Cerrar</span>
			</button>
		</div>
		</form>
	</div>
</div>

<?php

}

?>