<?php
class modules{

	private $autor;
	private $email_autor;
	private $profile;
	private $mobil_autor;

	function __construct($autor, $email_autor, $profile, $mobil_autor){
		$this->autor = $autor;
		$this->email_autor = $email_autor;
		$this->profile = $profile;
		$this->mobil_autor = $mobil_autor;
	}
	public function error_url(){
		exit('<p>La accion no puede ser ejecutada. <br>
		Esto se debe a la inexistencia de ciertos archivos. <br>
		Contacte al Desarrollador. <br>
		Email: '.$this->email_autor.' <br>
		Facebook:
		<a href="'.$this->profile.'" target="_blank">'.$this->autor.'</a>
		<br> Mobil: '.$this->mobil_autor.'</p>');
	}
	public function in_module(){
		if((isset($_GET['m']))and(isset($_GET['f']))){

			$m = htmlspecialchars(addslashes(trim($_GET['m'])),ENT_QUOTES);
			$f = htmlspecialchars(addslashes(trim($_GET['f'])),ENT_QUOTES);

			$fileoff = MODULES.$m."/".$f.".".$m.EXT;
			$session = SYSTEM."session/session".EXT;
			$header = SYSTEM."includes/header".EXT;
			$footer = SYSTEM."includes/footer".EXT;
			
			if(file_exists($fileoff) AND is_readable($fileoff)){
				if($m != LOGINFOLDER){
					require_once($session);
					require_once($header);
				} 
				# @@ MUY IMPORTANTE @@
				require_once($fileoff);
				# @@ MUY IMPORTANTE @@
				if($m != LOGINFOLDER){
					require_once($footer);
				} 
				return $fileoff;
			}else{
				$this->error_url();	
			} 

		}elseif(isset($_GET['m'])){
			if(MASTER){ if(file_exists(MASTERINI)) require_once(MASTERINI); }
			else{ $this->error_url(); }
		}else{
			exit("No ha seleccionado ningun modulo!");
		}
	}
}

$modules = new modules(AUTOR, EMAILAUTOR, PROFILE, MOBILE);
$modules->in_module();

// El nombre de la ruta del arhivo completo
// se puede recibir de la siguiente forma
// $ruta = $modules->in_module();
// para luego imprimirla o usarla para trabajar

?>