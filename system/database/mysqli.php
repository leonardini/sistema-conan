<?php

require_once(CONFIG.'database.php');

class DBConnector {

	protected static $conn; # Objeto conector mysqli
	protected static $stmt; # preparación de la consulta SQL
	protected static $reflection; # Objeto Reflexivo de mysqli_stmt
	protected static $sql; # Sentencia SQL a ser preparada
	protected static $data; # Array conteniendo los tipo de datos más los datos a ser enlazados
	public static $results; # Colección de datos retornados por una consulta de selección

	protected static function conectar() {
		self::$conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	}

	protected static function preparar() {
		self::$stmt = self::$conn->prepare(self::$sql);
		self::$reflection = new ReflectionClass('mysqli_stmt');
	}

	protected static function set_params() {
		$method = self::$reflection->getMethod('bind_param');
		$method->invokeArgs(self::$stmt, self::$data);
	}

	protected static function get_data($fields) {
		$method = self::$reflection->getMethod('bind_result');
		$method->invokeArgs(self::$stmt, $fields);
		while(self::$stmt->fetch()) {
			self::$results[] = unserialize(serialize($fields));
		}
	}

	protected static function finalizar() {
		self::$stmt->close();
		self::$conn->close();
	}

	public static function ejecutar($sql, $data, $fields=False) {
		self::$sql = $sql; # setear la propiedad $sql
		self::$data = $data; # setear la propiedad $data
		self::conectar(); # conectar a la base de datos
		self::preparar(); # preparar la consulta SQL
		self::set_params(); # enlazar los datos
		self::$stmt->execute(); # ejecutar la consulta
		if($fields) {
			self::get_data($fields);
		} else {
			if(strpos(self::$sql, strtoupper('INSERT')) === 0) {
				return self::$stmt->insert_id;
			}
		}
		self::finalizar(); # cerrar conexiones abiertas
	}

	public function query($query) {
		self::$sql = $query; # setear la propiedad $sql
		self::conectar(); # conectar a la base de datos
		self::preparar(); # preparar la consulta SQL
        $query = mysqli_query(self::$conn, $query) or die(mysqli_error(self::$conn));
        return $query; 
    }

    public function total_rows($query){
    	return $query->num_rows;
    }

    public function fetch_array($query){
    	while($crow = mysqli_fetch_array($query)){
    		$results[] = $crow[0];
    	}
    	return $results;
    }

    public function fetch_assoc($query) {
        $query = mysqli_fetch_assoc($query);
        return $query;
    }
}

class master_mysqli extends mysqli{

	public function __construct() {
		parent::__construct(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	}
	public function __destruct(){
		parent::__destruct();
	}
	public function sqlFetchObject($p_rs=NULL, $p_result_type=NULL){
		if(isset($this->myrs)) $frs=$this->myrs;
		if(isset($p_rs)) $frs=$p_rs;
		if($frs){
			$r_obj=$frs->fetch_object();
			if(!isset($p_rs)) $this->myrs=$frs;
			foreach($r_obj as $k=>$v) $r_rw[$k]=$v;
			$data=$this->getFetchedSafeRowArray($r_rw, $this->ensureSafeData);
			$rtrn=new stdClass();
			foreach($data as $k=>$v) $rtrn->$k=$v;
		}
		return $rtrn;
	}
}

?>