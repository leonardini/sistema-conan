<?php

// Powered by OHK
// There's no documentation. Sorry Guys.

class DB extends mysqli {

    function __construct(){
        parent::__construct(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    }
 
    public function execute($query) {
    	$result = $this->query($query);
    	if(!$result){
        	printf("Error: %s\n", $this->error);
        }else{
        	return $result;
        }
    }
    
    public function getobject($query){
		return $this->fetch_object($query);
    }
 
    public function getRow($query) {
        $result = $this->query($query);
        if (!$result) return null;
        return $result->fetch_assoc();
    }    
 
    public function getOne($query) {
        $result = $this->query($query);
        if (!$result) return null;
        $row = $result->fetch_row();
        return is_array($row) ? reset($row) : false;
    }

    public function getAll($query) {
        $result = $this->query($query);
        $ret = array();
        if (!$result) return null;
        while ($row = $result->fetch_assoc()) {
            $ret[] = $row;
        }
        return $ret;
    }
 
    public function getAssoc($query) {
        $result = $this->query($query);
        $ret = array();
 
        if (!$result) return null;
 
        while ($row = $result->fetch_assoc()) {
            $values = array_values($row);
	    $ret[$values[0]] = $values[1];
        }
 
        return $ret;
    }
 
    public function qstr($str) {
        if (is_array($str)) {
            return $this->qstrArr($str);
        }
 
        return "'{$this->real_escape_string($str)}'";
    }
 
    public function qstrArr($arr) {
        foreach ($arr as $key => $value) {
            $arr[$key] = $this->qstr($value);
        }
 
        return $arr;
    }
 
    public function getInsertSql($table_name, $fields) {
        $keys = array_keys($fields);
        $count = count($keys);
 
        for ($i = 0; $i < $count; $i++) {
            $keys[$i] = "`" . $keys[$i] . "`";
        }
 
        $keys = implode(", ", $keys);
        $values = implode(", ", $fields);
 
        return "INSERT INTO `$table_name` ($keys) VALUES ($values)";
    }
 
    public function getUpdateSql($table_name, $fields, $key_name, $key_value) {
        $keys = array_keys($fields);
 
        $update = array();
        foreach ($fields as $key => $value) {
            $update[] = "`$key`=$value";
        }
 
        $update = implode(", ", $update);
 
        return "UPDATE `$table_name` SET $update WHERE `$key_name`=$key_value";
    }
 
    public function regexpPrepare($str) {
        $length = mb_strlen($str);
        $buffer = '';
 
        for ($i = 0; $i < $length; $i++) {
            $char = mb_substr($str, $i, 1);
            if ('UTF-8' == mb_detect_encoding($char, 'auto')) {
                $buffer .= '('.mb_strtoupper($char).'|'.mb_strtolower($char).')';
            } else {
                $buffer .= $char;
            }
        }
 
        return $buffer;
    }
 
    public function getInsertId() {
        return $this->insert_id;
    }
    
    public function get_tables(){
		return $this->getAll("SHOW TABLES");
    }
 
    public function trim($table, $params) {
        $columns = $this->getAll("DESCRIBE `$table`");
        $data = array();
 
        foreach ($columns as $col) {
            if (!isset($params[$col['Field']]) || (strlen($params[$col['Field']]) == 0)) {
                if ($col['Null'] == 'YES') {
                    $data[$col['Field']] = 'NULL';
                } else if ($col['Default'] == NULL) {
                    $data[$col['Field']] = "''";
                }
            } else {
                $data[$col['Field']] = $this->qstr($params[$col['Field']]);
            }
        }
 
        return $data;
    }
    
    public function _arma_where($where){
		if(($where!=NULL)and(is_array($where))){
		    $c = "WHERE ";
		    $i = 0;
		    $contador = count($where);
		    if($contador >1){
			foreach($where AS $id => $value){
			    $c .= "".$id." = '".$value."' ";
			    $i++;
			    if($i<$contador){
				$c .= " AND ";
			    }
			}
			return $c;
		    }else{
			$keyw = array_keys($where);
			$valuew = array_values($where);
			$c = "WHERE ".$keyw[0]." = '".$valuew[0]."' ";
			return $c;
		    }
		}elseif($where!=NULL){
		    return "WHERE ".$where;
		}else{
		    return FALSE;
		}
    }
	
    public function _arma_order($order){
		if(($order!=NULL)and(is_array($order))){
		    $c = "ORDER BY ";
		    $i = 0;
		    $contador = count($order);
		    if($contador >1){
			foreach($order AS $id => $value){
			    $c .= "`".$id."` ".$value;
			    $i++;
			    if($i<$contador){
				$c .= ", ";
			    }
			}
			return $c;
		    }else{
			$keyw = array_keys($order);
			$valuew = array_values($order);
			$c = "ORDER BY ".$keyw[0]." ".$valuew[0];
			return $c;
		    }
		}elseif($order!=NULL){
		    return "ORDER BY ".$order;
		}else{
		    return FALSE;
		}
    }
    
    public function _arma_limit($limit=FALSE){
		if($limit){
		    return " LIMIT $limit";
		}else{
		    echo "Error al crear el limit, debe ser un string.";
		}
    }
    
    public function _arma_select($fields, $table){
		if($fields == '*'){
		    return "SELECT * FROM `$table` ";
		}
		if(($fields!=NULL)and(is_array($fields))){
		    $c = "SELECT ";
		    $i = 0;
		    $contador = count($fields);
		    if($contador >1){
			foreach($fields AS $id => $value){
			    $c .= "`".$value."` ";
			    $i++;
			    if($i<$contador){
				$c .= ", ";
			    }
			}
			return $c." FROM `".$table."` ";
		    }else{
			$keyw = array_keys($fields);
			$valuew = array_values($fields);
			$c = $keyw[0]." ".$valuew[0]." FROM `".$table."` ";
			return $c;
		    }
		}elseif($fields!=NULL){
		    return $fields." FROM `".$table."` ";
		}else{
		    return FALSE;
		}
	    }
	    
	public function _arma_select_olny($fields){
		if($fields == '*'){
		    return "SELECT * ";
		}
		if(($fields!=NULL)and(is_array($fields))){
		    $c = "SELECT ";
		    $i = 0;
		    $contador = count($fields);
		    if($contador >1){
			foreach($fields AS $id => $value){
			    $c .= "".$value." ";
			    $i++;
			    if($i<$contador){
				$c .= ", ";
			    }
			}
			return $c." ";
		    }else{
			$keyw = array_keys($fields);
			$valuew = array_values($fields);
			$c = $keyw[0]." ".$valuew[0]." ";
			return $c;
		    }
		}elseif($fields!=NULL){
		    return $fields." ";
		}else{
		    return FALSE;
		}
    }
    
    public function _arma_from_left_join($table, $otable, $equal){
	if(is_array($equal)){
	    $c = "FROM (".$otable." LEFT JOIN ".$table." ";
	    $v = "ON ";
	    if(count($equal)==1){
		foreach($equal AS $id => $value){
		    $v .= "`".$id."` = `".$value."`";
		}
		$return = $c.$v.")";
		return $return;
	    }else{
		echo "No se permite mas de 1 dato en el array.";
	    }
	}
    }
    
    public function _arma_multiple_join($fields, $table, $join_table, $on, $where){
		$select = $this->_arma_select_olny($fields);
		$parentesis = count($join_table);
		$from = "FROM ";
		$start = 0;
		while($start<$parentesis){
		    $from .= "(";
		    $start++;
		}
		$from .= " $table ";
		$start = 0;
		$left = "";
		
		foreach($on AS $keyon => $valoron){
		    $left .= "LEFT JOIN ".$join_table[$start]." ON ";
		    $left .= "".$keyon." = ".$valoron.")";
		    $start++;
		}
		
		$where = " ".$this->_arma_where($where);
		return $select.$from.$left.$where;
    }

    public function _arma_multiple_join_order($fields, $table, $join_table, $on, $where, $order){
		$select = $this->_arma_select_olny($fields);
		$parentesis = count($join_table);
		$from = "FROM ";
		$start = 0;
		while($start<$parentesis){
		    $from .= "(";
		    $start++;
		}
		$from .= " $table ";
		$start = 0;
		$left = "";
		
		foreach($on AS $keyon => $valoron){
		    $left .= "LEFT JOIN ".$join_table[$start]." ON ";
		    $left .= "".$keyon." = ".$valoron.")";
		    $start++;
		}

		$order = " ".$order;
		
		$where = " ".$this->_arma_where($where);
		return $select.$from.$left.$where.$order;
    }
    
    public function _arma_insert($fields=FALSE, $table){
		if(($fields)and(is_array($fields))){
		    $c = "INSERT INTO `$table`(";
		    $v = "VALUES (";
		    $i = 0;
		    $contador = count($fields);
		    foreach($fields AS $id => $value){
			$c .= "`".$id."` ";
			$v .= "'".$value."' ";
			$i++;
			if($i<$contador){
			    $c .= ", ";
			    $v .= ", ";
			}
			if($i==$contador){
			    $c .= ") ";
			    $v .= ") ";
			}
		    }
		    return $c.$v;
		}else{
		    echo "Error al crear el INSERT. <br>
		    Formato de array: array('nombre_campo'=>'valor', 'nombre_campo2'=>'valor2');";
		}
    }
    
    public function _arma_set($set){
		if(($set!=NULL)and(is_array($set))){
		    $c = "SET ";
		    $i = 0;
		    $contador = count($set);
		    if($contador >1){
			foreach($set AS $id => $value){
			    $c .= "`".$id."` = '".$value."' ";
			    $i++;
			    if($i<$contador){
				    $c .= " , ";
			    }
			}
			return $c;
		    }else{
			$keyw = array_keys($set);
			$valuew = array_values($set);
			$c = "SET ".$keyw[0]." = '".$valuew[0]."' ";
			return $c;
		    }
		}elseif($set!=NULL){
		    return "SET ".$set;
		}else{
		    return FALSE;
		}
    }

    public function _call_multiple_left_join_db($table, $fields, $join_table, $on, $where){
		$query_ = $this->_arma_multiple_join($fields, $table, $join_table, $on, $where); 
		return $this->execute($query_); 
	}
    
    public function _arma_update($table=FALSE){
		if($table){
		    return "UPDATE $table ";
		}else{
		    echo "Error al armar el UPDATE.";
		    return FALSE;
		}
    }
    
    public function _arma_delete($table=FALSE){
		if($table){
		    return "DELETE FROM $table ";
		}else{
		    echo "Error al armar el DELETE.";
		    return FALSE;
		}
    }
    
    public function _get_tables(){
		return "SHOW TABLES";
    }
    
    public function _get_columns($table=FALSE){
		if($table){
		    return "SHOW COLUMNS FROM ".$table;
		}else{
		    echo "Error al crear la consulta.";
		    return FALSE;
		}
    }
 
}
?>
