<?php 
session_start();
// Por norma
// Funciones que determinan la existencia
// o no de una sesion/cookie

// Llamando a la base de datos
require(DBMYSQLI);

// Variable de almacena la ruta de la redireccion
// en caso de que las sessiones esten activas

// Correcto
$redireccion_correcto_ = "in.php?m=start&f=index";
// Incorrecto
$redireccion_incorrecto_ = "in.php?m=login&f=login&e=error";

// Funcion que verifica la activacion de
// nuestro nombre de session/cookie

// Nombre de la session requerida: $_SESSION['usuario']
// Nombre de la session del nivel de usuario: $_SESSION['nivel']
// Nombre de la cooke $_COOKIE['identificado']
_check_session_($redireccion_incorrecto_);

//	@return true or false
//  @global redireccion correcto/incorrecto
function _check_session_($redireccion_incorrecto_){
	if(isset($_SESSION['usuario'])){
		return;
	}elseif(isset($_COOKIE['identificado'])){
		$cookie_ = $_COOKIE['identificado'];
		$wh_ = _check_cookie_($cookie_);
		if(!$wh_){
			header("Location: $redireccion_incorrecto_");
			exit;
		}
	}else{
		header("Location: $redireccion_incorrecto_");
		exit;
	}
}

// Funcion que retorna la comprobacion de un
// usuario mediante la cookie

function _check_cookie_($cookie_){
	// Instancia a la Base de datos
	$db_ = new DB();
	// Consulta SQL (MYSQL[mysqli])
	$sql_ = "SELECT id_users, user, nivel, email 
	FROM users WHERE cookie = '$cookie_' 
	AND last_time_login < now()";
	$result_ = $db_->execute($sql_);
	if(!$result_){
		echo $db_->error;
		$db_->Close();
		return FALSE;
	}else{
		$row_ = $result_->fetch_object();
		if($row_->user==NULL){
			$_SESSION['usuario'] = $row_->email;
			$_SESSION['nivel'] = $row_->nivel;
			$_SESSION['idusuario'] = $row_->id_users;
		}else{
			$_SESSION['usuario'] = $row_->user;
			$_SESSION['nivel'] = $row_->nivel;
			$_SESSION['idusuario'] = $row_->id_users;
		}
		return TRUE;
	}
}

?>