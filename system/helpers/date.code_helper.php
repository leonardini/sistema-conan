<?php
// Funciones sobre fechas
// Formatear fecha a tipo Y-m-d
function formatear_en($fecha){
    $dates = new DateTime($fecha);
    $dat = $dates->format('Y-m-d');
    return $dat;
}
// Formatear fecha a tipo d-m-Y
function formatear_es($fecha){
    $dates = new DateTime($fecha);
    $dat = $dates->format('d-m-Y');
    return $dat;
}

// Formatear fecha a tipo d-m-Y
function formatear_es_($fecha){
    $dates = new DateTime($fecha);
    $dat = $dates->format('d/m/Y');
    return $dat;
}

// Retorna el día según la fecha introducida
// EJ: 2014-05-19 -> Monday
function dia_literal_en($fecha){
    $fecha = formatear_en($fecha);
    return date("l", strtotime($fecha));
}

// Crea formato para emitir factura
function formato_factura($fecha)
{
    $fecha = formatear_en($fecha);
    // Separando Año, Mes, Dia FINAL
    $ano = substr($fecha, 0, -6);
    $mes = substr($fecha, 5, -3);
    $dia = substr($fecha, -2);
    return $ano.$mes.$dia;
}

// Funcion que retorna el mes en literal
// forma de ingreso de los datos 09 = Septiembre
function mes_literal($mes)
{
    switch ($mes) {
        case '01':
            return "Enero";
            break;

        case '02':
            return "Febrero";
            break;

        case '03':
            return "Marzo";
            break;
        
        case '04':
            return "Abril";
            break;

        case '05':
            return "Mayo";
            break;

        case '06':
            return "Junio";
            break;

        case '07':
            return "Julio";
            break;

        case '08':
            return "Agosto";
            break;

        case '09':
            return "Septiembre";
            break;

        case '10':
            return "Octubre";
            break;

        case '11':
            return "Noviembre";
            break;

        case '12':
            return "Diciembre";
            break;
    }
}

// Retorna el día según la fecha introducida
// EJ: 2014-05-19 -> Lunes
function dia_literal_es($fecha){
    $fecha = formatear_en($fecha);
    $dia = date("l", strtotime($fecha));
    switch ($dia) {
        case 'Monday':
            return "Lunes";
            break;

        case 'Tuesday':
            return "Martes";
            break;

        case 'Wednesday':
            return "Miércoles";
            break;
        
        case 'Thursday':
            return "Jueves";
            break;

        case 'Friday':
            return "Viernes";
            break;

        case 'Saturday':
            return "Sabado";
            break;

        case 'Sunday':
            return "Domingo";
            break;
    }
}

// Días entre dos fechas
if(!function_exists("cuantos_dias_hay")){
    function cuantos_dias_hay($fecha_inicial, $fecha_final){
        // Formateando fecha inicial a Y-m-d
        $data_inicial = formatear_en($fecha_inicial);
        // Formateando fecha final a Y-m-d
        $data_final = formatear_en($fecha_final);
        // Separando Año, Mes, Dia INICIAL
        $anoi = substr($data_inicial, 0, -6);
        $mesi = substr($data_inicial, 5, -3);
        $diai = substr($data_inicial, -2);
        // Separando Año, Mes, Dia FINAL
        $ano = substr($data_final, 0, -6);
        $mes = substr($data_final, 5, -3);
        $dia = substr($data_final, -2);
        // Iniciando diferencia
        $dias = floor((mktime(null, null, null, $mesi, $diai, $anoi)-mktime(null, null, null, $mes, $dia, $ano))/86400);
        //$dias = abs($dias);
        return $dias;
    }
}
// Cuantos dias hay desde HOY hasta la fecha introducida
if(!function_exists("dias_desde_hoy")){
    function dias_desde_hoy($fecha_final){
        // Formateando fecha final a Y-m-d
		$data_final = formatear_en($fecha_final);
        // Separando Año, Mes, Dia FINAL
        $ano = substr($data_final, 0, -6);
        $mes = substr($data_final, 5, -3);
        $dia = substr($data_final, -2);
        // Iniciando diferencia
        $dias = floor((mktime(null, null, null, $mes, $dia, $ano)-time())/86400);
        $dias = ($dias + 1);
        return $dias;
    }
}
// Cambiar formato de Y-m-d a Dia-mm-AAAA
if(!function_exists("formato_letra_es")){
    function formato_letra_es($fecha){
	$mes = '';
        // Formateando fecha a Y-m-d
        $datef = new DateTime($fecha);
        $data = $datef->format('Y-m-d');
        $ingresado = $data;
		
        $y = substr($ingresado, 0, -6);
        $m = substr($ingresado, 5, -3);
        $d = substr($ingresado, -2);
        
        switch ($m) {
                case '01':
                        $mes='Ene';
                        break;
                case '02':
                        $mes='Feb';
                        break;
                case '03':
                        $mes='Mar';
                        break;
                case '04':
                        $mes='Abr';
                        break;
                case '05':
                        $mes='May';
                        break;
                case '06':
                        $mes='Jun';
                        break;
                case '07':
                        $mes='Jul';
                        break;
                case '08':
                        $mes='Ago';
                        break;
                case '09':
                        $mes='Sep';
                        break;
                case '10':
                        $mes='Oct';
                        break;
                case '11':
                        $mes='Nov';
                        break;
                case '12':
                        $mes='Dic';
                        break;
        }
        $diamesano = $d."-".$mes."-".$y;
        return $diamesano;
    }
}
// Cambiar formato de Y-m-d a Dia-mm-AAAA
if(!function_exists("formato_letra_en")){
    function formato_letra_en($fecha){
	$mes = '';
        // Formateando fecha a Y-m-d
        $datef = new DateTime($fecha);
        $data = $datef->format('Y-m-d');
        $ingresado = $data;
		
        $y = substr($ingresado, 0, -6);
        $m = substr($ingresado, 5, -3);
        $d = substr($ingresado, -2);
        
        switch ($m) {
                case '01':
                        $mes='Ene';
                        break;
                case '02':
                        $mes='Feb';
                        break;
                case '03':
                        $mes='Mar';
                        break;
                case '04':
                        $mes='Abr';
                        break;
                case '05':
                        $mes='May';
                        break;
                case '06':
                        $mes='Jun';
                        break;
                case '07':
                        $mes='Jul';
                        break;
                case '08':
                        $mes='Ago';
                        break;
                case '09':
                        $mes='Sep';
                        break;
                case '10':
                        $mes='Oct';
                        break;
                case '11':
                        $mes='Nov';
                        break;
                case '12':
                        $mes='Dic';
                        break;
        }
        $diamesano = $y."-".$mes."-".$d;
        return $diamesano;
    }
}
// Diferencia entre dias
function diferenciaDias($inicio, $fin){
    $inicio = strtotime($inicio);
    $fin = strtotime($fin);
    $dif = $fin - $inicio;
    $diasFalt = (( ( $dif / 60 ) / 60 ) / 24);
    return ceil($diasFalt);
}
// Diferencia entre dias
function adiferenciaDias($ainicio, $afin){
    $ainicio = strtotime($ainicio);
    $afin = strtotime($afin);
    $adif = $afin - $ainicio;
    $adiasFalt = (( ( $adif / 60 ) / 60 ) / 24);
    return ceil($adiasFalt);
}
// Retorna la fecha introducida mas la cantidad introducida
// Ejemplo 2013-03-21 :: 2 = 2013-03-23
// Ejemplo 2013-02-28 :: 2 = 2013-03-02
function sumar_dias_afecha_en($fecha, $dias){
    $fecha = strtotime(formatear_en($fecha));
    $nfecha = date("Y-m-d", strtotime("+$dias day", $fecha));
    return $nfecha;
}
// Retorna la fecha introducida menos la cantidad introducida
// Ejemplo 2013-03-21 :: 2 = 2013-03-19
// Ejemplo 2013-03-01 :: 2 = 2013-02-27
function restar_dias_afecha_en($fecha, $dias){
    $fecha = strtotime(formatear_en($fecha));
    $nfecha = date("Y-m-d", strtotime("-$dias day", $fecha));
    return $nfecha;
}
// Retorna la fecha introducida mas la cantidad introducida
// Ejemplo 2013-03-21 :: 2 = 2013-03-23
// Ejemplo 2013-02-28 :: 2 = 2013-03-02
function sumar_dias_afecha_es($fecha, $dias){
    $fecha = strtotime(formatear_es($fecha));
    $nfecha = date("d-m-Y", strtotime("+$dias day", $fecha));
    return $nfecha;
}
// Retorna la fecha introducida menos la cantidad introducida
// Ejemplo 2013-03-21 :: 2 = 2013-03-19
// Ejemplo 2013-03-01 :: 2 = 2013-02-27
function restar_dias_afecha_es($fecha, $dias){
    $fecha = strtotime(formatear_es($fecha));
    $nfecha = date("d-m-Y", strtotime("-$dias day", $fecha));
    return $nfecha;
}
/**
 * Retorna la resta de la horafin menos la horaini
 * ejemplo 10:10:10 - 09:10:10 = 01:00:00
 * ejemplo 19:10:10 - 09:00:00 = 10:10:10
 */
function restaHoras($horaIni, $horaFin){
    return (date("H:i:s", strtotime("00:00:00") + strtotime($horaFin) - strtotime($horaIni) ));
}

if(!function_exists("getUltimoDiaMes"))
{
    function getUltimoDiaMes($elAnio,$elMes) {
        return date("d",(mktime(0,0,0,$elMes+1,1,$elAnio)-1));
    }
}

?>