<?php 
class formDate{
    var $dia;
    var $mes;
    var $ano;
    var $nombre;
    var $inicio;
    var $final;
    
    public function __construct($fecha, $nombre, $inicio = false, $final = false) {
        $date = date_parse($fecha);
        $this->dia = $date['day'];
        $this->mes = $date['month'];
        $this->ano = $date['year'];
        
        $this->nombre = $nombre;
        if(!$inicio)
            $this->inicio = date('Y')-1; // si no se definio año inicial se configura un año antes del actual
        else
            $this->inicio = $inicio;
        
        if(!$final)
            $this->final = date('Y')+1; // si no se definio año final se configura un año siguiente del actual
        else
            $this->final = $final;
    }

    public function showSelect(){
        // mostrar dia
        $date = '<select name="'.$this->nombre.'[day]" >';
        for($i = 1; $i<=31; $i++ ){
            $date.= '<option value="'.$i.'" '.(($i == $this->dia)?'selected="selected"':'').'>'.(($i<10)?('0'.$i):$i).'</option>';
        }
        $date.= '</select>/';
        
        // mostrar mes
        $meses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre',);
        $date.= '<select name="'.  $this->nombre.'[month]" >';
        foreach ($meses as $key => $value) {
            $date.= '<option value="'.($key+1).'" '.((($key+1) == $this->mes)?'selected="selected"':'').'>'.$value.'</option>';
        }
        $date.= '</select>/';
        
        // mostrar año
        $date.= '<select name="'.  $this->nombre.'[year]" >';
        for($i = $this->inicio; $i<=$this->final; $i++ ){
            $date.= '<option value="'.$i.'" '.(($i == $this->ano)?'selected="selected"':'').'>'.$i.'</option>';
        }
        $date.= '</select>';
        
        return $date;
    }
	
	public function setFecha($fecha){
		$date = date_parse($fecha);
        $this->dia = $date['day'];
        $this->mes = $date['month'];
        $this->ano = $date['year'];
	}
	
	public function setNombre($nombre){
		$this->nombre = $nombre;	
	}
	
	public function setInicio($year){
		$this->inicio = $year;	
	}
	
	public function setFinal($year){
		$this->final = $year;	
	} 
}
?>