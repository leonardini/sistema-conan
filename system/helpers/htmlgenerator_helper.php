<?php

class Generator {

    function __construct(){
      //initiate auto consutuctor
    }
	
    function create_textbox($fld_type='text',$fld_name,$fld_val,$max_len=20){

        $frm_tag ="<input type ='$fld_type' name='$fld_name' id='$fld_name' value='$fld_val' size='$max_len'>";
        return $frm_tag;
    }
	
    function create_textarea($fld_name,$fld_val,$rows=4,$cols=6){
        $frm_tag ="<TEXTAREA name='$fld_name' id='$fld_name' rows='$rows' cols='$cols'>$fld_val</TEXTAREA>";
        return $frm_tag;
    }
	
    function create_optionbox($fld_name,$input_array,$selected,$style='width=200px'){

        $frm_tag .="<select name='$fld_name' id='$fld_name' style='$style'>";
        $frm_tag .="<option value='0'>CHOOSE ANY ONE </option>";

            foreach($input_array as $key=>$value){

                 if($key == $selected){
                    //EX; $frm_tag .="<option value = '202' SELECTED>INDIA</option>";
                    $frm_tag .="<option value = '$key' SELECTED>$value</option>";
                 }else{
                     $frm_tag .="<option value = '$key'>$value</option>";
                 }
            }


       $frm_tag .="</select>";

        return $frm_tag;
    }
	
    function create_checkbox($fld_name='fld_name',$input_array,$selectedlist,$num_rows=1){

        $a_selectedlist = split(",",$selectedlist);

        $table_list.="<table width=100% border=0><tr width=100%>";

        foreach($input_array as $k=>$v){
                $chkvalue="";
                if ($a_selectedlist){
                    if (in_Array($k,$a_selectedlist)) $chkvalue="checked";
                }
            $table_list.="<td width=33% class=checkbox_td colspan=$num>
                            <input type=checkbox name=".$fld_name."[] id=".$fld_name."[] value=$k id=$fld_name$k $chkvalue class=checkbox>&nbsp;&nbsp;&nbsp;&nbsp;".ucwords(strtolower($v))."<label for=".$fld_name.$input_array[1]."></label>&nbsp;
                          </td>";

            $i++;

            if ($i%$num_rows==0) $table_list .="</tr><tr>";

        }

        $table_list .="</tr></table>";

        return $table_list;
    }
	
     function create_radiobox($fld_name,$input_array,$selectedlist,$num_rows=1){

        $a_selectedlist = split(",",$selectedlist);

        $table_list.="<table width=100% border=0><tr width=100%>";

        foreach($input_array as $k=>$v){
                $chkvalue="";
                if ($a_selectedlist){
                    if (in_Array($k,$a_selectedlist)) $chkvalue="checked";
                }

            $table_list.="<td width=33% class=checkbox_td colspan=$num>
                            <input type=radio name=".$fld_name."[] id=".$fld_name."[] value=$k id=$fld_name$k $chkvalue class=checkbox>&nbsp;&nbsp;&nbsp;&nbsp;".ucwords(strtolower($v))."<label for=".$fld_name.$input_array[1]."></label>&nbsp;
                          </td>";

            $i++;

            if ($i%$num_rows==0) $table_list .="</tr><tr>";

        }

        $table_list .="</tr></table>";

        return $table_list;
    }

    
}//EndClass
?> 