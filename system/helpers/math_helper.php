<?php
// Funciones Matemáticas simples...
// Suma
if(!function_exists("sumar")){
    function sumar($uno, $dos){
        return $uno + $dos;
    }
}
// Resta
if(!function_exists("restar")){
    function restar($uno, $dos){
        return $uno - $dos;
    }
}
// Multiplicacion
if(!function_exists("por")){
    function por($uno, $dos){
        return $uno * $dos;
    }
}
// Division
if(!function_exists("dividir")){
    function dividir($uno, $dos){
        return $uno / $dos;
    }
}
// Calculo de Porcentaje
if(!function_exists("percent")){
    function percent($inicial, $porcentaje){
        $p = ($porcentaje / 100);
        return $inicial * $p;
    }
}
?>