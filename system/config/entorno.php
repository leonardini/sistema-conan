<?php

header('Expires: ' . gmdate('D, d M Y H:i:s', time()-86400*365*10) . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
header('Pragma: no-cache');

// Definiendo la zona horaria
date_default_timezone_set('America/La_Paz');

// Configuracion del Lenguaje (es = Español)
$lang = 'es';

// Prefijo de la base de datos
define('PREFIJO', 'ohk_');

// Espacio de trabajo js, img, css, fonts, otros
define('IMAGES', 'default/images/');
define('CSS', 'default/css/');
define('JS', 'default/js/');
define('ICON', 'default/icon/');
define('FONTS', 'default/fonts/');
define('LANG', 'default/lang/');
define('SOUND', 'default/sounds/');

// Derechos de autor (Mantener)
$title = 'Sistema de información de Ventas, Inventarios y Recursos :: SIVIR';
$copyright = '&copy; Copyright 32 Bits | Ing. Jose Aparicio | SIVIR 6.21.16_10_13_21_40';
$invoice_foot = 'Sistema SIVIR :: 32 BITS &#8482;';

// Programador (Editar a conveniencia)
define('AUTOR', 'Ing. Lenin Aparicio');
define('EMAILAUTOR', 'ohkmalganis@gmail.com');
define('PROFILE', 'http://www.facebook.com/Lenin.Aparicio');
define('MOBILE', '591-76108443');

// Archivos y Directorios (Permisos)
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

// Nombre de los archivos base
define('LISTADO', 'lista');
define('ALTAS', 'nuevo');
define('BAJAS', 'eliminar');
define('CAMBIOS', 'editar');
define('VISTA', 'see');
define('GUARDAR', 'save');
define('GUARDARCAMBIOS', 'savechanges');

$sourcedir = 'sources';		# Path to the Sources directory.
$htmldir = 'sources/html';		# Path to the Html directory.

define('LOGINFOLDER', 'login');
define('VISITFOLDER', 'start');

?>
