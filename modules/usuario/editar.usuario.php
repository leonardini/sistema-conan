<?php

require(SYSTEM.'helpers/date.code_helper'.EXT);
require_once(MODULES.'usuario/db.usuario'.EXT);

$pro = new usuario();

$campos_persona = array('nombres', 'apellidos', 'id_personal');
$value_persona = $pro->_select_usuario($campos_persona, NULL, NULL, NULL, "personal");
if(!$value_persona)echo $pro->error;

// Recibiendo variable
$id_usuario = addslashes(trim($_GET['id_usuario']));

$news = new usuario();

// Estableciendo parametro recibido
$where = array("id_usuario" => "$id_usuario");
$values = $news->_select_usuario('*', $where);
if(!$values)echo $news->error;

?>
<script> 
$(function(){ 
	$('#id_personal').select(); 
	$('#last_time_login').datepicker({ 
	showOtherMonths: true,
	selectOtherMonths: true,
	dateFormat: 'dd-mm-yy'
	});
	$.validate(); 
}); 
$(function(){
	$("#re-passwd").blur(function(){

		var first = $('#passwd').val();
		var compare = $('#re-passwd').val();

		if(first == compare)
		{
			$("#re-passwd").attr('class', 'form-control valid');
			$("#status").html("<span class='form-valid help-block'>Correcto.</span>");
		}
		else
		{
			$("#re-passwd").attr("class", "form-control error");
    		$("#re-passwd").attr("style", "border-color: red;");
    		$("#status").html("<span class='form-error help-block'>Las contraseñas no concuerdan.</span>");
		}

    });
});
</script> 
<br>
<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Edicion de Usuario</strong></div> 
	<div class="panel-body">
		<?php while($row = $values->fetch_object()){ ?>
		<form class="container" name="form1" method="post" id="formid" action="?m=usuario&f=savechanges"> 
			<p> 
				<p> 
				<label for="id_persona"> 
					<strong>Seleccione a la Persona:</strong> 
					<select name="id_personal" id="id_personal" class="form-control"> 
					<?php while($row_persona = $value_persona -> fetch_object()){ ?>
						<option value="<?= $row_persona->id_personal; ?>" <?php if($row->id_personal==$row_persona->id_personal){ echo "selected"; } ?>><?= $row_persona->nombres; ?> <?= $row_persona->apellidos; ?></option> 
					<?php } ?>
					</select> 
				</label> 
				</p> 
				<p> 
				<label for="user"> 
					<strong>Nombre de usuario:</strong> 
					<input type="text" name="user" id="user" disabled data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->user, ENT_QUOTES); ?>"/> 
					<input type="hidden" name="user" id="user" value="<?= htmlspecialchars_decode($row->user, ENT_QUOTES); ?>">
				</label> 
				</p> 
				<p> 
				<label for="email"> 
					<strong>Correo electronico:</strong> 
					<input type="text" name="email" id="email" data-validation="email" data-validation-optional="true" class="form-control" value="<?= htmlspecialchars_decode($row->email, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="passwd"> 
					<strong>Contraseña:</strong> 
					<input type="password" placeholder="8 caracteres mínimo" name="passwd" id="passwd" data-validation="length" data-validation-length="min8" class="form-control"/>
				</label> 
				</p> 
				<p> 
				<label for="re-passwd"> 
					<strong>Repita la Contraseña:</strong> 
					<input type="password" name="re-passwd" id="re-passwd" data-validation="confirmation" class="form-control"/>
					<div id="status"></div>
				</label> 
				</p> 
				<p> 
				<label for="nivel"> 
					<strong>Nivel:</strong> 
					<select name="nivel" id="nivel" class="form-control">
					    <option value="1" <?php if($row->nivel == 1) echo "selected"; ?>>Tecnico</option>
					    <option value="2" <?php if($row->nivel == 2) echo "selected"; ?>>Supervisor</option>
					    <option value="3" <?php if($row->nivel == 3) echo "selected"; ?>>Administrador</option>
					</select>
				</label> 
				</p>
			</p> 
			<p>
				<input type="hidden" name="id_usuario" id="id_usuario" value="<?= $id_usuario; ?>"/>
				<button type="submit" name="Enviar" class="btn btn-success" /><span class="glyphicon glyphicon-floppy-disk"></span> <span class="hidden-xs">Guardar Cambios</span></button> <a href="?m=usuario&f=lista" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span> <span class="hidden-xs">Cancelar</span></a>
			</p>
		</form> 
		<?php } ?>
	</p> 
	</div> 
