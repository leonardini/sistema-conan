<?php

require_once(MODULES.'usuario/db.usuario'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);

$new = new usuario();

$id_usuario = addslashes(trim($_GET['id_usuario']));
$ff = array("usuario.id_usuario", "usuario.user", "usuario.email", 
	"usuario.last_time_login", "personal.nombres", "personal.apellidos");
$tt = "usuario";
$jt = array("personal");
$on = array(
"personal.id_personal" => "usuario.id_personal" 
 );

$where_u = array("usuario.id_usuario" => "$id_usuario");

$values = $new->_call_multiple_left_join($ff, $jt, $on, $where_u);
if(!$values)echo $new->error;
$new->close();

?>
<script type="text/javascript" charset="utf-8"> 
function eliminar(id){
	bootbox.confirm("Está seguro que desea eliminar?",function(result){
		if(result){
			window.location="?m=usuario&f=eliminar&id_usuario="+id;
		}
	});
}
</script> 
<div class="panel panel-primary"> 
	<div class="panel-heading"><strong> Vista </strong></div> 
	<div class="panel-body"> 
	<p>Opciones disponibles: </p> 
	<p> 
		<a href="?m=usuario&f=lista" class='btn btn-success'><span class="glyphicon glyphicon-list"></span><span class="hidden-xs"> Listado</span></a> 
		<a href="?m=usuario&f=nuevo" class='btn btn-primary'><span class="glyphicon glyphicon-plus"></span><span class="hidden-xs"> Nuevo(a)</span></a> 
		<a href="?m=usuario&f=editar&id_usuario=<?= $id_usuario; ?>" class='btn btn-warning'><span class="glyphicon glyphicon-pencil"></span><span class="hidden-xs"> Editar</span></a> 
		<a href="javascript:eliminar(<?= $id_usuario; ?>)" class='btn btn-danger'><span class="glyphicon glyphicon-remove"></span><span class="hidden-xs"> Eliminar</span></a> 
	</p> 
		<div class="panel panel-default"> 
			<div class="panel-heading"><strong>Vista</strong></div> 
			<div class="panel-body"> 
			<p>Vista única</p> 
			
			<div class="table-responsive"> 
			<?php while($row = $values->fetch_object()){ ?> 
			<table class="table table-condensed table-bordered"> 
				<tbody> 
					<tr>
						<th><strong>#</strong></th>
						<td><?= $row->id_usuario; ?></td>
					</tr>
					<tr>
						<th><strong>Persona</strong></th>
						<td><?= $row->nombres; ?> <?= $row->apellidos; ?></td>
					</tr>
					<tr>
						<th><strong>User</strong></th>
						<td><?= htmlspecialchars_decode($row->user, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Email</strong></th>
						<td><?= htmlspecialchars_decode($row->email, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Nivel</strong></th>
						<td><?php if($row->nivel==0){ echo "Vendedor"; }elseif($row->nivel==1){ echo "Caja"; }elseif($row->nivel==2){ echo "Jefe de Ventas"; }elseif($row->nivel==3){ echo "Gerente"; }elseif($row->nivel==4){ echo "Impuestos Nacionales"; }else{ echo "Vendedor Directo"; } ?></td>
					</tr>
				</tbody> 
			</table> 
			<?php } ?> 
			</div> 
		</div>
	</div>
</div>
