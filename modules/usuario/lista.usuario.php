<?php

require_once(MODULES.'usuario/db.usuario'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);

$new = new usuario();

$ff = array("usuario.id_usuario", "usuario.user", "usuario.email", 
"usuario.nivel", "personal.nombres", "personal.apellidos");
$tt = "usuario";
$jt = array("personal");
$on = array(
"personal.id_personal" => "usuario.id_personal" 
 );

$where_u = "";
$values = $new->_call_multiple_left_join($ff, $jt, $on, $where_u);

?>
<script> 
$(function(){ 
	$('#sort').dataTable(); 
	$('[title]').tooltip(); 
}); 
function eliminar(id){
	bootbox.confirm("Está seguro que desea eliminar?",function(result){
		if(result){
			window.location="?m=usuario&f=eliminar&id_usuario="+id;
		}
	});
}
</script> 
<br>
<h4 class="page-header">Usuarios</h4>
<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Listado de Usuarios </strong></div> 
	<div class="panel-body"> 
	<p>
		<a href="?m=usuario&f=nuevo" class='btn btn-success'>
			<span class="glyphicon glyphicon-share"></span>
			<span class="hidden-xs"> Crear nuevo usuario</span>
		</a>
	</p> 
	
		<div class="table-responsive">
		<table class="table table-bordered table-condensed" id="sort"> 
			<thead> 
				<tr class="text-center"> 
					<th><strong>#</strong></th>
					<th><strong>Persona</strong></th>
					<th><strong>User</strong></th>
					<th><strong>Email</strong></th>
					<th><strong>Nivel</strong></th>
					<th class="text-center">Opciones</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php
				while($row = $values->fetch_object()){
				?>
				<tr> 
					<td><?= $row->id_usuario; ?></td>
					<td><?= $row->nombres; ?> <?= $row->apellidos; ?></td>
					<td><?= htmlspecialchars_decode($row->user, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->email, ENT_QUOTES); ?></td>
					<td>
						<?php if($row->id_usuario!=1){ ?>
						<?php if($row->nivel==0){ echo "Vendedor"; }elseif($row->nivel==1){ echo "Caja"; }elseif($row->nivel==2){ echo "Jefe de Ventas"; }elseif($row->nivel==3){ echo "Gerente"; }elseif($row->nivel==4){ echo "Impuestos Nacionales"; }else{ echo "Vendedor Directo"; } ?>
						<?php }else{ echo "Supervisor de Sistemas"; } ?>
					</td>
					<td class="text-center">
						<a href="?m=usuario&f=editar&id_usuario=<?= $row->id_usuario; ?>" title="Editar"><span class="glyphicon glyphicon-pencil"></span></a>
						<a href="?m=usuario&f=nivel&id_usuario=<?= $row->id_usuario; ?>" title="Cambiar Nivel"><span class="glyphicon glyphicon-pawn"></span></a>
						<a href="#" onclick="eliminar(<?= $row->id_usuario; ?>)" title="Eliminar"><span class="glyphicon glyphicon-remove"></span></a>
					</td>
				</tr> 
				<?php } ?> 
			</tbody> 
		</table> 
		</div> 
	</div>
