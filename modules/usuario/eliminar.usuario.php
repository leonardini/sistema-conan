<?php

// Incluyendo archivo de la base de Datos
require_once(MODULES.'usuario/db.usuario'.EXT);
require_once(MODULES.'usuarioseliminados/db.usuarioseliminados'.EXT);
// Inicializando la clase de la base de Datos
$new = new usuario();
$us = new usuarioseliminados();
// Recibiendo y limpiando Datos
$id_usuario = addslashes(trim($_GET['id_usuario']));

$select = array("id_usuario", "user");
$where = array("id_usuario" => $id_usuario);
$result = $new->_select_usuario($select, $where);
$row = $result->fetch_object();

$idusuario = $id_usuario;
$usuario = $row->user;

if($idusuario != 1)
{
	$insert = array("id_usuario" => $idusuario, "usuario" => $usuario);
	$last = $us->_make_insert_usuarioseliminados($insert);

	$delete = array("id_usuario" => "$id_usuario");
	$new->_make_delete_usuario($delete);

	require(MODULES.'interprete/db.interprete'.EXT);
	$inter = new interprete();
	$uss = $_SESSION['usuario'];
	$detalle = "El usuario: <ul>$uss</ul> elimino al usuario <b>$usuario</b>.";
	$inter->interpreta($detalle);
}

header("Location: ?m=usuario&f=lista");
exit;

?>
