<?php

require(SYSTEM.'helpers/date.code_helper'.EXT);
require_once(MODULES.'usuario/db.usuario'.EXT);

$news = new usuario();
$db = new usuario();

$id_usuario = addslashes(trim($_GET['id_usuario']));

$consulta = "SELECT nombres, apellidos FROM personal, usuario 
WHERE personal.id_personal = usuario.id_personal AND usuario.id_usuario = '$id_usuario'";
$res = $db->ejecutar($consulta);
$rowp = $res->fetch_object();

// Estableciendo parametro recibido
$where = array("id_usuario" => "$id_usuario");
$values = $news->_select_usuario('*', $where);
if(!$values)echo $news->error;

?>
<script> 
$(function(){ 

	$('#id_personal').select(); 
	$('#last_time_login').datepicker({ 
	showOtherMonths: true,
	selectOtherMonths: true,
	dateFormat: 'dd-mm-yy'
	});
	$.validate(); 
}); 

</script> 
<br>
<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Cambiar Nivel </strong></div> 
	<div class="panel-body">
		<?php while($row = $values->fetch_object()){ ?>
		<form class="container" name="form1" method="post" id="formid" action="?m=usuario&f=savenivel">
			<p>
			<label for="id_persona">
				<strong>Persona:</strong> 
				<?= $rowp->nombres." ".$rowp->apellidos; ?>
				</select> 
			</label>
			</p>
			<p>
			<label for="nivel"> 
				<strong>Nivel:</strong> 
				<select name="nivel" id="nivel" class="form-control">
				    <option value="1" <?php if($row->nivel == 1) echo "selected"; ?>>Tecnico</option>
				    <option value="2" <?php if($row->nivel == 2) echo "selected"; ?>>Supervisor</option>
				    <option value="3" <?php if($row->nivel == 3) echo "selected"; ?>>Administrador</option>
				</select>
			</label>
			</p>
			<input type="hidden" name="id_usuario" id="id_usuario" value="<?= $id_usuario; ?>"/>
			<button type="submit" name="Enviar" class="btn btn-success" />
				<span class="glyphicon glyphicon-floppy-disk"></span> 
				<span class="hidden-xs">Guardar Cambios</span>
			</button> 
			<a href="?m=usuario&f=lista" class="btn btn-danger">
				<span class="glyphicon glyphicon-ban-circle"></span>
				<span class="hidden-xs">Cancelar</span>
			</a>
		</form> 
		<?php } ?>
</div> 
