<?php

require(SYSTEM.'helpers/date.code_helper'.EXT);
require_once(MODULES.'usuario/db.usuario'.EXT);

$pro = new usuario();

$campos_persona = array('nombres', 'apellidos', 'id_personal');
$value_persona = $pro->_select_usuario($campos_persona, NULL, NULL, NULL, "personal");
if(!$value_persona)echo $pro->error;

?>
<script> 
$(function(){ 
	$('#id_personal').select();
	$.validate(); 
}); 
$(function(){
	$("#re-passwd").blur(function(){

		var first = $('#passwd').val();
		var compare = $('#re-passwd').val();

		if(first == compare)
		{
			$("#re-passwd").attr('class', 'form-control valid');
			$("#status").html("<span class='form-valid help-block'>Correcto.</span>");
		}
		else
		{
			$("#re-passwd").attr("class", "form-control error");
    		$("#re-passwd").attr("style", "border-color: red;");
    		$("#status").html("<span class='form-error help-block'>Las contraseñas no concuerdan.</span>");
		}

    });
});
</script> 
<br>
<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Creando nuevo usuario </strong></div> 
	<div class="panel-body">
		<form class="container" name="form1" method="post" id="formid" action="?m=usuario&f=save">
			<p> 
				<p> 
				<label for="id_persona"> 
					<strong>Seleccione a la Persona:</strong> 
					<select name="id_personal" id="id_personal" class="form-control"> 
					<?php while($row_persona = $value_persona -> fetch_object()){ ?>
						<option value="<?= $row_persona->id_personal; ?>"><?= $row_persona->nombres; ?> <?= $row_persona->apellidos; ?></option> 
					<?php } ?>
					</select> 
				</label> 
				</p> 
				<p> 
				<label for="user"> 
					<strong>Usuario:</strong> 
					<input type="text" name="user" id="user" data-validation="required" class="form-control"/>
				</label> 
				</p> 
				<p> 
				<label for="email"> 
					<strong>Correo electronico:</strong> 
					<input type="text" name="email" id="email" data-validation="email" data-validation-optional="true" class="form-control"/>
				</label> 
				</p> 
				<p> 
				<label for="passwd"> 
					<strong>Contraseña:</strong> 
					<input type="password" name="passwd" placeholder="8 caracteres mínimo" id="passwd" data-validation="length" data-validation-length="min8" class="form-control"/>
				</label> 
				</p> 
				<p> 
				<label for="re-passwd"> 
					<strong>Repita la Contraseña:</strong> 
					<input type="password" name="re-passwd" id="re-passwd" data-validation="required" class="form-control"/>
					<div id="status"></div>
				</label> 
				</p> 
				<p> 
				<label for="nivel"> 
					<strong>Seleccione el Nivel de Usuario:</strong> 
					<select name="nivel" id="nivel" class="form-control">
					    <!--<option value="0">Visitante</option>-->
					    <option value="1">Tecnico</option>
					    <option value="2">Supervisor</option>
					    <option value="3">Administrador</option>
					</select>
				</label> 
				</p>
			</p> 
			<p>
				<button type="submit" name="Enviar" class="btn btn-success" /><span class="glyphicon glyphicon-floppy-disk"></span> <span class="hidden-xs">Guardar nuevo usuario</span></button> <a href="?m=usuario&f=lista" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span><span class="hidden-xs"> Cancelar</span></a>
			</p>
		</form>
</div> 
