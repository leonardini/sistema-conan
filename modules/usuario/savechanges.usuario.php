<?php
// Incluyendo archivo de la base de Datos
require_once(MODULES.'usuario/db.usuario'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
// Inicializando la clase de la base de Datos
$new = new usuario();
// Recibiendo y limpiando Datos
$id_usuario = $_POST['id_usuario'];
$id_personal = $_POST['id_personal'];
$user = htmlspecialchars($_POST['user'], ENT_QUOTES);
$email = htmlspecialchars($_POST['email'], ENT_QUOTES);
$passwd = $_POST['passwd'];
$nivel = $_POST['nivel'];

$secure_variable_ = TRUE;

$us_ = "|ohk|u-s|{[\+-/.*|@]}";
$co_ = "|ohk|c-s|{[.+\-*/|@]}";

if($secure_variable_)
{
	$passwd = sha1($us_.md5($passwd));
}
else
{
	$passwd = sha1(md5($passwd));
}

$update = array("id_personal" => "$id_personal", "user" => "$user", "email" => "$email", "passwd" => "$passwd", "nivel" => "$nivel");
$where = array("id_usuario" => "$id_usuario");
$new->_make_update_usuario($update, $where);

require(MODULES.'interprete/db.interprete'.EXT);
$inter = new interprete();
$usuario = $_SESSION['usuario'];
$detalle = "El usuario: <ul>$usuario</ul> modifico la información del usuario <b>$user</b>.";
$inter->interpreta($detalle);

header("Location: ?m=usuario&f=lista");
exit;

?>
