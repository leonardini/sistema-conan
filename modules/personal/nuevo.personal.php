<?php
require(SYSTEM.'helpers/date.code_helper'.EXT);
require_once(MODULES.'personal/db.personal'.EXT);

$pro = new personal();

$campos_cargos = array('cargo', 'id_cargo');
$value_cargos = $pro->_select_personal($campos_cargos, NULL, NULL, NULL, "cargos");
if(!$value_cargos)echo $pro->error;

?>
	<script> 
	$(function(){ 
		$('#nombres').select(); 
		$('#ingreso').datepicker({ 
		showOtherMonths: true,
		selectOtherMonths: true,
		dateFormat: 'dd-mm-yy'
		});
		$.validate(); 
	}); 
	</script> 
	<br>
	<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Ingresando nueva informacion </strong></div> 
	<div class="panel-body"> 
		<form class="container" name="form1" method="post" id="formid" action="?m=personal&f=save"> 
			<p> 
				<p> 
				<label for="nombres"> 
					<strong>Nombres:</strong> 
					<input type="text" name="nombres" id="nombres" data-validation="required" class="form-control" size="40" />
				</label> 
				</p> 
				<p> 
				<label for="apellidos"> 
					<strong>Apellidos:</strong> 
					<input type="text" name="apellidos" id="apellidos" data-validation="required" class="form-control" size="40" />
				</label> 
				</p> 
				<p> 
				<label for="documento"> 
					<strong>Carnet de identidad:</strong> 
					<input type="text" name="documento" id="documento" data-validation="required" class="form-control" size="40" />
				</label> 
				</p> 
				<p> 
				<label for="id_cargo"> 
					<strong>Cargo:</strong> 
					<select name="id_cargo" id="id_cargo" class="form-control"> 
					<?php while($row_cargos = $value_cargos -> fetch_object()){ ?>
						<option value="<?= $row_cargos->id_cargo; ?>"><?= $row_cargos->cargo; ?></option> 
					<?php } ?>
					</select> 
				</label> 
				</p> 
				<p> 
				<label for="direccion"> 
					<strong>Direccion:</strong> 
					<textarea name="direccion" id="direccion" class="form-control" data-validation="required" rows="5" cols="55"></textarea> 
				</label> 
				</p> 
				<p> 
				<label for="telefonos"> 
					<strong>Telefonos:</strong> 
					<input type="text" name="telefonos" id="telefonos" data-validation="required" class="form-control" size="40" />
				</label> 
				</p> 
				<p> 
				<label for="referencias"> 
					<strong>Referencias:</strong> 
					<textarea name="referencias" id="referencias" class="form-control" data-validation="required" rows="5" cols="55"></textarea> 
				</label> 
				</p> 
				<p> 
				<label for="ingreso"> 
					<strong>Ingreso:</strong> 
					<input type="text" name="ingreso" id="ingreso" data-validation="date" data-validation-format="dd-mm-yyyy" class="form-control" value="<?= date("d-m-Y"); ?>" /> 
				</label> 
				</p> 
			</p> 
			<p>
				<button type="submit" name="Enviar" class="btn btn-success" /><span class="glyphicon glyphicon-floppy-disk"></span> <span class="hidden-xs">Guardar Informacion</span></button> <a href="?m=personal&f=lista" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span><span class="hidden-xs"> Cancelar</span></a>
			</p>
		</form> 
	</div> 
