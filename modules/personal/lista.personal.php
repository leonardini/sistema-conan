<?php

require_once(MODULES.'personal/db.personal'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new personal();

$ff = array("personal.id_personal", "personal.nombres", "personal.apellidos", "personal.documento", "personal.direccion", "personal.telefonos", "personal.referencias", "personal.ingreso", "cargos.cargo");
$tt = "personal";
$jt = array("cargos");
$on = array(
"cargos.id_cargo" => "personal.id_cargo" 
 );

$where_u = "";

$values = $new->_call_multiple_left_join($ff, $jt, $on, $where_u);

?>
<script> 
$(function(){ 
	$('#sort').dataTable(); 
	$('[title]').tooltip(); 
}); 
function eliminar(id){
	bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
		if(result){
			window.location="?m=personal&f=eliminar&id_personal="+id;
		}
	});
}
</script> 
<br> 
<h4 class="page-header">Personas</h4>
<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Listado General </strong></div> 
	<div class="panel-body"> 
	<p><a href="?m=personal&f=nuevo" class='btn btn-success'><span class="glyphicon glyphicon-share"></span><span class="hidden-xs"> Crear nuevo personal</span></a></p> 
		<div class="table-responsive">
		<table class="table table-bordered table-condensed table-hover" id="sort"> 
			<thead> 
				<tr class="text-center"> 
					<th><strong>#</strong></th>
					<th><strong>Nombres</strong></th>
					<th><strong>Apellidos</strong></th>
					<th><strong>Carnet de identidad</strong></th>
					<th><strong>Cargo</strong></th>
					<th><strong>Direccion</strong></th>
					<th><strong>Telefonos</strong></th>
					<th><strong>Referencias</strong></th>
					<th><strong>Ingreso</strong></th>
					<th class="text-center">Opciones</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php
				$count=0;
				while($row = $values->fetch_object()){ 
					$count++;
				?>
				<tr> 
					<td><?= $count; ?></td>
					<td><?= htmlspecialchars_decode($row->nombres, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->apellidos, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->documento, ENT_QUOTES); ?></td>
					<td><?= $row->cargo; ?></td>
					<td><?= htmlspecialchars_decode($row->direccion, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->telefonos, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->referencias, ENT_QUOTES); ?></td>
					<td><?= formato_letra_es($row->ingreso); ?></td>
					<td class="text-center">
						<a href="?m=personal&f=editar&id_personal=<?= $row->id_personal; ?>" title="Modificar" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span> Modificar</a>
						<a href="javascript:eliminar(<?= $row->id_personal; ?>)" title="Eliminar" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Eliminar</a>
					</td>
				</tr> 
				<?php } ?> 
			</tbody> 
		</table> 
		</div> 
	</div> 
