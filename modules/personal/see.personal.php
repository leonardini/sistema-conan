<?php

/*
*   Vista regular de los campos de una base de datos
*   Powered by OHK
*/

require_once(MODULES.'personal/db.personal'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new personal();

$id_personal = addslashes(trim($_GET['id_personal']));
$ff = array("personal.id_personal", "personal.nombres", "personal.apellidos", "personal.documento", "personal.direccion", "personal.telefonos", "personal.referencias", "personal.ingreso", "cargos.cargo");
$tt = "personal";
$jt = array("cargos");
$on = array(
"cargos.id_cargo" => "personal.id_cargo" 
 );

$where_u = array("personal.id_personal" => "$id_personal");

$values = $new->_call_multiple_left_join($ff, $jt, $on, $where_u);
if(!$values)echo $new->error;
$new->close();
?>

	<script> 
	function eliminar(id){
		bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
			if(result){
				window.location="?m=personal&f=eliminar&id_personal="+id;
			}
		});
	}
	</script> 
	<div class="panel panel-primary"> 
	<div class="panel-heading"><strong> Vista de personal </strong></div> 
	<div class="panel-body"> 
	<p>Opciones en personal: </p> 
	<p> 
		<a href="?m=personal&f=lista" class='btn btn-success'><span class="glyphicon glyphicon-list"></span><span class="hidden-xs"> Listado</span></a> 
		<a href="?m=personal&f=nuevo" class='btn btn-primary'><span class="glyphicon glyphicon-plus"></span><span class="hidden-xs"> Nuevo(a)</span></a> 
		<a href="?m=personal&f=editar&id_personal=<?= $id_personal; ?>" class='btn btn-warning'><span class="glyphicon glyphicon-pencil"></span><span class="hidden-xs"> Modificar</span></a> 
		<a href="javascript:eliminar(<?= $id_personal; ?>)" class='btn btn-danger'><span class="glyphicon glyphicon-remove"></span><span class="hidden-xs"> Eliminar</span></a> 
	</p> 
		<div class="panel panel-default"> 
			<div class="panel-heading"><strong>personal</strong></div> 
			<div class="panel-body"> 
			<p>Vista única de personal</p> 
			</div> 
			<div class="table-responsive"> 
			<?php while($row = $values->fetch_object()){ ?> 
			<table class="table table-condensed table-bordered table-hover"> 
				<tbody> 
					<tr>
						<th><strong>#</strong></th>
						<td><?= $row->id_personal; ?></td>
					</tr>
					<tr>
						<th><strong>Nombres</strong></th>
						<td><?= htmlspecialchars_decode($row->nombres, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Apellidos</strong></th>
						<td><?= htmlspecialchars_decode($row->apellidos, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Carnet de identidad</strong></th>
						<td><?= htmlspecialchars_decode($row->documento, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Cargo</strong></th>
						<td><?= $row->cargo; ?></td>
					</tr>
					<tr>
						<th><strong>Direccion</strong></th>
						<td><?= htmlspecialchars_decode($row->direccion, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Telefonos</strong></th>
						<td><?= htmlspecialchars_decode($row->telefonos, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Referencias</strong></th>
						<td><?= htmlspecialchars_decode($row->referencias, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Ingreso</strong></th>
						<td><?= formato_letra_es($row->ingreso); ?></td>
					</tr>
				</tbody> 
			</table> 
			<?php } ?> 
		</div>
	</div>
</div>
