<?php
// Incluyendo archivo de la base de Datos
require_once(MODULES.'personal/db.personal'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
// Inicializando la clase de la base de Datos
$new = new personal();
// Recibiendo y limpiando Datos
$id_personal = $_POST['id_personal'];
$nombres = htmlspecialchars($_POST['nombres'], ENT_QUOTES);
$apellidos = htmlspecialchars($_POST['apellidos'], ENT_QUOTES);
$documento = htmlspecialchars($_POST['documento'], ENT_QUOTES);
$id_cargo = $_POST['id_cargo'];
$direccion = htmlspecialchars($_POST['direccion'], ENT_QUOTES);
$telefonos = htmlspecialchars($_POST['telefonos'], ENT_QUOTES);
$referencias = htmlspecialchars($_POST['referencias'], ENT_QUOTES);
$ingreso = formatear_en($_POST['ingreso']);
$update = array("nombres" => "$nombres", "apellidos" => "$apellidos", "documento" => "$documento", "id_cargo" => "$id_cargo", "direccion" => "$direccion", "telefonos" => "$telefonos", "referencias" => "$referencias", "ingreso" => "$ingreso");
$where = array("id_personal" => "$id_personal");
$new->_make_update_personal($update, $where);
header("Location: in.php?m=personal&f=lista");
exit;

?>
