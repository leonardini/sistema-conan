<?php

/*
*   Vista regular de los campos de una base de datos
*   Powered by OHK
*/

require_once(MODULES.'comunidad/db.comunidad'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new comunidad();

$id_comunidad = addslashes(trim($_GET['id_comunidad']));
$ff = array("comunidad.id_comunidad", "comunidad.nombre_comunidad", "comunidad.latitud", "comunidad.longitud", "municipio.municipio");
$tt = "comunidad";
$jt = array("municipio");
$on = array(
"municipio.id_municipio" => "comunidad.id_municipio" 
 );

$where_u = array("comunidad.id_comunidad" => "$id_comunidad");

$values = $new->_call_multiple_left_join($ff, $jt, $on, $where_u);
if(!$values)echo $new->error;
$new->close();
?>

	<script> 
	function eliminar(id){
		bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
			if(result){
				window.location="?m=comunidad&f=eliminar&id_comunidad="+id;
			}
		});
	}
	</script> 
	<div class="panel panel-primary"> 
	<div class="panel-heading"><strong> Vista de comunidad </strong></div> 
	<div class="panel-body"> 
	<p>Opciones en comunidad: </p> 
	<p> 
		<a href="?m=comunidad&f=lista" class='btn btn-success'><span class="glyphicon glyphicon-list"></span><span class="hidden-xs"> Listado</span></a> 
		<a href="?m=comunidad&f=nuevo" class='btn btn-primary'><span class="glyphicon glyphicon-plus"></span><span class="hidden-xs"> Nuevo(a)</span></a> 
		<a href="?m=comunidad&f=editar&id_comunidad=<?= $id_comunidad; ?>" class='btn btn-warning'><span class="glyphicon glyphicon-pencil"></span><span class="hidden-xs"> Modificar</span></a> 
		<a href="javascript:eliminar(<?= $id_comunidad; ?>)" class='btn btn-danger'><span class="glyphicon glyphicon-remove"></span><span class="hidden-xs"> Eliminar</span></a> 
	</p> 
		<div class="panel panel-default"> 
			<div class="panel-heading"><strong>comunidad</strong></div> 
			<div class="panel-body"> 
			<p>Vista única de comunidad</p> 
			</div> 
			<div class="table-responsive"> 
			<?php while($row = $values->fetch_object()){ ?> 
			<table class="table table-condensed table-bordered table-hover"> 
				<tbody> 
					<tr>
						<th><strong>#</strong></th>
						<td><?= $row->id_comunidad; ?></td>
					</tr>
					<tr>
						<th><strong>Municipio</strong></th>
						<td><?= $row->municipio; ?></td>
					</tr>
					<tr>
						<th><strong>Comunidad</strong></th>
						<td><?= htmlspecialchars_decode($row->nombre_comunidad, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Latitud</strong></th>
						<td><?= htmlspecialchars_decode($row->latitud, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Longitud</strong></th>
						<td><?= htmlspecialchars_decode($row->longitud, ENT_QUOTES); ?></td>
					</tr>
				</tbody> 
			</table> 
			<?php } ?> 
		</div>
	</div>
</div>
