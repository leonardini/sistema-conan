<?php
require(SYSTEM.'helpers/date.code_helper'.EXT);
require_once(MODULES.'comunidad/db.comunidad'.EXT);

$pro = new comunidad();

$campos_municipio = array('municipio', 'id_municipio');
$value_municipio = $pro->_select_comunidad($campos_municipio, NULL, NULL, NULL, "municipio");
if(!$value_municipio)echo $pro->error;

// Recibiendo variable
$id_comunidad = addslashes(trim($_GET['id_comunidad']));

$news = new comunidad();

// Estableciendo parametro recibido
$where = array("id_comunidad" => "$id_comunidad");
$values = $news->_select_comunidad('*', $where);
if(!$values)echo $news->error;
$news->close();
?>
	<script> 
	$(function(){ 
		$('#id_municipio').select(); 
		$.validate(); 
	}); 
	</script> 
	<br>
	<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Modificando la informacion </strong></div> 
	<div class="panel-body"> 
		<?php while($row = $values->fetch_object()){ ?>
		<form class="container" name="form1" method="post" id="formid" action="?m=comunidad&f=savechanges"> 
			<p> 
				<p> 
				<label for="id_municipio"> 
					<strong>Municipio:</strong> 
					<select name="id_municipio" id="id_municipio" class="form-control"> 
					<?php while($row_municipio = $value_municipio -> fetch_object()){ ?>
						<option value="<?= $row_municipio->id_municipio; ?>" <?php if($row->id_municipio==$row_municipio->id_municipio){ echo "selected"; } ?>><?= $row_municipio->municipio; ?></option> 
					<?php } ?>
					</select> 
				</label> 
				</p> 
				<p> 
				<label for="nombre_comunidad"> 
					<strong>Comunidad:</strong> 
					<input type="text" name="nombre_comunidad" id="nombre_comunidad" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->nombre_comunidad, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="latitud"> 
					<strong>Latitud:</strong> 
					<input type="text" name="latitud" id="latitud" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->latitud, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="longitud"> 
					<strong>Longitud:</strong> 
					<input type="text" name="longitud" id="longitud" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->longitud, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
			</p> 
			<p>
				<input type="hidden" name="id_comunidad" id="id_comunidad" value="<?= $id_comunidad; ?>"/>
				<button type="submit" name="Enviar" class="btn btn-success" /><span class="glyphicon glyphicon-floppy-disk"></span> <span class="hidden-xs">Guardar Cambios</span></button> <a href="?m=comunidad&f=lista" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span> <span class="hidden-xs">Cancelar</span></a>
			</p>
		</form> 
		<?php } ?>
	</div> 
