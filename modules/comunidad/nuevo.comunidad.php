<?php
require(SYSTEM.'helpers/date.code_helper'.EXT);
require_once(MODULES.'comunidad/db.comunidad'.EXT);

$pro = new comunidad();

$campos_municipio = array('municipio', 'id_municipio');
$value_municipio = $pro->_select_comunidad($campos_municipio, NULL, NULL, NULL, "municipio");
if(!$value_municipio)echo $pro->error;

?>
	<script> 
	$(function(){ 
		$('#id_municipio').select(); 
		$.validate(); 
	}); 
	</script> 
	<br>
	<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Ingresando nueva informacion </strong></div> 
	<div class="panel-body"> 
		<form class="container" name="form1" method="post" id="formid" action="?m=comunidad&f=save"> 
			<p> 
				<p> 
				<label for="id_municipio"> 
					<strong>Municipio:</strong> 
					<select name="id_municipio" id="id_municipio" class="form-control"> 
					<?php while($row_municipio = $value_municipio -> fetch_object()){ ?>
						<option value="<?= $row_municipio->id_municipio; ?>"><?= $row_municipio->municipio; ?></option> 
					<?php } ?>
					</select> 
				</label> 
				</p> 
				<p> 
				<label for="nombre_comunidad"> 
					<strong>Comunidad:</strong> 
					<input type="text" name="nombre_comunidad" id="nombre_comunidad" data-validation="required" class="form-control" size="40" />
				</label> 
				</p> 
				<p> 
				<label for="latitud"> 
					<strong>Latitud:</strong> 
					<input type="text" name="latitud" id="latitud" data-validation="required" class="form-control" size="40" />
				</label> 
				</p> 
				<p> 
				<label for="longitud"> 
					<strong>Longitud:</strong> 
					<input type="text" name="longitud" id="longitud" data-validation="required" class="form-control" size="40" />
				</label> 
				</p> 
			</p> 
			<p>
				<button type="submit" name="Enviar" class="btn btn-success" /><span class="glyphicon glyphicon-floppy-disk"></span> <span class="hidden-xs">Guardar Informacion</span></button> <a href="?m=comunidad&f=lista" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span><span class="hidden-xs"> Cancelar</span></a>
			</p>
		</form> 
	</div> 
