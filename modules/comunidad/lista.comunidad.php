<?php

require_once(MODULES.'comunidad/db.comunidad'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new comunidad();

$ff = array("comunidad.id_comunidad", "comunidad.nombre_comunidad", "comunidad.latitud", "comunidad.longitud", "municipio.municipio");
$tt = "comunidad";
$jt = array("municipio");
$on = array(
"municipio.id_municipio" => "comunidad.id_municipio" 
 );

$where_u = "";

$values = $new->_call_multiple_left_join($ff, $jt, $on, $where_u);

?>
<script> 
$(function(){ 
	$('#sort').dataTable(); 
	$('[title]').tooltip(); 
}); 
function eliminar(id){
	bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
		if(result){
			window.location="?m=comunidad&f=eliminar&id_comunidad="+id;
		}
	});
}
</script> 
<br> 
<h4 class="page-header">Comunidad</h4>
	<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Listado General </strong></div> 
	<div class="panel-body"> 
	<p><a href="?m=comunidad&f=nuevo" class='btn btn-success'><span class="glyphicon glyphicon-share"></span><span class="hidden-xs"> Crear nueva comunidad</span></a></p> 
		<div class="table-responsive">
		<table class="table table-bordered table-condensed table-hover" id="sort"> 
			<thead> 
				<tr class="text-center"> 
					<th><strong>#</strong></th>
					<th><strong>Municipio</strong></th>
					<th><strong>Comunidad</strong></th>
					<th><strong>Latitud</strong></th>
					<th><strong>Longitud</strong></th>
					<th class="text-center">Opciones</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php
				$count=0;
				while($row = $values->fetch_object()){ 
					$count++;
				?>
				<tr> 
					<td><?= $count; ?></td>
					<td><?= $row->municipio; ?></td>
					<td><?= htmlspecialchars_decode($row->nombre_comunidad, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->latitud, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->longitud, ENT_QUOTES); ?></td>
					<td class="text-center">
						<a href="?m=comunidad&f=editar&id_comunidad=<?= $row->id_comunidad; ?>" title="Modificar" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span> Modificar</a>
						<a href="javascript:eliminar(<?= $row->id_comunidad; ?>)" title="Eliminar" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Eliminar</a>
					</td>
				</tr> 
				<?php } ?> 
			</tbody> 
		</table> 
		</div> 
	</div> 
