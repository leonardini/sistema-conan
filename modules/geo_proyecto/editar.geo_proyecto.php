<?php
require(SYSTEM.'helpers/date.code_helper'.EXT);
require_once(MODULES.'geo_proyecto/db.geo_proyecto'.EXT);

$pro = new geo_proyecto();

$campos_proyecto = array('nombre_proyecto', 'id_proyecto');
$value_proyecto = $pro->_select_geo_proyecto($campos_proyecto, NULL, NULL, NULL, "proyecto");
if(!$value_proyecto)echo $pro->error;


$campos_iconos = array('clasificador', 'id_iconos');
$value_iconos = $pro->_select_geo_proyecto($campos_iconos, NULL, NULL, NULL, "iconos");
if(!$value_iconos)echo $pro->error;


$campos_municipio = array('municipio', 'id_municipio');
$value_municipio = $pro->_select_geo_proyecto($campos_municipio, NULL, NULL, NULL, "municipio");
if(!$value_municipio)echo $pro->error;

// Recibiendo variable
$id_geo_proyecto = addslashes(trim($_GET['id_geo_proyecto']));

$news = new geo_proyecto();

// Estableciendo parametro recibido
$where = array("id_geo_proyecto" => "$id_geo_proyecto");
$values = $news->_select_geo_proyecto('*', $where);
if(!$values)echo $news->error;
$news->close();
?>
	<script> 
	$(function(){ 
		$('#id_proyecto').select(); 
		$.validate(); 
	}); 
	</script> 
	<br>
	<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Modificando la informacion </strong></div> 
	<div class="panel-body"> 
		<?php while($row = $values->fetch_object()){ ?>
		<form class="container" name="form1" method="post" id="formid" action="?m=geo_proyecto&f=savechanges"> 
			<p> 
				<p> 
				<label for="id_proyecto"> 
					<strong>Proyecto:</strong> 
					<select name="id_proyecto" id="id_proyecto" class="form-control"> 
					<?php while($row_proyecto = $value_proyecto -> fetch_object()){ ?>
						<option value="<?= $row_proyecto->id_proyecto; ?>" <?php if($row->id_proyecto==$row_proyecto->id_proyecto){ echo "selected"; } ?>><?= $row_proyecto->nombre_proyecto; ?></option> 
					<?php } ?>
					</select> 
				</label> 
				</p> 
				<p> 
				<label for="id_iconos"> 
					<strong>Iconos:</strong> 
					<select name="id_iconos" id="id_iconos" class="form-control"> 
					<?php while($row_iconos = $value_iconos -> fetch_object()){ ?>
						<option value="<?= $row_iconos->id_iconos; ?>" <?php if($row->id_iconos==$row_iconos->id_iconos){ echo "selected"; } ?>><?= $row_iconos->clasificador; ?></option> 
					<?php } ?>
					</select> 
				</label> 
				</p> 
				<p> 
				<label for="id_municipio"> 
					<strong>Municipio:</strong> 
					<select name="id_municipio" id="id_municipio" class="form-control"> 
					<?php while($row_municipio = $value_municipio -> fetch_object()){ ?>
						<option value="<?= $row_municipio->id_municipio; ?>" <?php if($row->id_municipio==$row_municipio->id_municipio){ echo "selected"; } ?>><?= $row_municipio->municipio; ?></option> 
					<?php } ?>
					</select> 
				</label> 
				</p> 
				<p> 
				<label for="latitud"> 
					<strong>Latitud:</strong> 
					<input type="text" name="latitud" id="latitud" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->latitud, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="longitud"> 
					<strong>Longitud:</strong> 
					<input type="text" name="longitud" id="longitud" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->longitud, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
			</p> 
			<p>
				<input type="hidden" name="id_geo_proyecto" id="id_geo_proyecto" value="<?= $id_geo_proyecto; ?>"/>
				<button type="submit" name="Enviar" class="btn btn-success" /><span class="glyphicon glyphicon-floppy-disk"></span> <span class="hidden-xs">Guardar Cambios</span></button> <a href="?m=geo_proyecto&f=lista" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span> <span class="hidden-xs">Cancelar</span></a>
			</p>
		</form> 
		<?php } ?>
	</div> 
