<?php

/*
*   Vista regular de los campos de una base de datos
*   Powered by OHK
*/

require_once(MODULES.'geo_proyecto/db.geo_proyecto'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new geo_proyecto();

$id_geo_proyecto = addslashes(trim($_GET['id_geo_proyecto']));
$ff = array("geo_proyecto.id_geo_proyecto", "geo_proyecto.latitud", "geo_proyecto.longitud", "proyecto.nombre_proyecto", "iconos.clasificador", "municipio.municipio");
$tt = "geo_proyecto";
$jt = array("proyecto", "iconos", "municipio");
$on = array(
"proyecto.id_proyecto" => "geo_proyecto.id_proyecto" , 
"iconos.id_iconos" => "geo_proyecto.id_iconos" , 
"municipio.id_municipio" => "geo_proyecto.id_municipio" 
 );

$where_u = array("geo_proyecto.id_geo_proyecto" => "$id_geo_proyecto");

$values = $new->_call_multiple_left_join($ff, $jt, $on, $where_u);
if(!$values)echo $new->error;
$new->close();
?>

	<script> 
	function eliminar(id){
		bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
			if(result){
				window.location="?m=geo_proyecto&f=eliminar&id_geo_proyecto="+id;
			}
		});
	}
	</script> 
	<div class="panel panel-primary"> 
	<div class="panel-heading"><strong> Vista de geo_proyecto </strong></div> 
	<div class="panel-body"> 
	<p>Opciones en geo_proyecto: </p> 
	<p> 
		<a href="?m=geo_proyecto&f=lista" class='btn btn-success'><span class="glyphicon glyphicon-list"></span><span class="hidden-xs"> Listado</span></a> 
		<a href="?m=geo_proyecto&f=nuevo" class='btn btn-primary'><span class="glyphicon glyphicon-plus"></span><span class="hidden-xs"> Nuevo(a)</span></a> 
		<a href="?m=geo_proyecto&f=editar&id_geo_proyecto=<?= $id_geo_proyecto; ?>" class='btn btn-warning'><span class="glyphicon glyphicon-pencil"></span><span class="hidden-xs"> Modificar</span></a> 
		<a href="javascript:eliminar(<?= $id_geo_proyecto; ?>)" class='btn btn-danger'><span class="glyphicon glyphicon-remove"></span><span class="hidden-xs"> Eliminar</span></a> 
	</p> 
		<div class="panel panel-default"> 
			<div class="panel-heading"><strong>geo_proyecto</strong></div> 
			<div class="panel-body"> 
			<p>Vista única de geo_proyecto</p> 
			</div> 
			<div class="table-responsive"> 
			<?php while($row = $values->fetch_object()){ ?> 
			<table class="table table-condensed table-bordered table-hover"> 
				<tbody> 
					<tr>
						<th><strong>#</strong></th>
						<td><?= $row->id_geo_proyecto; ?></td>
					</tr>
					<tr>
						<th><strong>Proyecto</strong></th>
						<td><?= $row->nombre_proyecto; ?></td>
					</tr>
					<tr>
						<th><strong>Iconos</strong></th>
						<td><?= $row->clasificador; ?></td>
					</tr>
					<tr>
						<th><strong>Municipio</strong></th>
						<td><?= $row->municipio; ?></td>
					</tr>
					<tr>
						<th><strong>Latitud</strong></th>
						<td><?= htmlspecialchars_decode($row->latitud, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Longitud</strong></th>
						<td><?= htmlspecialchars_decode($row->longitud, ENT_QUOTES); ?></td>
					</tr>
				</tbody> 
			</table> 
			<?php } ?> 
		</div>
	</div>
</div>
