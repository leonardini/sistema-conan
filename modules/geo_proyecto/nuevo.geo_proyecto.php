<?php
require(SYSTEM.'helpers/date.code_helper'.EXT);
require_once(MODULES.'geo_proyecto/db.geo_proyecto'.EXT);

$pro = new geo_proyecto();

$campos_proyecto = array('nombre_proyecto', 'id_proyecto');
$value_proyecto = $pro->_select_geo_proyecto($campos_proyecto, NULL, NULL, NULL, "proyecto");
if(!$value_proyecto)echo $pro->error;


$campos_iconos = array('clasificador', 'id_iconos');
$value_iconos = $pro->_select_geo_proyecto($campos_iconos, NULL, NULL, NULL, "iconos");
if(!$value_iconos)echo $pro->error;


$campos_municipio = array('municipio', 'id_municipio');
$value_municipio = $pro->_select_geo_proyecto($campos_municipio, NULL, NULL, NULL, "municipio");
if(!$value_municipio)echo $pro->error;

?>
	<script> 
	$(function(){ 
		$('#id_proyecto').select(); 
		$.validate(); 
	}); 
	</script> 
	<br>
	<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Ingresando nueva informacion </strong></div> 
	<div class="panel-body"> 
		<form class="container" name="form1" method="post" id="formid" action="?m=geo_proyecto&f=save"> 
			<p> 
				<p> 
				<label for="id_proyecto"> 
					<strong>Proyecto:</strong> 
					<select name="id_proyecto" id="id_proyecto" class="form-control"> 
					<?php while($row_proyecto = $value_proyecto -> fetch_object()){ ?>
						<option value="<?= $row_proyecto->id_proyecto; ?>"><?= $row_proyecto->nombre_proyecto; ?></option> 
					<?php } ?>
					</select> 
				</label> 
				</p> 
				<p> 
				<label for="id_iconos"> 
					<strong>Iconos:</strong> 
					<select name="id_iconos" id="id_iconos" class="form-control"> 
					<?php while($row_iconos = $value_iconos -> fetch_object()){ ?>
						<option value="<?= $row_iconos->id_iconos; ?>"><?= $row_iconos->clasificador; ?></option> 
					<?php } ?>
					</select> 
				</label> 
				</p> 
				<p> 
				<label for="id_municipio"> 
					<strong>Municipio:</strong> 
					<select name="id_municipio" id="id_municipio" class="form-control"> 
					<?php while($row_municipio = $value_municipio -> fetch_object()){ ?>
						<option value="<?= $row_municipio->id_municipio; ?>"><?= $row_municipio->municipio; ?></option> 
					<?php } ?>
					</select> 
				</label> 
				</p> 
				<p> 
				<label for="latitud"> 
					<strong>Latitud:</strong> 
					<input type="text" name="latitud" id="latitud" data-validation="required" class="form-control" size="40" />
				</label> 
				</p> 
				<p> 
				<label for="longitud"> 
					<strong>Longitud:</strong> 
					<input type="text" name="longitud" id="longitud" data-validation="required" class="form-control" size="40" />
				</label> 
				</p> 
			</p> 
			<p>
				<button type="submit" name="Enviar" class="btn btn-success" /><span class="glyphicon glyphicon-floppy-disk"></span> <span class="hidden-xs">Guardar Informacion</span></button> <a href="?m=geo_proyecto&f=lista" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span><span class="hidden-xs"> Cancelar</span></a>
			</p>
		</form> 
	</div> 
