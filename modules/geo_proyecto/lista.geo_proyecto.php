<?php

require_once(MODULES.'geo_proyecto/db.geo_proyecto'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new geo_proyecto();

$ff = array("geo_proyecto.id_geo_proyecto", "geo_proyecto.latitud", "geo_proyecto.longitud", "proyecto.nombre_proyecto", "iconos.clasificador", "municipio.municipio");
$tt = "geo_proyecto";
$jt = array("proyecto", "iconos", "municipio");
$on = array(
"proyecto.id_proyecto" => "geo_proyecto.id_proyecto" , 
"iconos.id_iconos" => "geo_proyecto.id_iconos" , 
"municipio.id_municipio" => "geo_proyecto.id_municipio" 
 );

$where_u = "";

$values = $new->_call_multiple_left_join($ff, $jt, $on, $where_u);
?>
<script> 
$(function(){ 
	$('#sort').dataTable(); 
	$('[title]').tooltip(); 
}); 
function eliminar(id){
	bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
		if(result){
			window.location="?m=geo_proyecto&f=eliminar&id_geo_proyecto="+id;
		}
	});
}
</script> 
<br> 
<h4 class="page-header">Geo-Referenciar Proyectos</h4>
<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Listado General </strong></div> 
	<div class="panel-body"> 
	<p><a href="?m=geo_proyecto&f=nuevo" class='btn btn-success'><span class="glyphicon glyphicon-map-marker"></span><span class="hidden-xs"> Referenciar proyecto</span></a></p> 
		<div class="table-responsive">
		<table class="table table-bordered table-condensed table-hover" id="sort"> 
			<thead> 
				<tr class="text-center"> 
					<th><strong>#</strong></th>
					<th><strong>Proyecto</strong></th>
					<th><strong>Iconos</strong></th>
					<th><strong>Municipio</strong></th>
					<th><strong>Latitud</strong></th>
					<th><strong>Longitud</strong></th>
					<th class="text-center">Opciones</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php
				$count=0;
				while($row = $values->fetch_object()){ 
					$count++;
				?>
				<tr> 
					<td><?= $count; ?></td>
					<td><?= $row->nombre_proyecto; ?></td>
					<td><?= $row->clasificador; ?></td>
					<td><?= $row->municipio; ?></td>
					<td><?= htmlspecialchars_decode($row->latitud, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->longitud, ENT_QUOTES); ?></td>
					<td class="text-center">
						<a href="?m=geo_proyecto&f=editar&id_geo_proyecto=<?= $row->id_geo_proyecto; ?>" title="Modificar" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span> Modificar</a>
						<a href="javascript:eliminar(<?= $row->id_geo_proyecto; ?>)" title="Eliminar" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Eliminar</a>
					</td>
				</tr> 
				<?php } ?> 
			</tbody> 
		</table> 
		</div> 
	</div> 
