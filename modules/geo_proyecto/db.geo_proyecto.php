<?php
/*
*   Las clases a seguir con las básicas requeridas para
*   el funcionamiento de los A,B,M,S
*   Powered by OHK
*/

// Inclusion del arhivo de MYSQLI 
// Que incluye las clases necesarias 
require_once(DBMYSQLI); 

// Clase geo_proyecto


class geo_proyecto extends DB{ 

	private $table = "geo_proyecto";

	public function __construct(){
		DB::__construct(); 
	}
	public function _insert_geo_proyecto($fields){
		$insert = DB::getInsertSql($this->table, $fields); 
		$insert = DB::getInsertSql($this->table, $fields); 
		$last_insert = DB::getInsertId(DB::execute($insert)); 
		return $last_insert; 
	}

	public function _select_all_geo_proyecto(){ 
		$query_ = DB::_arma_select('*', $this->table); 
		return DB::execute($query_); 
	}

	public function _prepare_insert($table, $fields){ 
		return DB::getInsertSql($table, $fields); 
	}

	// Realiza la insercion de la tabla 
	public function _make_insert_geo_proyecto($fields){ 
		$query_ = DB::_arma_insert($fields, $this->table); 
		$last_insert = DB::getInsertId(DB::execute($query_)); 
		return $last_insert; 
	}

	// Realiza la actualizacion de campos a la tabla 
	public function _make_update_geo_proyecto($fields, $where){ 
		$udp_ = DB::_arma_update($this->table); 
		$set_ = DB::_arma_set($fields); 
		$where_ = DB::_arma_where($where); 
		$query_ = $udp_.$set_.$where_; 
		DB::execute($query_); 
		return TRUE; 
	}

	// Realiza la eliminacion fisica de los datos 
	public function _make_delete_geo_proyecto($where){ 
		$delete_ = DB::_arma_delete($this->table); 
		$where_ = DB::_arma_where($where); 
		$query_ = $delete_.$where_; 
		DB::execute($query_); 
		return TRUE; 
	}

	// Selecciona los campos de una geo_proyecto 
	// De a cuerdo a un ID 
	public function _select_all_geo_proyecto_id($id){ 
		$fields = DB::_arma_select('*', $this->table); 
		$where = DB::_arma_where($id); 
		$query_ = $fields.$where; 
		$result = DB::execute($query_); 
		return $result; 
	}

	// Selecciona los campos de una geo_proyecto 
	// De acuerdo a varios parametros. 
	public function _select_geo_proyecto($fields='*', $where=FALSE, $order=FALSE, $limit=FALSE, $table=FALSE){ 
		if(!$table) $table = $this->table; 
		$fields = DB::_arma_select($fields, $table); 
		if($where) $where = DB::_arma_where($where); else $where = ""; 
		if($order) $order = DB::_arma_order($order); else $order = ""; 
		if($limit) $limit = DB::_arma_limit($limit); else $limit = ""; 
		$query_ = $fields.$where.$order.$limit; 
		return DB::execute($query_); 
	}

	// Funcion que crea multiples joins

	public function _call_multiple_left_join($fields, $join_table, $on, $where){
		$query_ = DB::_arma_multiple_join($fields, $this->table, $join_table, $on, $where); 
		return DB::execute($query_); 
	}

	// Funcion que ejecuta cualquier consulta

	public function ejecutar($query){
		return DB::execute($query); 
	}

	// Muestra errores 
	public function _error_deleting(){ 
		echo "Error al eliminar los datos."; 
	}

}

?>