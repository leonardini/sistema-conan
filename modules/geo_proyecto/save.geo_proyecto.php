<?php
// Incluyendo archivo de la base de Datos
require_once(MODULES.'geo_proyecto/db.geo_proyecto'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
// Inicializando la clase de la base de Datos
$new = new geo_proyecto();
// Recibiendo y limpiando Datos
$id_geo_proyecto = $_POST['id_geo_proyecto'];
$id_proyecto = $_POST['id_proyecto'];
$id_iconos = $_POST['id_iconos'];
$id_municipio = $_POST['id_municipio'];
$latitud = htmlspecialchars($_POST['latitud'], ENT_QUOTES);
$longitud = htmlspecialchars($_POST['longitud'], ENT_QUOTES);
$insert = array("id_proyecto" => "$id_proyecto", "id_iconos" => "$id_iconos", "id_municipio" => "$id_municipio", "latitud" => "$latitud", "longitud" => "$longitud");
$last_insert = $new->_make_insert_geo_proyecto($insert);
header("Location: in.php?m=geo_proyecto&f=lista");
exit;

?>
