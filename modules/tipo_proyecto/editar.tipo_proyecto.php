<?php
require(SYSTEM.'helpers/date.code_helper'.EXT);
require_once(MODULES.'tipo_proyecto/db.tipo_proyecto'.EXT);
// Recibiendo variable
$id_tipo_proyecto = addslashes(trim($_GET['id_tipo_proyecto']));

$news = new tipo_proyecto();

// Estableciendo parametro recibido
$where = array("id_tipo_proyecto" => "$id_tipo_proyecto");
$values = $news->_select_tipo_proyecto('*', $where);
if(!$values)echo $news->error;

?>
<script> 
$(function(){ 
	$('#nombre_tipo').select(); 
	$.validate(); 
}); 
</script> 
<br>
<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Modificando la informacion </strong></div> 
	<div class="panel-body"> 
		<?php while($row = $values->fetch_object()){ ?>
		<form class="container" name="form1" method="post" id="formid" action="?m=tipo_proyecto&f=savechanges"> 
			<p> 
				<p> 
				<label for="nombre_tipo"> 
					<strong>Tipos de Proyectos:</strong> 
					<input type="text" name="nombre_tipo" id="nombre_tipo" size="40" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->nombre_tipo, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
			</p> 
			<p>
				<input type="hidden" name="id_tipo_proyecto" id="id_tipo_proyecto" value="<?= $id_tipo_proyecto; ?>"/>
				<button type="submit" name="Enviar" class="btn btn-success" /><span class="glyphicon glyphicon-floppy-disk"></span> <span class="hidden-xs">Guardar Cambios</span></button> <a href="?m=tipo_proyecto&f=lista" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span> <span class="hidden-xs">Cancelar</span></a>
			</p>
		</form> 
		<?php } ?>
	</div> 
