<?php

/*
*   Vista regular de los campos de una base de datos
*   Powered by OHK
*/

require_once(MODULES.'tipo_proyecto/db.tipo_proyecto'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new tipo_proyecto();

$id_tipo_proyecto = addslashes(trim($_GET['id_tipo_proyecto']));

$where_u = array("id_tipo_proyecto" => "$id_tipo_proyecto");

$values = $new->_select_tipo_proyecto('*', $where_u);
if(!$values)echo $new->error;
$new->close();
?>

	<script> 
	function eliminar(id){
		bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
			if(result){
				window.location="?m=tipo_proyecto&f=eliminar&id_tipo_proyecto="+id;
			}
		});
	}
	</script> 
	<div class="panel panel-primary"> 
	<div class="panel-heading"><strong> Vista de tipo_proyecto </strong></div> 
	<div class="panel-body"> 
	<p>Opciones en tipo_proyecto: </p> 
	<p> 
		<a href="?m=tipo_proyecto&f=lista" class='btn btn-success'><span class="glyphicon glyphicon-list"></span><span class="hidden-xs"> Listado</span></a> 
		<a href="?m=tipo_proyecto&f=nuevo" class='btn btn-primary'><span class="glyphicon glyphicon-plus"></span><span class="hidden-xs"> Nuevo(a)</span></a> 
		<a href="?m=tipo_proyecto&f=editar&id_tipo_proyecto=<?= $id_tipo_proyecto; ?>" class='btn btn-warning'><span class="glyphicon glyphicon-pencil"></span><span class="hidden-xs"> Modificar</span></a> 
		<a href="javascript:eliminar(<?= $id_tipo_proyecto; ?>)" class='btn btn-danger'><span class="glyphicon glyphicon-remove"></span><span class="hidden-xs"> Eliminar</span></a> 
	</p> 
		<div class="panel panel-default"> 
			<div class="panel-heading"><strong>tipo_proyecto</strong></div> 
			<div class="panel-body"> 
			<p>Vista única de tipo_proyecto</p> 
			</div> 
			<div class="table-responsive"> 
			<?php while($row = $values->fetch_object()){ ?> 
			<table class="table table-condensed table-bordered table-hover"> 
				<tbody> 
					<tr>
						<th><strong>#</strong></th>
						<td><?= $row->id_tipo_proyecto; ?></td>
					</tr>
					<tr>
						<th><strong>Tipos de Proyectos</strong></th>
						<td><?= htmlspecialchars_decode($row->nombre_tipo, ENT_QUOTES); ?></td>
					</tr>
				</tbody> 
			</table> 
			<?php } ?> 
		</div>
	</div>
</div>
