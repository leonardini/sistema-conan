<?php

/*
*   Lista regular de los campos de una base de datos
*   Powered by OHK
*/

require_once(MODULES.'tipo_proyecto/db.tipo_proyecto'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new tipo_proyecto();

$campos = array('*');
$limit = "0,150";
$where_u = "";

$values = $new->_select_tipo_proyecto('*', $where_u);

?>
<script> 
$(function(){ 
	$('#sort').dataTable(); 
	$('[title]').tooltip(); 
}); 
function eliminar(id){
	bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
		if(result){
			window.location="?m=tipo_proyecto&f=eliminar&id_tipo_proyecto="+id;
		}
	});
}
</script> 
<br> 
<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Listado General </strong></div> 
	<div class="panel-body"> 
	<p><a href="?m=tipo_proyecto&f=nuevo" class='btn btn-success'><span class="glyphicon glyphicon-share"></span><span class="hidden-xs"> Crear nuevo tipo de Proyecto</span></a></p> 
		<div class="table-responsive">
		<table class="table table-bordered table-condensed table-hover" id="sort"> 
			<thead> 
				<tr class="text-center"> 
					<th><strong>#</strong></th>
					<th><strong>Tipos de Proyectos</strong></th>
					<th class="text-center">Opciones</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php
				$count=0;
				while($row = $values->fetch_object()){ 
					$count++;
				?>
				<tr> 
					<td><?= $count; ?></td>
					<td><?= htmlspecialchars_decode($row->nombre_tipo, ENT_QUOTES); ?></td>
					<td class="text-center">
						<a href="?m=tipo_proyecto&f=editar&id_tipo_proyecto=<?= $row->id_tipo_proyecto; ?>" title="Modificar" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span> Modificar</a>
						<a href="javascript:eliminar(<?= $row->id_tipo_proyecto; ?>)" title="Eliminar" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Eliminar</a>
					</td>
				</tr> 
				<?php } ?> 
			</tbody> 
		</table> 
		</div> 
	</div> 
