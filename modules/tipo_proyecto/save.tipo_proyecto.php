<?php
// Incluyendo archivo de la base de Datos
require_once(MODULES.'tipo_proyecto/db.tipo_proyecto'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
// Inicializando la clase de la base de Datos
$new = new tipo_proyecto();
// Recibiendo y limpiando Datos
$id_tipo_proyecto = $_POST['id_tipo_proyecto'];
$nombre_tipo = htmlspecialchars($_POST['nombre_tipo'], ENT_QUOTES);
$insert = array("nombre_tipo" => "$nombre_tipo");
$last_insert = $new->_make_insert_tipo_proyecto($insert);
header("Location: in.php?m=tipo_proyecto&f=lista");
exit;

?>
