<?php 

require_once('../../system/config/database.php');
require_once('../../system/database/DBMySQLi.php');


if(isset($_POST['data']))
{
	$data = $_POST['data'];
	$array = json_decode($data,true);
	$id_municipio = str_replace(array("[","]",'"'), "", json_encode($array['municipios']));
	$id_departamento = str_replace(array("[","]",'"'), "", json_encode($array['departamentos']));
	$id_indicador = str_replace(array("[","]",'"'), "", json_encode($array['indicadores']));
	
	$db = new DB();
	$consulta = "
		SELECT
		geo_indicador.valor,
		geo_indicador.id_iconos,
		municipio.municipio,
		municipio.codine,
		municipio.longitud,
		municipio.latitud,
		municipio.id_municipio,
		geo_indicador.id_geo_indicador,
		indicador.codigo_indicador,
		indicador.nombre_indicador
		FROM
		geo_indicador
		INNER JOIN municipio ON municipio.id_municipio = geo_indicador.id_municipio
		INNER JOIN indicador ON indicador.id_indicador = geo_indicador.id_indicador 
		WHERE
		municipio.id_municipio in ($id_municipio)
		AND
		municipio.id_dpto in ($id_departamento)
		AND
		indicador.id_indicador in ($id_indicador)
	";
	$respuesta = $db->execute($consulta);

	$array = array();
	while($row = $respuesta->fetch_object())
		$array[] = $row;

	$array = json_encode($array);
	echo $array;
}
?>