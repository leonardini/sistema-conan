<?php

/*
*   Vista regular de los campos de una base de datos
*   Powered by OHK
*/

require_once(MODULES.'geo_indicador/db.geo_indicador'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new geo_indicador();

$id_geo_indicador = addslashes(trim($_GET['id_geo_indicador']));
$ff = array("geo_indicador.id_geo_indicador", "geo_indicador.gestion", "geo_indicador.valor", "geo_indicador.latitud", "geo_indicador.longitud", "indicador.codigo_indicador", "iconos.clasificador", "municipio.municipio");
$tt = "geo_indicador";
$jt = array("indicador", "iconos", "municipio");
$on = array(
"indicador.id_indicador" => "geo_indicador.id_indicador" , 
"iconos.id_iconos" => "geo_indicador.id_iconos" , 
"municipio.id_municipio" => "geo_indicador.id_municipio" 
 );

$where_u = array("geo_indicador.id_geo_indicador" => "$id_geo_indicador");

$values = $new->_call_multiple_left_join($ff, $jt, $on, $where_u);
if(!$values)echo $new->error;
$new->close();
?>

	<script> 
	function eliminar(id){
		bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
			if(result){
				window.location="?m=geo_indicador&f=eliminar&id_geo_indicador="+id;
			}
		});
	}
	</script> 
	<div class="panel panel-primary"> 
	<div class="panel-heading"><strong> Vista de geo_indicador </strong></div> 
	<div class="panel-body"> 
	<p>Opciones en geo_indicador: </p> 
	<p> 
		<a href="?m=geo_indicador&f=lista" class='btn btn-success'><span class="glyphicon glyphicon-list"></span><span class="hidden-xs"> Listado</span></a> 
		<a href="?m=geo_indicador&f=nuevo" class='btn btn-primary'><span class="glyphicon glyphicon-plus"></span><span class="hidden-xs"> Nuevo(a)</span></a> 
		<a href="?m=geo_indicador&f=editar&id_geo_indicador=<?= $id_geo_indicador; ?>" class='btn btn-warning'><span class="glyphicon glyphicon-pencil"></span><span class="hidden-xs"> Modificar</span></a> 
		<a href="javascript:eliminar(<?= $id_geo_indicador; ?>)" class='btn btn-danger'><span class="glyphicon glyphicon-remove"></span><span class="hidden-xs"> Eliminar</span></a> 
	</p> 
		<div class="panel panel-default"> 
			<div class="panel-heading"><strong>geo_indicador</strong></div> 
			<div class="panel-body"> 
			<p>Vista única de geo_indicador</p> 
			</div> 
			<div class="table-responsive"> 
			<?php while($row = $values->fetch_object()){ ?> 
			<table class="table table-condensed table-bordered table-hover"> 
				<tbody> 
					<tr>
						<th><strong>#</strong></th>
						<td><?= $row->id_geo_indicador; ?></td>
					</tr>
					<tr>
						<th><strong>Indicador</strong></th>
						<td><?= $row->codigo_indicador; ?></td>
					</tr>
					<tr>
						<th><strong>Iconos</strong></th>
						<td><?= $row->clasificador; ?></td>
					</tr>
					<tr>
						<th><strong>Municipio</strong></th>
						<td><?= $row->municipio; ?></td>
					</tr>
					<tr>
						<th><strong>Gestion</strong></th>
						<td><?= htmlspecialchars_decode($row->gestion, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Valor del Indicador</strong></th>
						<td><?= htmlspecialchars_decode($row->valor, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Latitud</strong></th>
						<td><?= htmlspecialchars_decode($row->latitud, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Longitud</strong></th>
						<td><?= htmlspecialchars_decode($row->longitud, ENT_QUOTES); ?></td>
					</tr>
				</tbody> 
			</table> 
			<?php } ?> 
		</div>
	</div>
</div>
