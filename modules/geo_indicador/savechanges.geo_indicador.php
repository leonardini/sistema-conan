<?php
// Incluyendo archivo de la base de Datos
require_once(MODULES.'geo_indicador/db.geo_indicador'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
// Inicializando la clase de la base de Datos
$new = new geo_indicador();
// Recibiendo y limpiando Datos
$id_geo_indicador = $_POST['id_geo_indicador'];
$id_indicador = $_POST['id_indicador'];
$id_iconos = $_POST['id_iconos'];
$id_municipio = $_POST['id_municipio'];
$gestion = htmlspecialchars($_POST['gestion'], ENT_QUOTES);
$valor = htmlspecialchars($_POST['valor'], ENT_QUOTES);
$latitud = htmlspecialchars($_POST['latitud'], ENT_QUOTES);
$longitud = htmlspecialchars($_POST['longitud'], ENT_QUOTES);
$update = array("id_indicador" => "$id_indicador", "id_iconos" => "$id_iconos", "id_municipio" => "$id_municipio", "gestion" => "$gestion", "valor" => "$valor", "latitud" => "$latitud", "longitud" => "$longitud");
$where = array("id_geo_indicador" => "$id_geo_indicador");
$new->_make_update_geo_indicador($update, $where);
header("Location: in.php?m=geo_indicador&f=lista");
exit;

?>
