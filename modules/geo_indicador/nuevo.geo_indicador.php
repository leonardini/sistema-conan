<?php
require(SYSTEM.'helpers/date.code_helper'.EXT);
require_once(MODULES.'geo_indicador/db.geo_indicador'.EXT);

$pro = new geo_indicador();

$campos_indicador = array('codigo_indicador', 'id_indicador');
$value_indicador = $pro->_select_geo_indicador($campos_indicador, NULL, NULL, NULL, "indicador");
if(!$value_indicador)echo $pro->error;


$campos_iconos = array('clasificador', 'id_iconos');
$value_iconos = $pro->_select_geo_indicador($campos_iconos, NULL, NULL, NULL, "iconos");
if(!$value_iconos)echo $pro->error;


$campos_municipio = array('municipio', 'id_municipio');
$value_municipio = $pro->_select_geo_indicador($campos_municipio, NULL, 'municipio', NULL, "municipio");
if(!$value_municipio)echo $pro->error;

?>
	<script> 
	$(function(){ 
		$('#id_indicador').select(); 
		$.validate(); 
	}); 
	</script> 
	<br>
	<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Ingresando nueva informacion </strong></div> 
	<div class="panel-body"> 
		<form class="form-inline" id="idFormulario">
			<div class="row">
			    <div class="col-sm-5"><hr /></div>
			    <div class="col-sm-2"><center><h5>Datos Básicos Indicador</h5></center></div>
			    <div class="col-sm-5"><hr /></div>
			</div>
			<br>
			<div class="form-group">
			<label for="exampleInputName2">Indicador: </label>
				<!-- <input type="text" class="form-control" id="exampleInputName2" placeholder="Jane Doe"> -->
				<select name="id_indicador" id="id_indicador" class="form-control"> 
				<option value="-1">Seleccionar Indicador</option>
				<?php while($row_indicador = $value_indicador -> fetch_object()){ ?>
					<option value="<?= $row_indicador->id_indicador; ?>"><?= $row_indicador->codigo_indicador; ?></option> 
				<?php } ?>
				</select> 
			</div>
			<div class="form-group">
				<label for="exampleInputEmail2"> Gestión: </label>
				<!-- <input type="email" class="form-control" id="exampleInputEmail2" placeholder="jane.doe@example.com"> -->
				<input type="text" name="gestion" id="gestion" data-validation="required" class="form-control" size="5" placeholder="Gestión"/>
			</div>
			<div class="form-group">
				<label for="exampleInputEmail2"> Icono: </label>
				<label for="">  </label>
				<button id="idColor" type="button"><i class="fa fa-map-marker fa-2x"></i></button>
				<input type="color" name="color" id="idHiddenColor">
				</select> 
			</div>
			<br>
			<br>
			
			<div class="row">
			    <div class="col-sm-5"><hr /></div>
			    <div class="col-sm-2"><center><h5>Valores Indicador</h5></center></div>
			    <div class="col-sm-5"><hr /></div>
			</div>
			<div class="form-group">
				<button type="button" id="idAgregarIndicador" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Agregar Valor Indicador</button>
			</div>
			<div id="contenedorValorIndicadores">
				<div id="valoresIndicadores">
					<br>
					<div class="form-group">
						<label for="exampleInputEmail2"> Municipio: </label>
						<select name="detalle[][municipio]" id="id_municipio" class="form-control"> 
								<option value="-1">Seleccionar Municipio</option>
						
							<?php while($row_municipio = $value_municipio -> fetch_object()){ ?>
								<option value="<?= $row_municipio->id_municipio; ?>"><?= $row_municipio->municipio; ?></option> 
							<?php } ?>
							</select> 
						 
					</div>
					<div class="form-group">
						<label for="exampleInputEmail2"> Valor indicador: </label>
						<!-- <input type="email" class="form-control" id="exampleInputEmail2" placeholder="jane.doe@example.com"> -->
						<input type="text" name="detalle[][valor]" id="valor" data-validation="required" class="form-control" size="10" placeholder="Valor Indicador" />
					</div>
					<div id="divEliminar" class="form-group">
						<button type="button"  class="form-control btn btn-danger btn-xs eliminar">Eliminar</button>
					</div>
					<br>
				</div>
			</div>
			<hr>
			
		</form>
		<hr>
		<form class="container" name="form1" method="post" id="formid" action="?m=geo_indicador&f=save"> 
			<button id="idAlmacenarIndicadores" type="button" name="Enviar" class="btn btn-success" /><span class="glyphicon glyphicon-floppy-disk"></span> <span class="hidden-xs"><small>Guardar Indicadores Georeferenciados</small></span></button> <a href="?m=geo_indicador&f=lista" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span><span class="hidden-xs"> Cancelar</span></a>
			</p>
		</form> 
	</div> 

<script>
	$(document).ready(function(){
		//$("#divEliminar").addClass("hidden")
		$("#idColor").on("click",function(){
			$("#idHiddenColor").click()
		})

		$("#idHiddenColor").on("input",function(){
			$("#idColor").css('color', $(this).val());
		})

		$("#idAgregarIndicador").on("click",function(){
			$("#valoresIndicadores").clone().appendTo('#contenedorValorIndicadores'); 
			//var formHtml = $("#contenedorValorIndicadores").html()

			//$("#contenedorValorIndicadores").append(formHtml)
		})

		$("body").on("click",".eliminar",function(){
			var cant_min_1 = $('.eliminar').length
			if(cant_min_1 > 1)
				$(this).parent().parent().remove()
		})

		$("#idAlmacenarIndicadores").on("click", function(){
			var formulario = JSON.stringify($('#idFormulario').serializeJSON());
			var formularioJSON = $('#idFormulario').serializeJSON();

			if(validate(formularioJSON))
			{
				$.post('modules/geo_indicador/save_geo_indicadores.geo_indicador.php', {data: formulario}, function(data, textStatus, xhr) {
					if(data == "success")
					{
						
						document.location = "in.php?m=geo_indicador&f=lista"
						//alert("success")
						
					}
					else
					{
						//alert("failure")
					}
				});
			}
			else
			{
				alert("Por favor completa los campos antes de guardar los indicadores georeferenciados")
			}
		})
	})

	function validate(json) {
		var flag = true;

		if(json.indicador < "1" )
			flag = false;
		if(json.gestion.length  < 0 )
			flag = false;
				
		$.each(json.detalle, function(index, val) {
			if(val.municipio == "-1")
				flag = false;
			if(val.valor.length <= 0)
				flag = false;
		});
		console.log(flag)
		return flag;
	}
</script>

<style type="text/css">
	#idHiddenColor{
	display: none;
}
</style>