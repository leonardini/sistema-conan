<?php
require(SYSTEM.'helpers/date.code_helper'.EXT);
require_once(MODULES.'geo_indicador/db.geo_indicador'.EXT);

$pro = new geo_indicador();

$campos_indicador = array('codigo_indicador', 'id_indicador');
$value_indicador = $pro->_select_geo_indicador($campos_indicador, NULL, NULL, NULL, "indicador");
if(!$value_indicador)echo $pro->error;


$campos_iconos = array('clasificador', 'id_iconos');
$value_iconos = $pro->_select_geo_indicador($campos_iconos, NULL, NULL, NULL, "iconos");
if(!$value_iconos)echo $pro->error;


$campos_municipio = array('municipio', 'id_municipio');
$value_municipio = $pro->_select_geo_indicador($campos_municipio, NULL, NULL, NULL, "municipio");
if(!$value_municipio)echo $pro->error;

// Recibiendo variable
$id_geo_indicador = addslashes(trim($_GET['id_geo_indicador']));

$news = new geo_indicador();

// Estableciendo parametro recibido
$where = array("id_geo_indicador" => "$id_geo_indicador");
$values = $news->_select_geo_indicador('*', $where);
if(!$values)echo $news->error;
$news->close();
?>
	<script> 
	$(function(){ 
		$('#id_indicador').select(); 
		$.validate(); 
	}); 
	</script> 
	<br>
	<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Modificando la informacion </strong></div> 
	<div class="panel-body"> 
		<?php while($row = $values->fetch_object()){ ?>
		<form class="container" name="form1" method="post" id="formid" action="?m=geo_indicador&f=savechanges"> 
			<p> 
				<p> 
				<label for="id_indicador"> 
					<strong>Indicador:</strong> 
					<select name="id_indicador" id="id_indicador" class="form-control"> 
					<?php while($row_indicador = $value_indicador -> fetch_object()){ ?>
						<option value="<?= $row_indicador->id_indicador; ?>" <?php if($row->id_indicador==$row_indicador->id_indicador){ echo "selected"; } ?>><?= $row_indicador->codigo_indicador; ?></option> 
					<?php } ?>
					</select> 
				</label> 
				</p> 
				<!-- <p> 
				<label for="id_iconos"> 
					<strong>Iconos:</strong> 
					<select name="id_iconos" id="id_iconos" class="form-control"> 
					<?php while($row_iconos = $value_iconos -> fetch_object()){ ?>
						<option value="<?= $row_iconos->id_iconos; ?>" <?php if($row->id_iconos==$row_iconos->id_iconos){ echo "selected"; } ?>><?= $row_iconos->clasificador; ?></option> 
					<?php } ?>
					</select> 
				</label> 
				</p>  -->
				<p> 
				<label for="id_municipio"> 
					<strong>Municipio:</strong> 
					<select name="id_municipio" id="id_municipio" class="form-control"> 
					<?php while($row_municipio = $value_municipio -> fetch_object()){ ?>
						<option value="<?= $row_municipio->id_municipio; ?>" <?php if($row->id_municipio==$row_municipio->id_municipio){ echo "selected"; } ?>><?= $row_municipio->municipio; ?></option> 
					<?php } ?>
					</select> 
				</label> 
				</p> 
				<p> 
				<label for="gestion"> 
					<strong>Gestion:</strong> 
					<input type="text" name="gestion" id="gestion" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->gestion, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="valor"> 
					<strong>Valor del Indicador:</strong> 
					<input type="text" name="valor" id="valor" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->valor, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
			<!-- 	<label for="latitud"> 
			 	<strong>Latitud:</strong> 
			 	<input type="text" name="latitud" id="latitud" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->latitud, ENT_QUOTES); ?>"/> 
			 </label> 
			 </p> 
			 <p> 
			 <label for="longitud"> 
			 	<strong>Longitud:</strong> 
			 	<input type="text" name="longitud" id="longitud" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->longitud, ENT_QUOTES); ?>"/> 
			 </label> 
			 </p> --> 
			</p> 
			<p>
				<input type="hidden" name="id_geo_indicador" id="id_geo_indicador" value="<?= $id_geo_indicador; ?>"/>
				<button type="submit" name="Enviar" class="btn btn-success" /><span class="glyphicon glyphicon-floppy-disk"></span> <span class="hidden-xs">Guardar Cambios</span></button> <a href="?m=geo_indicador&f=lista" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span> <span class="hidden-xs">Cancelar</span></a>
			</p>
		</form> 
		<?php } ?>
	</div> 
