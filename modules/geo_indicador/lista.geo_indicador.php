<?php

require_once(MODULES.'geo_indicador/db.geo_indicador'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);

$new = new geo_indicador();

$ff = array("geo_indicador.id_geo_indicador", "geo_indicador.gestion", "geo_indicador.valor", "geo_indicador.latitud", "geo_indicador.longitud", "indicador.codigo_indicador", "iconos.clasificador", "municipio.municipio");
$tt = "geo_indicador";
$jt = array("indicador", "iconos", "municipio");
$on = array(
"indicador.id_indicador" => "geo_indicador.id_indicador" , 
"iconos.id_iconos" => "geo_indicador.id_iconos" , 
"municipio.id_municipio" => "geo_indicador.id_municipio" 
 );

$where_u = "";

$values = $new->_call_multiple_left_join($ff, $jt, $on, $where_u);
?>
<script> 
$(function(){ 
	$('#sort').dataTable(); 
	$('[title]').tooltip(); 
}); 
function eliminar(id){
	bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
		if(result){
			window.location="?m=geo_indicador&f=eliminar&id_geo_indicador="+id;
		}
	});
}
</script> 
<br>
<h4 class="page-header">Geo-Referenciar Indicadores</h4>
<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Listado General </strong></div> 
	<div class="panel-body"> 
	<p><a href="?m=geo_indicador&f=nuevo" class='btn btn-success'><span class="glyphicon glyphicon-map-marker"></span><span class="hidden-xs"> Referenciar nuevo Indicador</span></a></p> 
		<div class="table-responsive">
		<table class="table table-bordered table-condensed table-hover" id="sort"> 
			<thead> 
				<tr class="text-center"> 
					<th><strong>#</strong></th>
					<th><strong>Indicador</strong></th>
					<!-- <th><strong>Iconos</strong></th> -->
					<th><strong>Municipio</strong></th>
					<th><strong>Gestion</strong></th>
					<th><strong>Valor del Indicador</strong></th>
					<!-- <th><strong>Latitud</strong></th>
					<th><strong>Longitud</strong></th> -->
					<th class="text-center">Opciones</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php
				$count=0;
				while($row = $values->fetch_object()){ 
					$count++;
				?>
				<tr> 
					<td><?= $count; ?></td>
					<td><?= $row->codigo_indicador; ?></td>
					<!-- <td><?= $row->clasificador; ?></td> -->
					<td><?= $row->municipio; ?></td>
					<td><?= htmlspecialchars_decode($row->gestion, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->valor, ENT_QUOTES); ?></td>
					<!-- <td><?= htmlspecialchars_decode($row->latitud, ENT_QUOTES); ?></td> -->
					<!-- <td><?= htmlspecialchars_decode($row->longitud, ENT_QUOTES); ?></td> -->
					<td class="text-center">
						<a href="?m=geo_indicador&f=editar&id_geo_indicador=<?= $row->id_geo_indicador; ?>" title="Modificar" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span> Modificar</a>
						<a href="javascript:eliminar(<?= $row->id_geo_indicador; ?>)" title="Eliminar" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Eliminar</a>
					</td>
				</tr> 
				<?php } ?> 
			</tbody> 
		</table> 
		</div> 
	</div> 
