<?php

/*
*   Vista regular de los campos de una base de datos
*   Powered by OHK
*/

require_once(MODULES.'municipio/db.municipio'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new municipio();

$id_municipio = addslashes(trim($_GET['id_municipio']));
$ff = array("municipio.id_municipio", "municipio.codine", "municipio.municipio", "municipio.latitud", "municipio.longitud", "dpto.departamento");
$tt = "municipio";
$jt = array("dpto");
$on = array(
"dpto.id_dpto" => "municipio.id_dpto" 
 );

$where_u = array("municipio.id_municipio" => "$id_municipio");

$values = $new->_call_multiple_left_join($ff, $jt, $on, $where_u);
if(!$values)echo $new->error;
$new->close();
?>

	<script> 
	function eliminar(id){
		bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
			if(result){
				window.location="?m=municipio&f=eliminar&id_municipio="+id;
			}
		});
	}
	</script> 
	<div class="panel panel-primary"> 
	<div class="panel-heading"><strong> Vista de municipio </strong></div> 
	<div class="panel-body"> 
	<p>Opciones en municipio: </p> 
	<p> 
		<a href="?m=municipio&f=lista" class='btn btn-success'><span class="glyphicon glyphicon-list"></span><span class="hidden-xs"> Listado</span></a> 
		<a href="?m=municipio&f=nuevo" class='btn btn-primary'><span class="glyphicon glyphicon-plus"></span><span class="hidden-xs"> Nuevo(a)</span></a> 
		<a href="?m=municipio&f=editar&id_municipio=<?= $id_municipio; ?>" class='btn btn-warning'><span class="glyphicon glyphicon-pencil"></span><span class="hidden-xs"> Modificar</span></a> 
		<a href="javascript:eliminar(<?= $id_municipio; ?>)" class='btn btn-danger'><span class="glyphicon glyphicon-remove"></span><span class="hidden-xs"> Eliminar</span></a> 
	</p> 
		<div class="panel panel-default"> 
			<div class="panel-heading"><strong>municipio</strong></div> 
			<div class="panel-body"> 
			<p>Vista única de municipio</p> 
			</div> 
			<div class="table-responsive"> 
			<?php while($row = $values->fetch_object()){ ?> 
			<table class="table table-condensed table-bordered table-hover"> 
				<tbody> 
					<tr>
						<th><strong>#</strong></th>
						<td><?= $row->id_municipio; ?></td>
					</tr>
					<tr>
						<th><strong>Departamento</strong></th>
						<td><?= $row->departamento; ?></td>
					</tr>
					<tr>
						<th><strong>Codigo INE</strong></th>
						<td><?= htmlspecialchars_decode($row->codine, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Nombre del Municipio</strong></th>
						<td><?= htmlspecialchars_decode($row->municipio, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Latitud</strong></th>
						<td><?= htmlspecialchars_decode($row->latitud, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Longitud</strong></th>
						<td><?= htmlspecialchars_decode($row->longitud, ENT_QUOTES); ?></td>
					</tr>
				</tbody> 
			</table> 
			<?php } ?> 
		</div>
	</div>
</div>
