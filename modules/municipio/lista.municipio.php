<?php

/*
*   Lista regular de los campos de una base de datos
*   Powered by OHK
*/

require_once(MODULES.'municipio/db.municipio'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new municipio();

$ff = array("municipio.id_municipio", "municipio.codine", "municipio.municipio", "municipio.latitud", "municipio.longitud", "dpto.departamento");
$tt = "municipio";
$jt = array("dpto");
$on = array(
"dpto.id_dpto" => "municipio.id_dpto" 
 );

$where_u = "";

$values = $new->_call_multiple_left_join($ff, $jt, $on, $where_u);
?>
<script> 
$(function(){ 
	$('#sort').dataTable(); 
	$('[title]').tooltip(); 
}); 
function eliminar(id){
	bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
		if(result){
			window.location="?m=municipio&f=eliminar&id_municipio="+id;
		}
	});
}
</script> 
<br> 
<h4 class="page-header">Municipios</h4>
<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Listado General </strong></div> 
	<div class="panel-body"> 
	<p><a href="?m=municipio&f=nuevo" class='btn btn-success'><span class="glyphicon glyphicon-share"></span><span class="hidden-xs"> Crear nuevo municipio</span></a></p> 
		<div class="table-responsive">
		<table class="table table-bordered table-condensed table-hover" id="sort"> 
			<thead> 
				<tr class="text-center"> 
					<th><strong>#</strong></th>
					<th><strong>Departamento</strong></th>
					<th><strong>Codigo INE</strong></th>
					<th><strong>Nombre del Municipio</strong></th>
					<th><strong>Latitud</strong></th>
					<th><strong>Longitud</strong></th>
					<th class="text-center">Opciones</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php
				$count=0;
				while($row = $values->fetch_object()){ 
					$count++;
				?>
				<tr> 
					<td><?= $count; ?></td>
					<td><?= $row->departamento; ?></td>
					<td><?= htmlspecialchars_decode($row->codine, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->municipio, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->latitud, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->longitud, ENT_QUOTES); ?></td>
					<td class="text-center">
						<a href="?m=municipio&f=editar&id_municipio=<?= $row->id_municipio; ?>" title="Modificar" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span> Modificar</a>
						<a href="javascript:eliminar(<?= $row->id_municipio; ?>)" title="Eliminar" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Eliminar</a>
					</td>
				</tr> 
				<?php } ?> 
			</tbody> 
		</table> 
		</div> 
	</div> 
