<?php
// Incluyendo archivo de la base de Datos
require_once(MODULES.'municipio/db.municipio'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
// Inicializando la clase de la base de Datos
$new = new municipio();
// Recibiendo y limpiando Datos
$id_municipio = $_POST['id_municipio'];
$id_dpto = $_POST['id_dpto'];
$codine = htmlspecialchars($_POST['codine'], ENT_QUOTES);
$municipio = htmlspecialchars($_POST['municipio'], ENT_QUOTES);
$latitud = htmlspecialchars($_POST['latitud'], ENT_QUOTES);
$longitud = htmlspecialchars($_POST['longitud'], ENT_QUOTES);
$update = array("id_dpto" => "$id_dpto", "codine" => "$codine", "municipio" => "$municipio", "latitud" => "$latitud", "longitud" => "$longitud");
$where = array("id_municipio" => "$id_municipio");
$new->_make_update_municipio($update, $where);
header("Location: in.php?m=municipio&f=lista");
exit;

?>
