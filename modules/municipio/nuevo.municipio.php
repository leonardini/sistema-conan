<?php
require(SYSTEM.'helpers/date.code_helper'.EXT);
require_once(MODULES.'municipio/db.municipio'.EXT);

$pro = new municipio();

$campos_dpto = array('departamento', 'id_dpto');
$value_dpto = $pro->_select_municipio($campos_dpto, NULL, NULL, NULL, "dpto");
if(!$value_dpto)echo $pro->error;

?>
	<script> 
	$(function(){ 
		$('#id_dpto').select(); 
		$.validate(); 
	}); 
	</script> 
	<br>
	<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Ingresando nueva informacion </strong></div> 
	<div class="panel-body"> 
		<form class="container" name="form1" method="post" id="formid" action="?m=municipio&f=save"> 
			<p> 
				<p> 
				<label for="id_dpto"> 
					<strong>Departamento:</strong> 
					<select name="id_dpto" id="id_dpto" class="form-control"> 
					<?php while($row_dpto = $value_dpto -> fetch_object()){ ?>
						<option value="<?= $row_dpto->id_dpto; ?>"><?= $row_dpto->departamento; ?></option> 
					<?php } ?>
					</select> 
				</label> 
				</p> 
				<p> 
				<label for="codine"> 
					<strong>Codigo INE:</strong> 
					<input type="text" name="codine" id="codine" data-validation="required" class="form-control" size="40" />
				</label> 
				</p> 
				<p> 
				<label for="municipio"> 
					<strong>Nombre del Municipio:</strong> 
					<input type="text" name="municipio" id="municipio" data-validation="required" class="form-control" size="40" />
				</label> 
				</p> 
				<p> 
				<label for="latitud"> 
					<strong>Latitud:</strong> 
					<input type="text" name="latitud" id="latitud" data-validation="required" class="form-control" size="40" />
				</label> 
				</p> 
				<p> 
				<label for="longitud"> 
					<strong>Longitud:</strong> 
					<input type="text" name="longitud" id="longitud" data-validation="required" class="form-control" size="40" />
				</label> 
				</p> 
			</p> 
			<p>
				<button type="submit" name="Enviar" class="btn btn-success" /><span class="glyphicon glyphicon-floppy-disk"></span> <span class="hidden-xs">Guardar Informacion</span></button> <a href="?m=municipio&f=lista" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span><span class="hidden-xs"> Cancelar</span></a>
			</p>
		</form> 
	</div> 
