<?php
require(SYSTEM.'helpers/date.code_helper'.EXT);
require_once(MODULES.'municipio/db.municipio'.EXT);

$pro = new municipio();

$campos_dpto = array('departamento', 'id_dpto');
$value_dpto = $pro->_select_municipio($campos_dpto, NULL, NULL, NULL, "dpto");
if(!$value_dpto)echo $pro->error;

// Recibiendo variable
$id_municipio = addslashes(trim($_GET['id_municipio']));

$news = new municipio();

// Estableciendo parametro recibido
$where = array("id_municipio" => "$id_municipio");
$values = $news->_select_municipio('*', $where);
if(!$values)echo $news->error;
$news->close();
?>
	<script> 
	$(function(){ 
		$('#id_dpto').select(); 
		$.validate(); 
	}); 
	</script> 
	<br>
	<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Modificando la informacion </strong></div> 
	<div class="panel-body"> 
		<?php while($row = $values->fetch_object()){ ?>
		<form class="container" name="form1" method="post" id="formid" action="?m=municipio&f=savechanges"> 
			<p> 
				<p> 
				<label for="id_dpto"> 
					<strong>Departamento:</strong> 
					<select name="id_dpto" id="id_dpto" class="form-control"> 
					<?php while($row_dpto = $value_dpto -> fetch_object()){ ?>
						<option value="<?= $row_dpto->id_dpto; ?>" <?php if($row->id_dpto==$row_dpto->id_dpto){ echo "selected"; } ?>><?= $row_dpto->departamento; ?></option> 
					<?php } ?>
					</select> 
				</label> 
				</p> 
				<p> 
				<label for="codine"> 
					<strong>Codigo INE:</strong> 
					<input type="text" name="codine" id="codine" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->codine, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="municipio"> 
					<strong>Nombre del Municipio:</strong> 
					<input type="text" name="municipio" id="municipio" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->municipio, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="latitud"> 
					<strong>Latitud:</strong> 
					<input type="text" name="latitud" id="latitud" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->latitud, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="longitud"> 
					<strong>Longitud:</strong> 
					<input type="text" name="longitud" id="longitud" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->longitud, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
			</p> 
			<p>
				<input type="hidden" name="id_municipio" id="id_municipio" value="<?= $id_municipio; ?>"/>
				<button type="submit" name="Enviar" class="btn btn-success" /><span class="glyphicon glyphicon-floppy-disk"></span> <span class="hidden-xs">Guardar Cambios</span></button> <a href="?m=municipio&f=lista" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span> <span class="hidden-xs">Cancelar</span></a>
			</p>
		</form> 
		<?php } ?>
	</div> 
