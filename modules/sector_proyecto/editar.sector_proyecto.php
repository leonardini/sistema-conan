<?php
require(SYSTEM.'helpers/date.code_helper'.EXT);
require_once(MODULES.'sector_proyecto/db.sector_proyecto'.EXT);
// Recibiendo variable
$id_sector_proyecto = addslashes(trim($_GET['id_sector_proyecto']));

$news = new sector_proyecto();

// Estableciendo parametro recibido
$where = array("id_sector_proyecto" => "$id_sector_proyecto");
$values = $news->_select_sector_proyecto('*', $where);
if(!$values)echo $news->error;

?>
<script> 
$(function(){ 
	$('#nombre_sector').select(); 
	$.validate(); 
}); 
</script> 
<br>
<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Modificando la informacion </strong></div> 
	<div class="panel-body"> 
		<?php while($row = $values->fetch_object()){ ?>
		<form class="container" name="form1" method="post" id="formid" action="?m=sector_proyecto&f=savechanges"> 
			<p> 
				<p> 
				<label for="nombre_sector"> 
					<strong>Sectores:</strong> 
					<input type="text" name="nombre_sector" id="nombre_sector" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->nombre_sector, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
			</p> 
			<p>
				<input type="hidden" name="id_sector_proyecto" id="id_sector_proyecto" value="<?= $id_sector_proyecto; ?>"/>
				<button type="submit" name="Enviar" class="btn btn-success" /><span class="glyphicon glyphicon-floppy-disk"></span> <span class="hidden-xs">Guardar Cambios</span></button> <a href="?m=sector_proyecto&f=lista" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span> <span class="hidden-xs">Cancelar</span></a>
			</p>
		</form> 
		<?php } ?>
	</div> 
