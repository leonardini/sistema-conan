<?php

require_once(MODULES.'sector_proyecto/db.sector_proyecto'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);

$new = new sector_proyecto();

$campos = array('*');
$limit = "0,150";
$where_u = "";

$values = $new->_select_sector_proyecto('*', $where_u);

?>
<script> 
$(function(){ 
	$('#sort').dataTable(); 
	$('[title]').tooltip(); 
}); 
function eliminar(id){
	bootbox.confirm("Está seguro que desea eliminar?",function(result){
		if(result){
			window.location="?m=sector_proyecto&f=eliminar&id_sector_proyecto="+id;
		}
	});
}
</script> 
<br>
<div class="panel panel-default">
	<div class="panel-heading"><strong> Sectores </strong></div> 
	<div class="panel-body"> 
	<p><a href="?m=sector_proyecto&f=nuevo" class='btn btn-success'><span class="glyphicon glyphicon-share"></span><span class="hidden-xs"> Crear nuevo sector</span></a></p> 
		<div class="table-responsive">
		<table class="table table-bordered table-condensed table-hover" id="sort"> 
			<thead> 
				<tr class="text-center"> 
					<th><strong>#</strong></th>
					<th><strong>Sectores</strong></th>
					<th class="text-center">Opciones</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php
				$count=0;
				while($row = $values->fetch_object()){ 
					$count++;
				?>
				<tr> 
					<td><?= $count; ?></td>
					<td><?= htmlspecialchars_decode($row->nombre_sector, ENT_QUOTES); ?></td>
					<td class="text-center">
						<a href="?m=sector_proyecto&f=editar&id_sector_proyecto=<?= $row->id_sector_proyecto; ?>" title="Modificar" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span> Modificar</a>
						<a href="javascript:eliminar(<?= $row->id_sector_proyecto; ?>)" title="Eliminar" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Eliminar</a>
					</td>
				</tr> 
				<?php } ?> 
			</tbody> 
		</table> 
		</div> 
	</div> 
