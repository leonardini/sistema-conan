<?php
// Incluyendo archivo de la base de Datos
require_once(MODULES.'sector_proyecto/db.sector_proyecto'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
// Inicializando la clase de la base de Datos
$new = new sector_proyecto();
// Recibiendo y limpiando Datos
$id_sector_proyecto = $_POST['id_sector_proyecto'];
$nombre_sector = htmlspecialchars($_POST['nombre_sector'], ENT_QUOTES);
$update = array("nombre_sector" => "$nombre_sector");
$where = array("id_sector_proyecto" => "$id_sector_proyecto");
$new->_make_update_sector_proyecto($update, $where);
header("Location: ?m=sector_proyecto&f=lista");
exit;

?>
