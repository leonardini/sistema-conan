<?php

/*
*   Vista regular de los campos de una base de datos
*   Powered by OHK
*/

require_once(MODULES.'sector_proyecto/db.sector_proyecto'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new sector_proyecto();

$id_sector_proyecto = addslashes(trim($_GET['id_sector_proyecto']));

$where_u = array("id_sector_proyecto" => "$id_sector_proyecto");

$values = $new->_select_sector_proyecto('*', $where_u);
if(!$values)echo $new->error;
$new->close();
?>

	<script> 
	function eliminar(id){
		bootbox.confirm("Está seguro que desea eliminar?",function(result){
			if(result){
				window.location="?m=sector_proyecto&f=eliminar&id_sector_proyecto="+id;
			}
		});
	}
	</script> 
	<div class="panel panel-primary"> 
	<div class="panel-heading"><strong> Vista de sector_proyecto </strong></div> 
	<div class="panel-body"> 
	<p>Opciones en sector_proyecto: </p> 
	<p> 
		<a href="?m=sector_proyecto&f=lista" class='btn btn-success'><span class="glyphicon glyphicon-list"></span><span class="hidden-xs"> Listado</span></a> 
		<a href="?m=sector_proyecto&f=nuevo" class='btn btn-primary'><span class="glyphicon glyphicon-plus"></span><span class="hidden-xs"> Nuevo(a)</span></a> 
		<a href="?m=sector_proyecto&f=editar&id_sector_proyecto=<?= $id_sector_proyecto; ?>" class='btn btn-warning'><span class="glyphicon glyphicon-pencil"></span><span class="hidden-xs"> Editar</span></a> 
		<a href="javascript:eliminar(<?= $id_sector_proyecto; ?>)" class='btn btn-danger'><span class="glyphicon glyphicon-remove"></span><span class="hidden-xs"> Eliminar</span></a> 
	</p> 
		<div class="panel panel-default"> 
			<div class="panel-heading"><strong>sector_proyecto</strong></div> 
			<div class="panel-body"> 
			<p>Vista única de sector_proyecto</p> 
			</div> 
			<div class="table-responsive"> 
			<?php while($row = $values->fetch_object()){ ?> 
			<table class="table table-condensed table-bordered table-hover"> 
				<tbody> 
					<tr>
						<th><strong>#</strong></th>
						<td><?= $row->id_sector_proyecto; ?></td>
					</tr>
					<tr>
						<th><strong>Sectores</strong></th>
						<td><?= htmlspecialchars_decode($row->nombre_sector, ENT_QUOTES); ?></td>
					</tr>
				</tbody> 
			</table> 
			<?php } ?> 
		</div>
	</div>
</div>
