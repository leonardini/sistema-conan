<?php 

session_start();
// Llamando a la base de datos
require(DBMYSQLI);

// Funcion que realiza en inicio de sesion
$usuario = addslashes(trim($_POST['usuario']));
$password = $_POST['password'];
$remember = $_POST['remember'];

if($remember=="on") $remember = TRUE;

// Variables de incremento de la seguridad
// tanto para las sesiones como para las
// cokies de acceso del usuario.
// Se pueden usar o no.

$us_ = "|ohk|u-s|{[\+-/.*|@]}";
$co_ = "|ohk|c-s|{[.+\-*/|@]}";

// Variable de almacena la ruta de la redireccion
// en caso de que las sessiones esten activas

// Correcto
$redireccion_correcto_ = "in.php?m=start&f=index";
// Incorrecto
$redireccion_incorrecto_ = "in.php?m=login&f=login&e=error";

// Variable booleana que habilita o no el requerimiento
// y uso de las variables adicionales de seguridad.

$secure_variable_ = TRUE;

// Ejecutando la funcion
$login_ = _do_login_($usuario, $password, $remember, $us_, $co_, $secure_variable_);

if($login_){
    
    header("Location: $redireccion_correcto_");
    exit;

}else{
    header("Location: $redireccion_incorrecto_");
    exit;
}

// Nombre de la session requerida: $_SESSION['usuario']
// Nombre de la session del nivel de usuario: $_SESSION['nivel']
// Nombre de la cooke $_COOKIE['identificado']

function _do_login_($usuario, $password, $remember=FALSE, $us, $co, $sv=TRUE){
    $db_ = new DB();
    if($sv)$password = sha1($us.md5($password));
    else $password = sha1(md5($password));
    $sql_ = "SELECT id_usuario, user, email, nivel FROM usuario 
    WHERE (user = '$usuario' AND passwd = '$password')
    OR (email = '$usuario' AND passwd = '$password')";
    $result_ = $db_->execute($sql_);
    $row_ = $result_->fetch_object();
    if(!$row_->id_usuario){
        //echo $db_->error();
        //$db_->Close();
        return FALSE;
    }else{
        //$row_ = $result_->fetch_object();
        if($row_->user==NULL){
            $_SESSION['usuario'] = $row_->email;
            $_SESSION['nivel'] = $row_->nivel;
            $_SESSION['idusuario'] = $row_->id_usuario;
            $id_usuario = $row_->id_usuario;
        }
        else{
            $_SESSION['usuario'] = $row_->user;
            $_SESSION['nivel'] = $row_->nivel;
            $_SESSION['idusuario'] = $row_->id_usuario;
            $id_usuario = $row_->id_usuario;
        }
        if($remember){
            if($sv) $cookie_ = sha1($co.md5($_SESSION['usuario'].date("Y-m-d H:i:s")));
            else $cookie_ = sha1(md5($_SESSION['usuario'].date("Y-m-d H:i:s")));
            $sqlco_ = "UPDATE usuario SET cookie = '$cookie_', 
            last_time_login = DATE_ADD(now(),INTERVAL 24 HOUR)
            WHERE id_usuario = '$id_usuario'";
            $db_->execute($sqlco_);
            setcookie('identificado',$cookie_,time()+86400,'/');
        }
        $db_->Close();
        return TRUE;
    }
}

function rrmdir($dir) {
    foreach(glob($dir . '/*') as $file) {
        if(is_dir($file)){
            rrmdir($file); // Retorna a la recursividad.
        }else{
            unlink($file); // Elimina el archivo.
        }
    }
    rmdir($dir);
}

?>