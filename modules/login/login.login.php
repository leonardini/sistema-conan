<?php session_start(); if(isset($_SESSION['usuario'])){ header("Location: in.php?m=start&f=index"); exit; }   ?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="stylesheet" type="text/css" href="default/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="default/css/bootstrap-theme.min.css">
	<script src="default/js/jquery-1.11.0.min.js"></script>
	<script src="default/js/bootstrap.min.js"></script>
	<script src="default/js/jquery.backstretch.min.js"></script>
	<script>
		$(function(){
			$.backstretch([
		      "default/images/abc1.jpg"
		      ], {
		        fade: 750,
		        duration: 3000
		    });
		});
	</script>
	<!--<script src="default/js/jquery-ui-1.10.3.min.js"></script>-->
	<link rel="SHORTCUT ICON" href="default/icon/bpuzzle.ico">
	<!--<link rel="SHORTCUT ICON" href="default/icon/32blitz.ico">-->
	<title>Inicio de Sesión</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-sm-4">
			</div>
			<div class="col-sm-5">
			</div>
			<div class="col-sm-3">
				<br><br><br><br>
				<form role="form" method="post" action="in.php?m=login&f=do">
					<div class="panel panel-primary">
						<div class="panel-heading"><h3 class="panel-title"><strong>Ingrese sus datos</strong></h3></div>
						<div class="panel-body">
							<div class="form-group">
								<label>Usuario o email:</label>
								<input type="text" name="usuario" class="form-control" placeholder="Usuario o email" required autofocus>
							</div>
							<div class="form-group">
								<label>Contraseña:</label>
								<input type="password" name="password" class="form-control" placeholder="Contraseña" required>
							</div>
							<div class="form-group">
								<label>
									<input type="checkbox" name="remember" value="remember-me" checked> Recordarme <small>(24 horas)</small>
								</label>
							</div>
							<button type="submit" class="btn btn-md btn-success btn-block">Acceder</button>
						</div>
					</div>
				</form>
				<?php
					if(isset($_GET['e'])){
					?>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<strong>Confirmar datos de acceso</strong>
					</div>
					<?php
					}
					else
					{

					}
				?>
			</div>
			
		</div>
	</div>
</body>
</html>