<?php

// Por norma
session_start();

// Llamando a la base de datos
require(DBMYSQLI);

// Salida correcta
$salida_ = "index.php";

$des_ = _cerrar_sesion_($_COOKIE['identificado']);

if($des_){
	header("Location: $salida_");
	exit;
}else{
	exit("Error al cerrar la sesion.");
}

function _cerrar_sesion_($cookie){
	$dc = _destroy_cookie_($cookie);
	if($dc){
		$_SESSION = array();
		$session_name = session_name();
		unset($_SESSION['usuario']);
		unset($_SESSION['nivel']);
		unset($_SESSION['idusuario']);
		session_destroy();
		if ( isset( $_COOKIE[ $session_name ] ) ) {
	        setcookie($session_name, '', time()-3600, '/');   
	    }
	    if(isset($_COOKIE['identificado'])){
	        setcookie('identificado', '', time()-3600, '/'); 
	    }
	    return TRUE;
	}else{
		return FALSE;
		exit("Error al eliminar la validez de la cookie en la base de datos.");
	}
}

function _destroy_cookie_($cookie){
	$user = $_SESSION['usuario'];
	$iduser = $_SESSION['idusuario'];
	$db_ = new DB();
	$sql_ = "UPDATE usuario 
	SET last_time_login = now(), 
	cookie = ''
	WHERE id_usuario = '$iduser'";
	if($db_->execute($sql_)){
		return TRUE;
	}else{
		return FALSE;
	}
}

?>