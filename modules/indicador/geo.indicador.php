
	<script type="text/javascript" src="default/js/leaflet.js"></script>
	<script type="text/javascript" src="default/js/leaflet.markercluster.js"></script>
	<script type="text/javascript" src="default/js/map.js"></script>
	<script type="text/javascript" src="default/js/bootstrap-multiselect.js"></script>
	<link rel="stylesheet" href="default/css/leaflet.css">
	<link rel="stylesheet" href="default/css/MarkerCluster.Default.css">
	<link rel="stylesheet" type="text/css" href="default/css/bootstrap-multiselect.css">
	<link rel="stylesheet" type="text/css" href="default/css/map.css">
	
	
<h4 class="page-header">Georeferenciación</h4>
<div class="row">
	<div class="col-md-12">
		<div class="opciones">
			<button id="btnPanel" type="button" class="btn btn-primary "><strong>Ver Opciones</strong></button>
			<!-- <button type="button" id="btnHelp" class="btn btn-default" ><strong>?</strong></button> -->
		</div>
		<div id="map">
		
		</div>
	</div>
</div>
	
	<form id="formMap">
		<div class="modal fade" id="myModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h5 class="modal-title">Seleccione los parametros de busqueda</h5>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<h5>Seleccione los Indicadores</h5>
								<select id="selectIndicadores" multiple="multiple">
								</select>
							</div>
							<div class="col-md-12">
								<h5>Seleccione los Departamentos</h5>
								<select id="selectDepartamentos" multiple="multiple">
								</select>
							</div>
							<div class="col-md-12">
								<h5>Seleccione los Municipios</h5>
								<select id="selectMunicipios" multiple="multiple">
								</select>
							</div>
							<!-- <div class="col-md-12">
								<h5>Seleccione las Comunidades</h5>
								<select id="selectComunidades" multiple="multiple">
								</select>
							</div> -->
							<div class="col-md-3 col-md-offset-3">
								<h5>Gestión Inicial</h5>
								<input type="text" name="fecha_inicial"  readonly="readonly" class ="cFecha form-control" id="idDesFe" />
							</div>
							<div class="col-md-3">
								<h5>Gestión Final</h5>
								<input type="text" name="fecha_final"  readonly="readonly" class ="cFecha form-control" id="idDesFe2" />
							</div>
						</div>
						<p>
						</p>
					</div>
					<div class="modal-footer">
						<button id="loadMapa" type="button" class="btn btn-primary">Cargar al Mapa</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					</div>
				</div>
			</div>
		</div>
	</form>
