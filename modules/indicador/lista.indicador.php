<?php

/*
*   Lista regular de los campos de una base de datos
*   Powered by OHK
*/

require_once(MODULES.'indicador/db.indicador'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new indicador();

$campos = array('*');
$limit = "0,150";
$where_u = "";

$values = $new->_select_indicador('*', $where_u);

?>
<script> 
$(function(){ 
	$('#sort').dataTable(); 
	$('[title]').tooltip(); 
}); 
function eliminar(id){
	bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
		if(result){
			window.location="?m=indicador&f=eliminar&id_indicador="+id;
		}
	});
}
</script> 
<br> 
<h4 class="page-header">Indicadores</h4>
<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Listado General </strong></div> 
	<div class="panel-body"> 
	<p><a href="?m=indicador&f=nuevo" class='btn btn-success'><span class="glyphicon glyphicon-share"></span><span class="hidden-xs"> Crear nuevo indicador</span></a></p> 
		<div class="table-responsive">
		<table class="table table-bordered table-condensed table-hover" id="sort"> 
			<thead> 
				<tr class="text-center"> 
					<th><strong>#</strong></th>
					<th><strong>Codigo</strong></th>
					<th><strong>Indicador</strong></th>
					<th class="text-center">Opciones</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php
				$count=0;
				while($row = $values->fetch_object()){ 
					$count++;
				?>
				<tr> 
					<td><?= $count; ?></td>
					<td><?= htmlspecialchars_decode($row->codigo_indicador, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->nombre_indicador, ENT_QUOTES); ?></td>
					<td class="text-center">
						<a href="?m=indicador&f=editar&id_indicador=<?= $row->id_indicador; ?>" title="Modificar" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span> Modificar</a>
						<a href="javascript:eliminar(<?= $row->id_indicador; ?>)" title="Eliminar" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Eliminar</a>
					</td>
				</tr> 
				<?php } ?> 
			</tbody> 
		</table> 
		</div> 
	</div> 
