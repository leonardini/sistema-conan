<?php
require(SYSTEM.'helpers/date.code_helper'.EXT);
?>
	<script> 
	$(function(){ 
		$('#codigo_indicador').select(); 
		$.validate(); 
	}); 
	</script> 
	<br>
	<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Ingresando nueva informacion </strong></div> 
	<div class="panel-body"> 
		<form class="container" name="form1" method="post" id="formid" action="?m=indicador&f=save"> 
			<p> 
				<p> 
				<label for="codigo_indicador"> 
					<strong>Codigo:</strong> 
					<input type="text" name="codigo_indicador" id="codigo_indicador" data-validation="required" class="form-control" size="40" />
				</label> 
				</p> 
				<p> 
				<label for="nombre_indicador"> 
					<strong>Indicador:</strong> 
					<textarea name="nombre_indicador" id="nombre_indicador" class="jqte-test" rows="5" cols="55"></textarea> 
				</label> 
				</p> 
				<script>
				$('.jqte-test').jqte();
				// settings of status
				var jqteStatus = true;
				$(".status").click(function()
				{
					jqteStatus = jqteStatus ? false : true;
					$('.jqte-test').jqte({"status" : jqteStatus})
				});
				</script>
			</p> 
			<p>
				<button type="submit" name="Enviar" class="btn btn-success" /><span class="glyphicon glyphicon-floppy-disk"></span> <span class="hidden-xs">Guardar Informacion</span></button> <a href="?m=indicador&f=lista" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span><span class="hidden-xs"> Cancelar</span></a>
			</p>
		</form> 
	</div> 
