<?php
require(SYSTEM.'helpers/date.code_helper'.EXT);
require_once(MODULES.'indicador/db.indicador'.EXT);
// Recibiendo variable
$id_indicador = addslashes(trim($_GET['id_indicador']));

$news = new indicador();

// Estableciendo parametro recibido
$where = array("id_indicador" => "$id_indicador");
$values = $news->_select_indicador('*', $where);
if(!$values)echo $news->error;
$news->close();
?>
	<script> 
	$(function(){ 
		$('#codigo_indicador').select(); 
		$.validate(); 
	}); 
	</script> 
	<br>
	<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Modificando la informacion </strong></div> 
	<div class="panel-body"> 
		<?php while($row = $values->fetch_object()){ ?>
		<form class="container" name="form1" method="post" id="formid" action="?m=indicador&f=savechanges"> 
			<p> 
				<p> 
				<label for="codigo_indicador"> 
					<strong>Codigo:</strong> 
					<input type="text" name="codigo_indicador" id="codigo_indicador" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->codigo_indicador, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="nombre_indicador"> 
					<strong>Indicador:</strong> 
					<textarea name="nombre_indicador" id="nombre_indicador" class="jqte-test" rows="5" cols="55"><?= htmlspecialchars_decode($row->nombre_indicador, ENT_QUOTES); ?></textarea> 
				</label> 
				</p> 
				<script>
				$('.jqte-test').jqte();
				// settings of status
				var jqteStatus = true;
				$(".status").click(function()
				{
					jqteStatus = jqteStatus ? false : true;
					$('.jqte-test').jqte({"status" : jqteStatus})
				});
				</script>
			</p> 
			<p>
				<input type="hidden" name="id_indicador" id="id_indicador" value="<?= $id_indicador; ?>"/>
				<button type="submit" name="Enviar" class="btn btn-success" /><span class="glyphicon glyphicon-floppy-disk"></span> <span class="hidden-xs">Guardar Cambios</span></button> <a href="?m=indicador&f=lista" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span> <span class="hidden-xs">Cancelar</span></a>
			</p>
		</form> 
		<?php } ?>
	</div> 
