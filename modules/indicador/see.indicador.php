<?php

/*
*   Vista regular de los campos de una base de datos
*   Powered by OHK
*/

require_once(MODULES.'indicador/db.indicador'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new indicador();

$id_indicador = addslashes(trim($_GET['id_indicador']));

$where_u = array("id_indicador" => "$id_indicador");

$values = $new->_select_indicador('*', $where_u);
if(!$values)echo $new->error;
$new->close();
?>

	<script> 
	function eliminar(id){
		bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
			if(result){
				window.location="?m=indicador&f=eliminar&id_indicador="+id;
			}
		});
	}
	</script> 
	<div class="panel panel-primary"> 
	<div class="panel-heading"><strong> Vista de indicador </strong></div> 
	<div class="panel-body"> 
	<p>Opciones en indicador: </p> 
	<p> 
		<a href="?m=indicador&f=lista" class='btn btn-success'><span class="glyphicon glyphicon-list"></span><span class="hidden-xs"> Listado</span></a> 
		<a href="?m=indicador&f=nuevo" class='btn btn-primary'><span class="glyphicon glyphicon-plus"></span><span class="hidden-xs"> Nuevo(a)</span></a> 
		<a href="?m=indicador&f=editar&id_indicador=<?= $id_indicador; ?>" class='btn btn-warning'><span class="glyphicon glyphicon-pencil"></span><span class="hidden-xs"> Modificar</span></a> 
		<a href="javascript:eliminar(<?= $id_indicador; ?>)" class='btn btn-danger'><span class="glyphicon glyphicon-remove"></span><span class="hidden-xs"> Eliminar</span></a> 
	</p> 
		<div class="panel panel-default"> 
			<div class="panel-heading"><strong>indicador</strong></div> 
			<div class="panel-body"> 
			<p>Vista única de indicador</p> 
			</div> 
			<div class="table-responsive"> 
			<?php while($row = $values->fetch_object()){ ?> 
			<table class="table table-condensed table-bordered table-hover"> 
				<tbody> 
					<tr>
						<th><strong>#</strong></th>
						<td><?= $row->id_indicador; ?></td>
					</tr>
					<tr>
						<th><strong>Codigo</strong></th>
						<td><?= htmlspecialchars_decode($row->codigo_indicador, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Indicador</strong></th>
						<td><?= htmlspecialchars_decode($row->nombre_indicador, ENT_QUOTES); ?></td>
					</tr>
				</tbody> 
			</table> 
			<?php } ?> 
		</div>
	</div>
</div>
