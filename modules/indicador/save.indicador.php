<?php
// Incluyendo archivo de la base de Datos
require_once(MODULES.'indicador/db.indicador'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
// Inicializando la clase de la base de Datos
$new = new indicador();
// Recibiendo y limpiando Datos
$id_indicador = $_POST['id_indicador'];
$codigo_indicador = htmlspecialchars($_POST['codigo_indicador'], ENT_QUOTES);
$nombre_indicador = htmlspecialchars($_POST['nombre_indicador'], ENT_QUOTES);
$insert = array("codigo_indicador" => "$codigo_indicador", "nombre_indicador" => "$nombre_indicador");
$last_insert = $new->_make_insert_indicador($insert);
header("Location: in.php?m=indicador&f=lista");
exit;

?>
