<?php
// Incluyendo archivo de la base de Datos
require_once(MODULES.'iconos/db.iconos'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
// Inicializando la clase de la base de Datos
$new = new iconos();
// Recibiendo y limpiando Datos
$id_iconos = $_POST['id_iconos'];
$clasificador = htmlspecialchars($_POST['clasificador'], ENT_QUOTES);
$url_icono = htmlspecialchars($_POST['url_icono'], ENT_QUOTES);
$insert = array("clasificador" => "$clasificador", "url_icono" => "$url_icono");
$last_insert = $new->_make_insert_iconos($insert);
header("Location: in.php?m=iconos&f=lista");
exit;

?>
