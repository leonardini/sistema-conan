<?php

require_once(MODULES.'iconos/db.iconos'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new iconos();

$campos = array('*');
$limit = "0,150";
$where_u = "";

$values = $new->_select_iconos('*', $where_u);

?>
<script> 
$(function(){ 
	$('#sort').dataTable(); 
	$('[title]').tooltip(); 
}); 
function eliminar(id){
	bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
		if(result){
			window.location="?m=iconos&f=eliminar&id_iconos="+id;
		}
	});
}
</script> 
<br> 
<h4 class="page-header">Iconos</h4>
<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Listado General </strong></div> 
	<div class="panel-body"> 
	<p><a href="?m=iconos&f=nuevo" class='btn btn-success'><span class="glyphicon glyphicon-share"></span><span class="hidden-xs"> Adicionar nuevo ícono</span></a></p> 
		<div class="table-responsive">
		<table class="table table-bordered table-condensed table-hover" id="sort"> 
			<thead> 
				<tr class="text-center"> 
					<th><strong>#</strong></th>
					<th><strong>Clasificador</strong></th>
					<th><strong>Url icono</strong></th>
					<th class="text-center">Opciones</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php
				$count=0;
				while($row = $values->fetch_object()){ 
					$count++;
				?>
				<tr> 
					<td><?= $count; ?></td>
					<td><?= htmlspecialchars_decode($row->clasificador, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->url_icono, ENT_QUOTES); ?></td>
					<td class="text-center">
						<a href="?m=iconos&f=editar&id_iconos=<?= $row->id_iconos; ?>" title="Modificar" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span> Modificar</a>
						<a href="javascript:eliminar(<?= $row->id_iconos; ?>)" title="Eliminar" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Eliminar</a>
					</td>
				</tr> 
				<?php } ?> 
			</tbody> 
		</table> 
		</div> 
	</div> 
