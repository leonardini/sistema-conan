<?php
require(SYSTEM.'helpers/date.code_helper'.EXT);
require_once(MODULES.'iconos/db.iconos'.EXT);
// Recibiendo variable
$id_iconos = addslashes(trim($_GET['id_iconos']));

$news = new iconos();

// Estableciendo parametro recibido
$where = array("id_iconos" => "$id_iconos");
$values = $news->_select_iconos('*', $where);
if(!$values)echo $news->error;
$news->close();
?>
	<script> 
	$(function(){ 
		$('#clasificador').select(); 
		$.validate(); 
	}); 
	</script> 
	<br>
	<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Modificando la informacion </strong></div> 
	<div class="panel-body"> 
		<?php while($row = $values->fetch_object()){ ?>
		<form class="container" name="form1" method="post" id="formid" action="?m=iconos&f=savechanges"> 
			<p> 
				<p> 
				<label for="clasificador"> 
					<strong>Clasificador:</strong> 
					<input type="text" name="clasificador" id="clasificador" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->clasificador, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="url_icono"> 
					<strong>Url icono:</strong> 
					<input type="text" name="url_icono" id="url_icono" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->url_icono, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
			</p> 
			<p>
				<input type="hidden" name="id_iconos" id="id_iconos" value="<?= $id_iconos; ?>"/>
				<button type="submit" name="Enviar" class="btn btn-success" /><span class="glyphicon glyphicon-floppy-disk"></span> <span class="hidden-xs">Guardar Cambios</span></button> <a href="?m=iconos&f=lista" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span> <span class="hidden-xs">Cancelar</span></a>
			</p>
		</form> 
		<?php } ?>
	</div> 
