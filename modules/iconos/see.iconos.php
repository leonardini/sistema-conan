<?php

/*
*   Vista regular de los campos de una base de datos
*   Powered by OHK
*/

require_once(MODULES.'iconos/db.iconos'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new iconos();

$id_iconos = addslashes(trim($_GET['id_iconos']));

$where_u = array("id_iconos" => "$id_iconos");

$values = $new->_select_iconos('*', $where_u);
if(!$values)echo $new->error;
$new->close();
?>

	<script> 
	function eliminar(id){
		bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
			if(result){
				window.location="?m=iconos&f=eliminar&id_iconos="+id;
			}
		});
	}
	</script> 
	<div class="panel panel-primary"> 
	<div class="panel-heading"><strong> Vista de iconos </strong></div> 
	<div class="panel-body"> 
	<p>Opciones en iconos: </p> 
	<p> 
		<a href="?m=iconos&f=lista" class='btn btn-success'><span class="glyphicon glyphicon-list"></span><span class="hidden-xs"> Listado</span></a> 
		<a href="?m=iconos&f=nuevo" class='btn btn-primary'><span class="glyphicon glyphicon-plus"></span><span class="hidden-xs"> Nuevo(a)</span></a> 
		<a href="?m=iconos&f=editar&id_iconos=<?= $id_iconos; ?>" class='btn btn-warning'><span class="glyphicon glyphicon-pencil"></span><span class="hidden-xs"> Modificar</span></a> 
		<a href="javascript:eliminar(<?= $id_iconos; ?>)" class='btn btn-danger'><span class="glyphicon glyphicon-remove"></span><span class="hidden-xs"> Eliminar</span></a> 
	</p> 
		<div class="panel panel-default"> 
			<div class="panel-heading"><strong>iconos</strong></div> 
			<div class="panel-body"> 
			<p>Vista única de iconos</p> 
			</div> 
			<div class="table-responsive"> 
			<?php while($row = $values->fetch_object()){ ?> 
			<table class="table table-condensed table-bordered table-hover"> 
				<tbody> 
					<tr>
						<th><strong>#</strong></th>
						<td><?= $row->id_iconos; ?></td>
					</tr>
					<tr>
						<th><strong>Clasificador</strong></th>
						<td><?= htmlspecialchars_decode($row->clasificador, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Url icono</strong></th>
						<td><?= htmlspecialchars_decode($row->url_icono, ENT_QUOTES); ?></td>
					</tr>
				</tbody> 
			</table> 
			<?php } ?> 
		</div>
	</div>
</div>
