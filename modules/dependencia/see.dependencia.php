<?php

/*
*   Vista regular de los campos de una base de datos
*   Powered by OHK
*/

require_once(MODULES.'dependencia/db.dependencia'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new dependencia();

$id_dependencia = addslashes(trim($_GET['id_dependencia']));

$where_u = array("id_dependencia" => "$id_dependencia");

$values = $new->_select_dependencia('*', $where_u);
if(!$values)echo $new->error;
$new->close();
?>

	<script> 
	function eliminar(id){
		bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
			if(result){
				window.location="?m=dependencia&f=eliminar&id_dependencia="+id;
			}
		});
	}
	</script> 
	<div class="panel panel-primary"> 
	<div class="panel-heading"><strong> Vista de dependencia </strong></div> 
	<div class="panel-body"> 
	<p>Opciones en dependencia: </p> 
	<p> 
		<a href="?m=dependencia&f=lista" class='btn btn-success'><span class="glyphicon glyphicon-list"></span><span class="hidden-xs"> Listado</span></a> 
		<a href="?m=dependencia&f=nuevo" class='btn btn-primary'><span class="glyphicon glyphicon-plus"></span><span class="hidden-xs"> Nuevo(a)</span></a> 
		<a href="?m=dependencia&f=editar&id_dependencia=<?= $id_dependencia; ?>" class='btn btn-warning'><span class="glyphicon glyphicon-pencil"></span><span class="hidden-xs"> Modificar</span></a> 
		<a href="javascript:eliminar(<?= $id_dependencia; ?>)" class='btn btn-danger'><span class="glyphicon glyphicon-remove"></span><span class="hidden-xs"> Eliminar</span></a> 
	</p> 
		<div class="panel panel-default"> 
			<div class="panel-heading"><strong>dependencia</strong></div> 
			<div class="panel-body"> 
			<p>Vista única de dependencia</p> 
			</div> 
			<div class="table-responsive"> 
			<?php while($row = $values->fetch_object()){ ?> 
			<table class="table table-condensed table-bordered table-hover"> 
				<tbody> 
					<tr>
						<th><strong>#</strong></th>
						<td><?= $row->id_dependencia; ?></td>
					</tr>
					<tr>
						<th><strong>Dependencia</strong></th>
						<td><?= htmlspecialchars_decode($row->nombre_dependencia, ENT_QUOTES); ?></td>
					</tr>
				</tbody> 
			</table> 
			<?php } ?> 
		</div>
	</div>
</div>
