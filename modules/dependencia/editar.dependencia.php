<?php
require(SYSTEM.'helpers/date.code_helper'.EXT);
require_once(MODULES.'dependencia/db.dependencia'.EXT);
// Recibiendo variable
$id_dependencia = addslashes(trim($_GET['id_dependencia']));

$news = new dependencia();

// Estableciendo parametro recibido
$where = array("id_dependencia" => "$id_dependencia");
$values = $news->_select_dependencia('*', $where);
if(!$values)echo $news->error;
?>
<script> 
$(function(){ 
	$('#nombre_dependencia').select(); 
	$.validate(); 
}); 
</script> 
<br>
<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Modificando la informacion </strong></div> 
	<div class="panel-body"> 
		<?php while($row = $values->fetch_object()){ ?>
		<form class="container" name="form1" method="post" id="formid" action="?m=dependencia&f=savechanges"> 
			<p> 
				<p> 
				<label for="nombre_dependencia"> 
					<strong>Dependencia:</strong> 
					<input type="text" name="nombre_dependencia" id="nombre_dependencia" data-validation="required" class="form-control" size="40" value="<?= htmlspecialchars_decode($row->nombre_dependencia, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
			</p> 
			<p>
				<input type="hidden" name="id_dependencia" id="id_dependencia" value="<?= $id_dependencia; ?>"/>
				<button type="submit" name="Enviar" class="btn btn-success" /><span class="glyphicon glyphicon-floppy-disk"></span> <span class="hidden-xs">Guardar Cambios</span></button> <a href="?m=dependencia&f=lista" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span> <span class="hidden-xs">Cancelar</span></a>
			</p>
		</form> 
		<?php } ?>
	</div> 
