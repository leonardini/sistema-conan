<?php
// Incluyendo archivo de la base de Datos
require_once(MODULES.'dependencia/db.dependencia'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
// Inicializando la clase de la base de Datos
$new = new dependencia();
// Recibiendo y limpiando Datos
$id_dependencia = $_POST['id_dependencia'];
$nombre_dependencia = htmlspecialchars($_POST['nombre_dependencia'], ENT_QUOTES);
$insert = array("nombre_dependencia" => "$nombre_dependencia");
$last_insert = $new->_make_insert_dependencia($insert);
header("Location: in.php?m=dependencia&f=lista");
exit;

?>
