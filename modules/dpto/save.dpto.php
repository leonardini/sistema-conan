<?php
// Incluyendo archivo de la base de Datos
require_once(MODULES.'dpto/db.dpto'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
// Inicializando la clase de la base de Datos
$new = new dpto();
// Recibiendo y limpiando Datos
$id_dpto = $_POST['id_dpto'];
$departamento = htmlspecialchars($_POST['departamento'], ENT_QUOTES);
$latitud = htmlspecialchars($_POST['latitud'], ENT_QUOTES);
$longitud = htmlspecialchars($_POST['longitud'], ENT_QUOTES);
$poblacion = htmlspecialchars($_POST['poblacion'], ENT_QUOTES);
$municipios = htmlspecialchars($_POST['municipios'], ENT_QUOTES);
$insert = array("departamento" => "$departamento", "latitud" => "$latitud", "longitud" => "$longitud", "poblacion" => "$poblacion", "municipios" => "$municipios");
$last_insert = $new->_make_insert_dpto($insert);
header("Location: in.php?m=dpto&f=lista");
exit;

?>
