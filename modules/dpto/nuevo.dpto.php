<?php
require(SYSTEM.'helpers/date.code_helper'.EXT);
?>
	<script> 
	$(function(){ 
		$('#departamento').select(); 
		$.validate(); 
	}); 
	</script> 
<br>
<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Creando un nuevo Departamento </strong></div> 
	<div class="panel-body"> 
		<form class="container" name="form1" method="post" id="formid" action="?m=dpto&f=save"> 
			<p> 
				<p> 
				<label for="departamento"> 
					<strong>Departamento:</strong> 
					<input type="text" name="departamento" id="departamento" data-validation="required" class="form-control" size="40" />
				</label> 
				</p> 
				<p> 
				<label for="latitud"> 
					<strong>Latitud:</strong> 
					<input type="text" name="latitud" id="latitud" data-validation="required" class="form-control" size="40" />
				</label> 
				</p> 
				<p> 
				<label for="longitud"> 
					<strong>Longitud:</strong> 
					<input type="text" name="longitud" id="longitud" data-validation="required" class="form-control" size="40" />
				</label> 
				</p> 
				<p> 
				<label for="poblacion"> 
					<strong>Población del Departamento:</strong> 
					<input type="text" name="poblacion" id="poblacion" data-validation="required" class="form-control" size="40" />
				</label> 
				</p> 
				<p> 
				<label for="municipios"> 
					<strong>Cantidad de municipios:</strong> 
					<input type="text" name="municipios" id="municipios" data-validation="required" class="form-control" size="40" />
				</label> 
				</p> 
			</p> 
			<p>
				<button type="submit" name="Enviar" class="btn btn-success" /><span class="glyphicon glyphicon-floppy-disk"></span> <span class="hidden-xs">Guardar Informacion</span></button> <a href="?m=dpto&f=lista" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span><span class="hidden-xs"> Cancelar</span></a>
			</p>
		</form> 
	</div> 
