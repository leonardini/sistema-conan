<?php

require_once(MODULES.'dpto/db.dpto'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);

$new = new dpto();

$campos = array('*');
$limit = "0,150";
$where_u = "";

$values = $new->_select_dpto('*', $where_u);

?>
<script> 
$(function(){ 
	$('#sort').dataTable(); 
	$('[title]').tooltip(); 
}); 
function eliminar(id){
	bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
		if(result){
			window.location="?m=dpto&f=eliminar&id_dpto="+id;
		}
	});
}
</script> 
<br> 
<h4 class="page-header">Departamentos</h4>
<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Listado General de los Departamentos </strong></div> 
	<div class="panel-body"> 
	<p><a href="?m=dpto&f=nuevo" class='btn btn-success'><span class="glyphicon glyphicon-share"></span><span class="hidden-xs"> Crear nuevo Departamento</span></a></p> 
		<div class="table-responsive">
		<table class="table table-bordered table-condensed table-hover" id="sort"> 
			<thead> 
				<tr class="text-center"> 
					<th><strong>#</strong></th>
					<th><strong>Departamento</strong></th>
					<th><strong>Latitud</strong></th>
					<th><strong>Longitud</strong></th>
					<th><strong>Población del Departamento</strong></th>
					<th><strong>Cantidad de municipios</strong></th>
					<th class="text-center">Opciones</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php
				$count=0;
				while($row = $values->fetch_object()){ 
					$count++;
				?>
				<tr> 
					<td><?= $count; ?></td>
					<td><?= htmlspecialchars_decode($row->departamento, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->latitud, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->longitud, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode(number_format($row->poblacion,2,".",","), ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->municipios, ENT_QUOTES); ?></td>
					<td class="text-center">
						<a href="?m=dpto&f=editar&id_dpto=<?= $row->id_dpto; ?>" title="Modificar" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span> Modificar</a>
						<a href="javascript:eliminar(<?= $row->id_dpto; ?>)" title="Eliminar" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Eliminar</a>
					</td>
				</tr> 
				<?php } ?> 
			</tbody> 
		</table> 
		</div> 
	</div> 
