<?php
require(SYSTEM.'helpers/date.code_helper'.EXT);
require_once(MODULES.'dpto/db.dpto'.EXT);
// Recibiendo variable
$id_dpto = addslashes(trim($_GET['id_dpto']));

$news = new dpto();

// Estableciendo parametro recibido
$where = array("id_dpto" => "$id_dpto");
$values = $news->_select_dpto('*', $where);
if(!$values)echo $news->error;

?>
<script> 
$(function(){ 
	$('#departamento').select(); 
	$.validate(); 
}); 
</script> 
<br>
<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Modificando la informacion del Departamento </strong></div> 
	<div class="panel-body"> 
		<?php while($row = $values->fetch_object()){ ?>
		<form class="container" name="form1" method="post" id="formid" action="?m=dpto&f=savechanges"> 
			<p> 
				<p> 
				<label for="departamento"> 
					<strong>Departamento:</strong> 
					<input type="text" name="departamento" id="departamento" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->departamento, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="latitud"> 
					<strong>Latitud:</strong> 
					<input type="text" name="latitud" id="latitud" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->latitud, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="longitud"> 
					<strong>Longitud:</strong> 
					<input type="text" name="longitud" id="longitud" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->longitud, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="poblacion"> 
					<strong>Población del Departamento:</strong> 
					<input type="text" name="poblacion" id="poblacion" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->poblacion, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="municipios"> 
					<strong>Cantidad de municipios:</strong> 
					<input type="text" name="municipios" id="municipios" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->municipios, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
			</p> 
			<p>
				<input type="hidden" name="id_dpto" id="id_dpto" value="<?= $id_dpto; ?>"/>
				<button type="submit" name="Enviar" class="btn btn-success" /><span class="glyphicon glyphicon-floppy-disk"></span> <span class="hidden-xs">Guardar Cambios</span></button> <a href="?m=dpto&f=lista" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span> <span class="hidden-xs">Cancelar</span></a>
			</p>
		</form> 
		<?php } ?>
	</div> 
