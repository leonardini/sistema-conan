<?php

/*
*   Vista regular de los campos de una base de datos
*   Powered by OHK
*/

require_once(MODULES.'dpto/db.dpto'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new dpto();

$id_dpto = addslashes(trim($_GET['id_dpto']));

$where_u = array("id_dpto" => "$id_dpto");

$values = $new->_select_dpto('*', $where_u);
if(!$values)echo $new->error;
$new->close();
?>

	<script> 
	function eliminar(id){
		bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
			if(result){
				window.location="?m=dpto&f=eliminar&id_dpto="+id;
			}
		});
	}
	</script> 
	<div class="panel panel-primary"> 
	<div class="panel-heading"><strong> Vista de dpto </strong></div> 
	<div class="panel-body"> 
	<p>Opciones en dpto: </p> 
	<p> 
		<a href="?m=dpto&f=lista" class='btn btn-success'><span class="glyphicon glyphicon-list"></span><span class="hidden-xs"> Listado</span></a> 
		<a href="?m=dpto&f=nuevo" class='btn btn-primary'><span class="glyphicon glyphicon-plus"></span><span class="hidden-xs"> Nuevo(a)</span></a> 
		<a href="?m=dpto&f=editar&id_dpto=<?= $id_dpto; ?>" class='btn btn-warning'><span class="glyphicon glyphicon-pencil"></span><span class="hidden-xs"> Modificar</span></a> 
		<a href="javascript:eliminar(<?= $id_dpto; ?>)" class='btn btn-danger'><span class="glyphicon glyphicon-remove"></span><span class="hidden-xs"> Eliminar</span></a> 
	</p> 
		<div class="panel panel-default"> 
			<div class="panel-heading"><strong>dpto</strong></div> 
			<div class="panel-body"> 
			<p>Vista única de dpto</p> 
			</div> 
			<div class="table-responsive"> 
			<?php while($row = $values->fetch_object()){ ?> 
			<table class="table table-condensed table-bordered table-hover"> 
				<tbody> 
					<tr>
						<th><strong>#</strong></th>
						<td><?= $row->id_dpto; ?></td>
					</tr>
					<tr>
						<th><strong>Departamento</strong></th>
						<td><?= htmlspecialchars_decode($row->departamento, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Latitud</strong></th>
						<td><?= htmlspecialchars_decode($row->latitud, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Longitud</strong></th>
						<td><?= htmlspecialchars_decode($row->longitud, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Población del Departamento</strong></th>
						<td><?= htmlspecialchars_decode($row->poblacion, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Cantidad de municipios</strong></th>
						<td><?= htmlspecialchars_decode($row->municipios, ENT_QUOTES); ?></td>
					</tr>
				</tbody> 
			</table> 
			<?php } ?> 
		</div>
	</div>
</div>
