<?php

require_once(MODULES.'proyecto/db.proyecto'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new proyecto();

$ff = array("proyecto.id_proyecto", "proyecto.nombre_proyecto", "proyecto.componentes", "proyecto.objetivos", "proyecto.resultados", "proyecto.indicador_impacto", "proyecto.gestion_inicial", "proyecto.gestion_final", "proyecto.presupuesto_total", "proyecto.contraparte_municipal", "proyecto.cobertura_urbana_hombres", "proyecto.cobertura_urbana_mujeres", "proyecto.cobertura_rural_hombres", "proyecto.cobertura_rural_mujeres", "dependencia.nombre_dependencia", "sector_proyecto.nombre_sector", "tipo_proyecto.nombre_tipo");
$tt = "proyecto";
$jt = array("dependencia", "sector_proyecto", "tipo_proyecto");
$on = array(
"dependencia.id_dependencia" => "proyecto.id_dependencia" , 
"sector_proyecto.id_sector_proyecto" => "proyecto.id_sector_proyecto" , 
"tipo_proyecto.id_tipo_proyecto" => "proyecto.id_tipo_proyecto" 
 );

$where_u = "";

$values = $new->_call_multiple_left_join($ff, $jt, $on, $where_u);

?>
<script> 
$(function(){ 
	$('#sort').dataTable(); 
	$('[title]').tooltip(); 
}); 
function eliminar(id){
	bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
		if(result){
			window.location="?m=proyecto&f=eliminar&id_proyecto="+id;
		}
	});
}
</script> 
<br>
<h4 class="page-header">Programas/Proyectos</h4>
	<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Listado General de los Programas/Proyectos </strong></div> 
	<div class="panel-body"> 
	<p><a href="?m=proyecto&f=nuevo" class='btn btn-success'><span class="glyphicon glyphicon-share"></span><span class="hidden-xs"> Crear nuevo proyecto</span></a></p> 
		<div class="table-responsive">
		<table class="table table-bordered table-condensed table-hover" id="sort"> 
			<thead> 
				<tr class="text-center"> 
					<th><strong>#</strong></th>
					<th><strong>Ministerio</strong></th>
					<th><strong>Sector</strong></th>
					<th><strong>Nombre Programa/Proyecto</strong></th>
					<th><strong>Componentes</strong></th>
					<th><strong>Objetivos</strong></th>
					<th><strong>Resultados</strong></th>
					<th><strong>Indicador de Impacto</strong></th>
					<th><strong>Tipo</strong></th>
					<th><strong>Gestion inicial</strong></th>
					<th><strong>Gestion final</strong></th>
					<th><strong>Presupuesto total</strong></th>
					<th><strong>Contraparte municipal</strong></th>
					<th><strong>Cobertura poblacional urbana hombres</strong></th>
					<th><strong>Cobertura poblacional urbana mujeres</strong></th>
					<th><strong>Cobertura poblacional rural hombres</strong></th>
					<th><strong>Cobertura poblacional rural mujeres</strong></th>
					<th class="text-center">Opciones</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php
				$count=0;
				while($row = $values->fetch_object()){ 
					$count++;
				?>
				<tr> 
					<td><?= $count; ?></td>
					<td><?= $row->nombre_dependencia; ?></td>
					<td><?= $row->nombre_sector; ?></td>
					<td><?= htmlspecialchars_decode($row->nombre_proyecto, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->componentes, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->objetivos, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->resultados, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->indicador_impacto, ENT_QUOTES); ?></td>
					<td><?= $row->nombre_tipo; ?></td>
					<td><?= htmlspecialchars_decode($row->gestion_inicial, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->gestion_final, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->presupuesto_total, ENT_QUOTES); ?></td>
					<td><?= $row->contraparte_municipal; ?></td>
					<td><?= htmlspecialchars_decode($row->cobertura_urbana_hombres, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->cobertura_urbana_mujeres, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->cobertura_rural_hombres, ENT_QUOTES); ?></td>
					<td><?= htmlspecialchars_decode($row->cobertura_rural_mujeres, ENT_QUOTES); ?></td>
					<td class="text-center">
						<a href="?m=proyecto&f=editar&id_proyecto=<?= $row->id_proyecto; ?>" title="Modificar" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span> Modificar</a>
						<a href="javascript:eliminar(<?= $row->id_proyecto; ?>)" title="Eliminar" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Eliminar</a>
					</td>
				</tr> 
				<?php } ?> 
			</tbody> 
		</table> 
		</div> 
	</div> 
