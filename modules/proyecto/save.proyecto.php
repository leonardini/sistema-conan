<?php
// Incluyendo archivo de la base de Datos
require_once(MODULES.'proyecto/db.proyecto'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
// Inicializando la clase de la base de Datos
$new = new proyecto();
// Recibiendo y limpiando Datos
$id_proyecto = $_POST['id_proyecto'];
$id_dependencia = $_POST['id_dependencia'];
$id_sector_proyecto = $_POST['id_sector_proyecto'];
$nombre_proyecto = htmlspecialchars($_POST['nombre_proyecto'], ENT_QUOTES);
$componentes = htmlspecialchars($_POST['componentes'], ENT_QUOTES);
$objetivos = htmlspecialchars($_POST['objetivos'], ENT_QUOTES);
$resultados = htmlspecialchars($_POST['resultados'], ENT_QUOTES);
$indicador_impacto = htmlspecialchars($_POST['indicador_impacto'], ENT_QUOTES);
$id_tipo_proyecto = $_POST['id_tipo_proyecto'];
$gestion_inicial = htmlspecialchars($_POST['gestion_inicial'], ENT_QUOTES);
$gestion_final = htmlspecialchars($_POST['gestion_final'], ENT_QUOTES);
$presupuesto_total = htmlspecialchars($_POST['presupuesto_total'], ENT_QUOTES);
$contraparte_municipal = $_POST['contraparte_municipal'];
$cobertura_urbana_hombres = htmlspecialchars($_POST['cobertura_urbana_hombres'], ENT_QUOTES);
$cobertura_urbana_mujeres = htmlspecialchars($_POST['cobertura_urbana_mujeres'], ENT_QUOTES);
$cobertura_rural_hombres = htmlspecialchars($_POST['cobertura_rural_hombres'], ENT_QUOTES);
$cobertura_rural_mujeres = htmlspecialchars($_POST['cobertura_rural_mujeres'], ENT_QUOTES);
$insert = array("id_dependencia" => "$id_dependencia", "id_sector_proyecto" => "$id_sector_proyecto", "nombre_proyecto" => "$nombre_proyecto", "componentes" => "$componentes", "objetivos" => "$objetivos", "resultados" => "$resultados", "indicador_impacto" => "$indicador_impacto", "id_tipo_proyecto" => "$id_tipo_proyecto", "gestion_inicial" => "$gestion_inicial", "gestion_final" => "$gestion_final", "presupuesto_total" => "$presupuesto_total", "contraparte_municipal" => "$contraparte_municipal", "cobertura_urbana_hombres" => "$cobertura_urbana_hombres", "cobertura_urbana_mujeres" => "$cobertura_urbana_mujeres", "cobertura_rural_hombres" => "$cobertura_rural_hombres", "cobertura_rural_mujeres" => "$cobertura_rural_mujeres");
$last_insert = $new->_make_insert_proyecto($insert);

header("Location: in.php?m=gep_proyecto&f=nuevo&id_proyecto=$last_insert");
exit;

?>
