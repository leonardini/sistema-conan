<?php

/*
*   Vista regular de los campos de una base de datos
*   Powered by OHK
*/

require_once(MODULES.'proyecto/db.proyecto'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new proyecto();

$id_proyecto = addslashes(trim($_GET['id_proyecto']));
$ff = array("proyecto.id_proyecto", "proyecto.nombre_proyecto", "proyecto.componentes", "proyecto.objetivos", "proyecto.resultados", "proyecto.indicador_impacto", "proyecto.gestion_inicial", "proyecto.gestion_final", "proyecto.presupuesto_total", "proyecto.contraparte_municipal", "proyecto.cobertura_urbana_hombres", "proyecto.cobertura_urbana_mujeres", "proyecto.cobertura_rural_hombres", "proyecto.cobertura_rural_mujeres", "dependencia.nombre_dependencia", "sector_proyecto.nombre_sector", "tipo_proyecto.nombre_tipo");
$tt = "proyecto";
$jt = array("dependencia", "sector_proyecto", "tipo_proyecto");
$on = array(
"dependencia.id_dependencia" => "proyecto.id_dependencia" , 
"sector_proyecto.id_sector_proyecto" => "proyecto.id_sector_proyecto" , 
"tipo_proyecto.id_tipo_proyecto" => "proyecto.id_tipo_proyecto" 
 );

$where_u = array("proyecto.id_proyecto" => "$id_proyecto");

$values = $new->_call_multiple_left_join($ff, $jt, $on, $where_u);
if(!$values)echo $new->error;
$new->close();
?>

	<script> 
	function eliminar(id){
		bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
			if(result){
				window.location="?m=proyecto&f=eliminar&id_proyecto="+id;
			}
		});
	}
	</script> 
	<div class="panel panel-primary"> 
	<div class="panel-heading"><strong> Vista de proyecto </strong></div> 
	<div class="panel-body"> 
	<p>Opciones en proyecto: </p> 
	<p> 
		<a href="?m=proyecto&f=lista" class='btn btn-success'><span class="glyphicon glyphicon-list"></span><span class="hidden-xs"> Listado</span></a> 
		<a href="?m=proyecto&f=nuevo" class='btn btn-primary'><span class="glyphicon glyphicon-plus"></span><span class="hidden-xs"> Nuevo(a)</span></a> 
		<a href="?m=proyecto&f=editar&id_proyecto=<?= $id_proyecto; ?>" class='btn btn-warning'><span class="glyphicon glyphicon-pencil"></span><span class="hidden-xs"> Modificar</span></a> 
		<a href="javascript:eliminar(<?= $id_proyecto; ?>)" class='btn btn-danger'><span class="glyphicon glyphicon-remove"></span><span class="hidden-xs"> Eliminar</span></a> 
	</p> 
		<div class="panel panel-default"> 
			<div class="panel-heading"><strong>proyecto</strong></div> 
			<div class="panel-body"> 
			<p>Vista única de proyecto</p> 
			</div> 
			<div class="table-responsive"> 
			<?php while($row = $values->fetch_object()){ ?> 
			<table class="table table-condensed table-bordered table-hover"> 
				<tbody> 
					<tr>
						<th><strong>#</strong></th>
						<td><?= $row->id_proyecto; ?></td>
					</tr>
					<tr>
						<th><strong>Ministerio</strong></th>
						<td><?= $row->nombre_dependencia; ?></td>
					</tr>
					<tr>
						<th><strong>Sector</strong></th>
						<td><?= $row->nombre_sector; ?></td>
					</tr>
					<tr>
						<th><strong>Nombre Programa/Proyecto</strong></th>
						<td><?= htmlspecialchars_decode($row->nombre_proyecto, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Componentes</strong></th>
						<td><?= htmlspecialchars_decode($row->componentes, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Objetivos</strong></th>
						<td><?= htmlspecialchars_decode($row->objetivos, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Resultados</strong></th>
						<td><?= htmlspecialchars_decode($row->resultados, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Impacto</strong></th>
						<td><?= htmlspecialchars_decode($row->indicador_impacto, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Tipo</strong></th>
						<td><?= $row->nombre_tipo; ?></td>
					</tr>
					<tr>
						<th><strong>Gestion inicial</strong></th>
						<td><?= htmlspecialchars_decode($row->gestion_inicial, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Gestion final</strong></th>
						<td><?= htmlspecialchars_decode($row->gestion_final, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Presupuesto total</strong></th>
						<td><?= htmlspecialchars_decode($row->presupuesto_total, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Contraparte municipal</strong></th>
						<td><?= $row->contraparte_municipal; ?></td>
					</tr>
					<tr>
						<th><strong>Cobertura poblacional urbana hombres</strong></th>
						<td><?= htmlspecialchars_decode($row->cobertura_urbana_hombres, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Cobertura poblacional urbana mujeres</strong></th>
						<td><?= htmlspecialchars_decode($row->cobertura_urbana_mujeres, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Cobertura poblacional rural hombres</strong></th>
						<td><?= htmlspecialchars_decode($row->cobertura_rural_hombres, ENT_QUOTES); ?></td>
					</tr>
					<tr>
						<th><strong>Cobertura poblacional rural mujeres</strong></th>
						<td><?= htmlspecialchars_decode($row->cobertura_rural_mujeres, ENT_QUOTES); ?></td>
					</tr>
				</tbody> 
			</table> 
			<?php } ?> 
		</div>
	</div>
</div>
