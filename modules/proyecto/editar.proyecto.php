<?php
require(SYSTEM.'helpers/date.code_helper'.EXT);
require_once(MODULES.'proyecto/db.proyecto'.EXT);

$pro = new proyecto();

$campos_dependencia = array('nombre_dependencia', 'id_dependencia');
$value_dependencia = $pro->_select_proyecto($campos_dependencia, NULL, NULL, NULL, "dependencia");
if(!$value_dependencia)echo $pro->error;


$campos_sector_proyecto = array('nombre_sector', 'id_sector_proyecto');
$value_sector_proyecto = $pro->_select_proyecto($campos_sector_proyecto, NULL, NULL, NULL, "sector_proyecto");
if(!$value_sector_proyecto)echo $pro->error;


$campos_tipo_proyecto = array('nombre_tipo', 'id_tipo_proyecto');
$value_tipo_proyecto = $pro->_select_proyecto($campos_tipo_proyecto, NULL, NULL, NULL, "tipo_proyecto");
if(!$value_tipo_proyecto)echo $pro->error;

// Recibiendo variable
$id_proyecto = addslashes(trim($_GET['id_proyecto']));

$news = new proyecto();

// Estableciendo parametro recibido
$where = array("id_proyecto" => "$id_proyecto");
$values = $news->_select_proyecto('*', $where);
if(!$values)echo $news->error;

?>
<script> 
$(function(){ 
	$('#id_dependencia').select(); 
	$.validate(); 
}); 
</script> 
<br>
<h4 class="page-header">Modificando la Información del Proyecto</h4>
<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Modificando Proyecto </strong></div> 
	<div class="panel-body"> 
		<?php while($row = $values->fetch_object()){ ?>
		<form class="container" name="form1" method="post" id="formid" action="?m=proyecto&f=savechanges"> 
			<p> 
				<p> 
				<label for="id_dependencia"> 
					<strong>Ministerio:</strong> 
					<select name="id_dependencia" id="id_dependencia" class="form-control"> 
					<?php while($row_dependencia = $value_dependencia -> fetch_object()){ ?>
						<option value="<?= $row_dependencia->id_dependencia; ?>" <?php if($row->id_dependencia==$row_dependencia->id_dependencia){ echo "selected"; } ?>><?= $row_dependencia->nombre_dependencia; ?></option> 
					<?php } ?>
					</select> 
				</label> 
				</p> 
				<p> 
				<label for="id_sector_proyecto"> 
					<strong>Sector:</strong> 
					<select name="id_sector_proyecto" id="id_sector_proyecto" class="form-control"> 
					<?php while($row_sector_proyecto = $value_sector_proyecto -> fetch_object()){ ?>
						<option value="<?= $row_sector_proyecto->id_sector_proyecto; ?>" <?php if($row->id_sector_proyecto==$row_sector_proyecto->id_sector_proyecto){ echo "selected"; } ?>><?= $row_sector_proyecto->nombre_sector; ?></option> 
					<?php } ?>
					</select> 
				</label> 
				</p> 
				<p> 
				<label for="nombre_proyecto"> 
					<strong>Nombre Programa/Proyecto:</strong> 
					<input type="text" name="nombre_proyecto" id="nombre_proyecto" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->nombre_proyecto, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="componentes"> 
					<strong>Componentes:</strong> 
					<textarea name="componentes" id="componentes" data-validation="required" class="form-control" rows="5" cols="55"><?= htmlspecialchars_decode($row->componentes, ENT_QUOTES); ?></textarea> 
				</label> 
				</p> 
				<p> 
				<label for="objetivos"> 
					<strong>Objetivos:</strong> 
					<textarea name="objetivos" id="objetivos" data-validation="required" class="form-control" rows="5" cols="55"><?= htmlspecialchars_decode($row->objetivos, ENT_QUOTES); ?></textarea> 
				</label> 
				</p> 
				<p> 
				<label for="resultados"> 
					<strong>Resultados:</strong> 
					<textarea name="resultados" id="resultados" data-validation="required" class="form-control" rows="5" cols="55"><?= htmlspecialchars_decode($row->resultados, ENT_QUOTES); ?></textarea> 
				</label> 
				</p> 
				<p> 
				<label for="indicador_impacto"> 
					<strong>Indicador de Impacto:</strong> 
					<input type="text" name="indicador_impacto" id="indicador_impacto" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->indicador_impacto, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="id_tipo_proyecto"> 
					<strong>Tipo:</strong> 
					<select name="id_tipo_proyecto" id="id_tipo_proyecto" class="form-control"> 
					<?php while($row_tipo_proyecto = $value_tipo_proyecto -> fetch_object()){ ?>
						<option value="<?= $row_tipo_proyecto->id_tipo_proyecto; ?>" <?php if($row->id_tipo_proyecto==$row_tipo_proyecto->id_tipo_proyecto){ echo "selected"; } ?>><?= $row_tipo_proyecto->nombre_tipo; ?></option> 
					<?php } ?>
					</select> 
				</label> 
				</p> 
				<p> 
				<label for="gestion_inicial"> 
					<strong>Gestion inicial:</strong> 
					<input type="text" name="gestion_inicial" id="gestion_inicial" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->gestion_inicial, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="gestion_final"> 
					<strong>Gestion final:</strong> 
					<input type="text" name="gestion_final" id="gestion_final" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->gestion_final, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="presupuesto_total"> 
					<strong>Presupuesto total:</strong> 
					<input type="text" name="presupuesto_total" id="presupuesto_total" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->presupuesto_total, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="contraparte_municipal"> 
					<strong>Contraparte municipal:</strong> 
					<select name="contraparte_municipal" id="contraparte_municipal" class="form-control"> 
						<option value="<?= 'SI'; ?>" <?php if($row->contraparte_municipal=='SI'){ echo "selected"; } ?>><?= 'SI'; ?></option> 
						<option value="<?= 'NO'; ?>" <?php if($row->contraparte_municipal=='NO'){ echo "selected"; } ?>><?= 'NO'; ?></option> 
					</select> 
				</label> 
				</p> 
				<p> 
				<label for="cobertura_urbana_hombres"> 
					<strong>Cobertura poblacional urbana hombres:</strong> 
					<input type="text" name="cobertura_urbana_hombres" id="cobertura_urbana_hombres" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->cobertura_urbana_hombres, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="cobertura_urbana_mujeres"> 
					<strong>Cobertura poblacional urbana mujeres:</strong> 
					<input type="text" name="cobertura_urbana_mujeres" id="cobertura_urbana_mujeres" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->cobertura_urbana_mujeres, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="cobertura_rural_hombres"> 
					<strong>Cobertura poblacional rural hombres:</strong> 
					<input type="text" name="cobertura_rural_hombres" id="cobertura_rural_hombres" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->cobertura_rural_hombres, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
				<p> 
				<label for="cobertura_rural_mujeres"> 
					<strong>Cobertura poblacional rural mujeres:</strong> 
					<input type="text" name="cobertura_rural_mujeres" id="cobertura_rural_mujeres" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->cobertura_rural_mujeres, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
			</p> 
			<p>
				<input type="hidden" name="id_proyecto" id="id_proyecto" value="<?= $id_proyecto; ?>"/>
				<button type="submit" name="Enviar" class="btn btn-success" /><span class="glyphicon glyphicon-floppy-disk"></span> <span class="hidden-xs">Guardar Cambios</span></button> <a href="?m=proyecto&f=lista" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span> <span class="hidden-xs">Cancelar</span></a>
			</p>
		</form> 
		<?php } ?>
	</div> 
