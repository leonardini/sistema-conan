<?php
// Incluyendo archivo de la base de Datos
require_once(MODULES.'cargos/db.cargos'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
// Inicializando la clase de la base de Datos
$new = new cargos();
// Recibiendo y limpiando Datos
$id_cargo = $_POST['id_cargo'];
$cargo = htmlspecialchars($_POST['cargo'], ENT_QUOTES);
$insert = array("cargo" => "$cargo");
$last_insert = $new->_make_insert_cargos($insert);
header("Location: in.php?m=cargos&f=lista");
exit;

?>
