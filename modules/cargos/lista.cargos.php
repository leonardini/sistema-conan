<?php

/*
*   Lista regular de los campos de una base de datos
*   Powered by OHK
*/

require_once(MODULES.'cargos/db.cargos'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new cargos();

$campos = array('*');
$limit = "0,150";
$where_u = "";

$values = $new->_select_cargos('*', $where_u);

?>
<script> 
$(function(){ 
	$('#sort').dataTable(); 
	$('[title]').tooltip(); 
}); 
function eliminar(id){
	bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
		if(result){
			window.location="?m=cargos&f=eliminar&id_cargo="+id;
		}
	});
}
</script> 
<br>
<h4 class="page-header">Cargos</h4>
	<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Listado General </strong></div> 
	<div class="panel-body"> 
	<p><a href="?m=cargos&f=nuevo" class='btn btn-success'><span class="glyphicon glyphicon-share"></span><span class="hidden-xs"> Crear nuevo cargo</span></a></p> 
		<div class="table-responsive">
		<table class="table table-bordered table-condensed table-hover" id="sort"> 
			<thead> 
				<tr class="text-center"> 
					<th><strong>#</strong></th>
					<th><strong>Cargo</strong></th>
					<th class="text-center">Opciones</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php
				$count=0;
				while($row = $values->fetch_object()){ 
					$count++;
				?>
				<tr> 
					<td><?= $count; ?></td>
					<td><?= htmlspecialchars_decode($row->cargo, ENT_QUOTES); ?></td>
					<td class="text-center">
						<a href="?m=cargos&f=editar&id_cargo=<?= $row->id_cargo; ?>" title="Modificar" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span> Modificar</a>
						<a href="javascript:eliminar(<?= $row->id_cargo; ?>)" title="Eliminar" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Eliminar</a>
					</td>
				</tr> 
				<?php } ?> 
			</tbody> 
		</table> 
		</div> 
	</div> 
