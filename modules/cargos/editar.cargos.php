<?php
require(SYSTEM.'helpers/date.code_helper'.EXT);
require_once(MODULES.'cargos/db.cargos'.EXT);
// Recibiendo variable
$id_cargo = addslashes(trim($_GET['id_cargo']));

$news = new cargos();

// Estableciendo parametro recibido
$where = array("id_cargo" => "$id_cargo");
$values = $news->_select_cargos('*', $where);
if(!$values)echo $news->error;
$news->close();
?>
	<script> 
	$(function(){ 
		$('#cargo').select(); 
		$.validate(); 
	}); 
	</script> 
	<br>
	<div class="panel panel-default"> 
	<div class="panel-heading"><strong> Modificando la informacion </strong></div> 
	<div class="panel-body"> 
		<?php while($row = $values->fetch_object()){ ?>
		<form class="container" name="form1" method="post" id="formid" action="?m=cargos&f=savechanges"> 
			<p> 
				<p> 
				<label for="cargo"> 
					<strong>Cargo:</strong> 
					<input type="text" name="cargo" id="cargo" data-validation="required" class="form-control" value="<?= htmlspecialchars_decode($row->cargo, ENT_QUOTES); ?>"/> 
				</label> 
				</p> 
			</p> 
			<p>
				<input type="hidden" name="id_cargo" id="id_cargo" value="<?= $id_cargo; ?>"/>
				<button type="submit" name="Enviar" class="btn btn-success" /><span class="glyphicon glyphicon-floppy-disk"></span> <span class="hidden-xs">Guardar Cambios</span></button> <a href="?m=cargos&f=lista" class="btn btn-danger"><span class="glyphicon glyphicon-ban-circle"></span> <span class="hidden-xs">Cancelar</span></a>
			</p>
		</form> 
		<?php } ?>
	</div> 
