<?php

/*
*   Vista regular de los campos de una base de datos
*   Powered by OHK
*/

require_once(MODULES.'cargos/db.cargos'.EXT);
require(SYSTEM.'helpers/date.code_helper'.EXT);
$new = new cargos();

$id_cargo = addslashes(trim($_GET['id_cargo']));

$where_u = array("id_cargo" => "$id_cargo");

$values = $new->_select_cargos('*', $where_u);
if(!$values)echo $new->error;
$new->close();
?>

	<script> 
	function eliminar(id){
		bootbox.confirm("Está seguro que desea eliminar la informacion?",function(result){
			if(result){
				window.location="?m=cargos&f=eliminar&id_cargo="+id;
			}
		});
	}
	</script> 
	<div class="panel panel-primary"> 
	<div class="panel-heading"><strong> Vista de cargos </strong></div> 
	<div class="panel-body"> 
	<p>Opciones en cargos: </p> 
	<p> 
		<a href="?m=cargos&f=lista" class='btn btn-success'><span class="glyphicon glyphicon-list"></span><span class="hidden-xs"> Listado</span></a> 
		<a href="?m=cargos&f=nuevo" class='btn btn-primary'><span class="glyphicon glyphicon-plus"></span><span class="hidden-xs"> Nuevo(a)</span></a> 
		<a href="?m=cargos&f=editar&id_cargo=<?= $id_cargo; ?>" class='btn btn-warning'><span class="glyphicon glyphicon-pencil"></span><span class="hidden-xs"> Modificar</span></a> 
		<a href="javascript:eliminar(<?= $id_cargo; ?>)" class='btn btn-danger'><span class="glyphicon glyphicon-remove"></span><span class="hidden-xs"> Eliminar</span></a> 
	</p> 
		<div class="panel panel-default"> 
			<div class="panel-heading"><strong>cargos</strong></div> 
			<div class="panel-body"> 
			<p>Vista única de cargos</p> 
			</div> 
			<div class="table-responsive"> 
			<?php while($row = $values->fetch_object()){ ?> 
			<table class="table table-condensed table-bordered table-hover"> 
				<tbody> 
					<tr>
						<th><strong>#</strong></th>
						<td><?= $row->id_cargo; ?></td>
					</tr>
					<tr>
						<th><strong>Cargo</strong></th>
						<td><?= htmlspecialchars_decode($row->cargo, ENT_QUOTES); ?></td>
					</tr>
				</tbody> 
			</table> 
			<?php } ?> 
		</div>
	</div>
</div>
