<?php

/*
 +-------------------------------------------------------------------+
 |                       OHK FRAMEWORK 3.0                           |
 |                                                                   |
 | Copyright Ing. Lenin Aparicio     ohkmalganis@gmail.com           |
 | Desarrollado el 2013              Ultima modificacion 25 Mar 2014 |
 | Diseño Boostrap, adaptado por Wilfredo Nina 						 |
 +-------------------------------------------------------------------+
 | Framework de Only HacKers  3.0                                    |
 |                                                                   |
 | Framework liviano y de uso sencillo para sistemas.                |
 | Applet Technologies Innovation Development                        |
 | www.facebook.com/Lenin.Aparicio                                   |
 | La Paz - Bolivia                                                  |
 | En caso de Desarrollo bajo este framework, favor de conservar     |
 | el nombre del Autor.												 |
 | Phone: 591-76108443 :: 591-72929301                               |
 +-------------------------------------------------------------------+
*/

require_once('system/config/entorno.php');

#	Inclusion del archivo "Plantilla Inicial".
#	Modificar a requerimiento y gusto.

require_once($htmldir . '/index.html.php');

?>